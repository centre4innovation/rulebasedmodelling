# Rule Based Modelling #

Copyright 2018 Centre for Innovation, Leiden University

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Set up database ##

sudo -u postgres psql -p 5432

`
CREATE USER riskmonitor WITH PASSWORD 'riskmonitor';
CREATE DATABASE riskmonitoring OWNER riskmonitor;
\connect riskmonitoring
CREATE EXTENSION "uuid-ossp";
CREATE EXTENSION pgcrypto;
`

Run app with `db migrate mysettings.yml`