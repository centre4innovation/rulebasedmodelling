<#macro subheader pagetitle level1 level2>
    <!-- begin:: Subheader -->
    <div class="k-subheader   k-grid__item" id="k_subheader">
        <div class="k-subheader__main">
            <h3 class="k-subheader__title">${pagetitle}</h3>

            <span class="k-subheader__separator k-hidden"></span>
            <div class="k-subheader__breadcrumbs">
                <a href="#" class="k-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                <span class="k-subheader__breadcrumbs-separator"></span>
                <span class="k-subheader__breadcrumbs-link">${level1}</span>
                <span class="k-subheader__breadcrumbs-separator"></span>
                <span class="k-subheader__breadcrumbs-link">${level2}</span>
            </div>

        </div>
        <div class="k-subheader__toolbar">
            <div class="k-subheader__wrapper">

            </div>
        </div>
    </div>
</#macro>

<#macro basicportlet title>
<div class="k-portlet">
    <div class="k-portlet__head">
        <div class="k-portlet__head-label">
            <h3 class="k-portlet__head-title">
                ${title}
            </h3>
        </div>
    </div>
    <div class="k-portlet__body">
        <div class="k-portlet__content">
            <#nested>
        </div>
    </div>
</div>
</#macro>

<#macro dropdown>
    <div class="dropdown-menu dropdown-menu-right dropdown-menu-fit dropdown-menu-md horizontal_scroll" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(534px, 39px, 0px);">
        <!--begin::Nav-->
        <ul class="kt-nav" id="mylist">
        </ul>
        <!--end::Nav-->
    </div>
</#macro>

<#macro toolbarportlet title dropdown_name>
    <div class="k-portlet">
        <div class="k-portlet__head">
            <div class="k-portlet__head-label">
                <h3 class="k-portlet__head-title">
                    ${title}
                </h3>
            </div>
            <div class="k-portlet__head-toolbar">
                <a href="#" class="btn btn-label-brand btn-bold btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    ${dropdown_name}
                </a>
                <@dropdown></@dropdown>
            </div>
        </div>
        <div class="k-portlet__body">
            <div class="k-portlet__content">
                <#nested>
            </div>
        </div>
    </div>
</#macro>

<#macro buttonportlet title button_title button_id>
    <div class="k-portlet">
        <div class="k-portlet__head">
            <div class="k-portlet__head-label">
                <h3 class="k-portlet__head-title">
                    ${title}
                </h3>
            </div>
            <div class="k-portlet__head-toolbar">
                <button type="button" id=${button_id} class="btn btn-primary btn-sm">
                    ${button_title}
                </button>
            </div>
        </div>
        <div class="k-portlet__body">
            <div class="k-portlet__content">
                <#nested>
            </div>
        </div>
    </div>
</#macro>

<#macro heightportlet title height>
    <div class="k-portlet" style="min-height:${height}">
        <div class="k-portlet__head">
            <div class="k-portlet__head-label">
                <h3 class="k-portlet__head-title">
                    ${title}
                </h3>
            </div>
        </div>
        <div class="k-portlet__body">
            <div class="k-portlet__content">
                <#nested>
            </div>
        </div>
    </div>
</#macro>

<#macro idportlet title id>
    <div class="k-portlet">
        <div class="k-portlet__head">
            <div class="k-portlet__head-label">
                <h3 class="k-portlet__head-title">
                    ${title}
                </h3>
            </div>
        </div>
        <div class="k-portlet__body">
            <div class="k-portlet__content" id=${id}>
                <#nested>
            </div>
        </div>
    </div>
</#macro>