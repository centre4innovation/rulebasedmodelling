<#import "keen.ftl" as k>

<#macro pageTitle>
    Loading Data Sources
</#macro>

<#macro pageHead>
    <style>
        div.horizontal_scroll {
            width: 100%;
            height: 300px;
            overflow: auto;
        }
    </style>
</#macro>

<#macro pageContent>
    <@k.subheader pagetitle="Import data" level1="Prepare" level2="Import"></@k.subheader>
    <!-- begin:: Content -->
    <div class="k-content k-grid__item k-grid__item--fluid" id="k_content">
        <!--begin::Dashboard 1-->
        <div class="row">

            <div class="col-md-6">
                <!--begin::Portlet-->
                <div class="k-portlet">
                    <div class="k-portlet__head">
                        <div class="k-portlet__head-label">
                            <h3 class="k-portlet__head-title">Import data</h3>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <form class="k-form" >
                        <div class="k-portlet__body">

                            <div class="form-group">
                                <label for="datasetname">Name</label>
                                <input type="text" class="form-control"  name="name" id="name" placeholder="Dataset name">
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <input type="text" class="form-control" id="description" name="description" placeholder="Short summary">
                            </div>
                            <div class="form-group">
                                <label for="source">Source</label>
                                <input type="text" class="form-control" id="source" name="source" placeholder="Origin of the data">
                            </div>
                            <div class="form-group">
                                <label for="source">Date Dataset created</label>
                                <input type="date" class="form-control" id="createdYear" name="createdYear" placeholder="Year dataset initially created">
                            </div>
                            <div class="form-group">
                                <label for="source">Date Last Updated</label>
                                <input type="date" class="form-control" id="updated" name="updated" placeholder="Date of last update to dataset">
                            </div>
                            <div class="form-group">
                                <label for="source">Purpose</label>
                                <input type="text" class="form-control" id="whyCreated" name="whyCreated" placeholder="For what purpose was dataset created?">
                            </div>
                            <div class="form-group">
                                <label>File</label>
                                <div></div>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="csvFile" id="csvFile" accept = "*.csv">
                                    <label class="custom-file-label" for="location">Choose a .csv file</label>
                                </div>
                            </div>
                        </div>
                        <div class="k-portlet__foot">
                            <div class="k-form__actions">
                                <a href="#" id="importBtn" class="btn btn-primary">Upload</a>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
            <div class="col-md-6">
                <@k.toolbarportlet title="Table Statistics" dropdown_name="Summaries">
                    <p id="status"></p>
                </@k.toolbarportlet>
            </div>
        </div>
    </div>
</#macro>

<#macro pageScript>

    <script>
        $(document).ready(() => {

            var dataSummary;
            var summary;

            $('#importBtn').click(e => {
                $('#status').html('Uploading data...')
                const formData = new FormData();
                const name = $('#name').val();
                formData.append("name", name);
                formData.append("description", $('#description').val());
                formData.append("source", $('#source').val());
                formData.append("createdYear", $('#createdYear').val());
                formData.append("updated", $('#updated').val());
                formData.append("whyCreated", $('#whyCreated').val());
                formData.append("file", $('#csvFile')[0].files[0]);

                $.ajax({
                    type: "POST",
                    url: "/api/v1/datacombination/data/import",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: (a) => {
                        dataSummary = a;
                        let tabName = a['tableName'];
                        let cols = a['columns'];
                        summary = {'table':tabName,'# of columns':cols.length};
                        $('#status').html($.cfi.toTable(summary));
                        $.cfi.notify('Data uploaded');
                        listGenerator(cols);
                    },
                    error: (a) => {
                        $.cfi.notify('Error ' + a, 'error')
                    }
                })
            });

            var listGenerator = function(columns){
                var node = document.createElement("li");                 // Create a <li> node
                node.setAttribute("class", "k-nav__item")
                var anode = document.createElement("a");
                var textnode = document.createTextNode('Overall');         // Create a text node
                anode.appendChild(textnode);
                anode.setAttribute("href", "#");
                anode.setAttribute("id", "overall");
                anode.setAttribute("class", "summary");
                node.appendChild(anode);
                document.getElementById("mylist").appendChild(node);
                for(c in columns) {
                    var node = document.createElement("li");                 // Create a <li> node
                    node.setAttribute("class", "k-nav__item")
                    var anode = document.createElement("a");
                    var textnode = document.createTextNode(columns[c]['columnName']);         // Create a text node
                    anode.appendChild(textnode);
                    anode.setAttribute("href", "#");
                    anode.setAttribute("id", columns[c]['columnName']);
                    anode.setAttribute("class", 'summary');
                    node.appendChild(anode);
                    document.getElementById("mylist").appendChild(node);
                }
            };


            $(document).on('click', 'a.summary', function(e) {
                let colName = e.target.id;
                if(colName != 'overall') {
                    let cols = dataSummary['columns'];
                    let colOfInterest = cols.filter(c => c['columnName'] === colName);
                    $('#status').html($.cfi.toTable(colOfInterest[0]))
                }
                else{
                    $('#status').html($.cfi.toTable(summary))
                }
            });

        });

    </script>

</#macro>