<#import "keen.ftl" as k>

<#macro pageTitle>
    Data Modelling: Decision Tree Results
</#macro>

<#macro pageHead>
    <style>
        #cy {
            width: 100%;
            height: 100%;
            position: absolute;
            left: 0;
            top: 0;
            z-index: 999;
        }

        .multiselect {
            width: 200px;
        }

        .selectBox {
            position: relative;
        }

        .selectBox select {
            width: 100%;
            font-weight: bold;
        }

        .overSelect {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
        }

        div.horizontal {
            width: 1000px;
            height: 100%;
            overflow: auto;
        }

        div.horizontal_scroll {
            width: 100%;
            height: 300px;
            overflow: auto;
        }

        #checkboxes {
            display: none;
            border: 1px #dadada solid;
        }

        #checkboxes label {
            display: block;
        }

        #checkboxes label:hover {
            background-color: #1e90ff;
        }

        .table-scrollable{
            max-width: 100%;
        }


        #myProgress {
          width: 100%;
          background-color: #ddd;
        }

        #myBar {
          width: 0%;
          height: 50px;
          background-color: #9e42f4;
          text-align: center;
          line-height: 50px;
          color: white;
        }

        #myBaselineBar {
          width: 0%;
          height: 50px;
          background-color: #4286f4;
          text-align: center;
          line-height: 50px;
          color: white;
        }



    </style>

    <link href="/assets/vendors/custom/slider/css/bootstrap-slider.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/assets/vendors/custom/multiselect/css/bootstrap-multiselect.css" type="text/css"/>
    <link href="/assets/vendors/custom/footable/css/footable.bootstrap.css" rel="stylesheet" type="text/css" />

</#macro>

<#macro pageContent>
    <@k.subheader pagetitle="Model Results" level1="Decision Tree" level2="Analysis"></@k.subheader>

    <div class="k-content k-grid__item k-grid__item--fluid" id="k_content">
    <div class="row">
        <div class="col-lg-12">
            <@k.basicportlet title="Load Model">
                <form>
                    <div class="row" style="margin-bottom: 5%">
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    Model
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <select class="form-control" name="models" id="models" onchange = "selectModel(this)">
                                        <option>Select</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </@k.basicportlet>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <@k.basicportlet title="Model Requirements">
            The features required in the data to run the selected model are...
                <p id="status"></p>
            </@k.basicportlet>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <@k.basicportlet title="Run Model">
            <form>
                <div class="row" style="margin-bottom: 5%">
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-12">
                                Table
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <select class="form-control" name="tables" id="tables" onchange="setSelectedTable(this)">
                                    <option>Select</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <button type="button" id="saveButton" class="btn btn-primary btn-lg" onclick="runModel()">Run Decision Tree</button>
                    </div>
                </div>
                </form>
            </@k.basicportlet>
        </div>
        <div class="row">
            <div class="col-md-12" style="min-width: 300px">
                <@k.buttonportlet title="Predictions" button_title="CSV Export" button_id="csvexport">
                    <div class="horizontal">
                        <p id="preds"></p>
                    </div>
                </@k.buttonportlet>
            </div>
        </div>

    </div>



</#macro>

<#macro pageScript>
    <script src="/assets/vendors/custom/slider/bootstrap-slider.js" type="text/javascript"></script>
    <script type="text/javascript" src="/assets/vendors/custom/multiselect/js/bootstrap-multiselect.js"></script>
    <script src="/assets/vendors/custom/plotly/plotly.min.js" type="text/javascript"></script>
    <script src="/assets/vendors/custom/footable/js/footable.js" type="text/javascript"></script>
<!-- Code Editor scripts -->
    <script>

        var model_name;
        var table;

        selectModel = function(selectedObject){
            model_name = selectedObject.value;
            $.get('/api/v1/datacombination/data/loaddecisiontree',{
            modelName:model_name
        }, function(data){
                    let cols = {'Features':data[0]};
                    $('#status').html($.cfi.toTable(cols))
                }
            )
        };

        runModel = function(){
            $.get('/api/v1/datacombination/data/rundecisiontree',{dataset:table, modelName:model_name},
            function(data){
                $('#preds').html(createTable(data));
                jQuery(function($){
                    $('#predTable').footable();
                });
            });
        };


        $("#csvexport").on("click",function(){
            let csv = FooTable.get('#predTable').toCSV();
            var hiddenElement = document.createElement('a');
            hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
            hiddenElement.target = '_blank';
            hiddenElement.download = 'Prediction.csv';
            hiddenElement.click();
        });









        var tables;
        var columns;
        $.get('/api/v1/datacombination/data/datasetInfo', {}, function(data) {
            tables = Object.keys(data);
            columns = data;
        });


        findTable = function(word, namelist){
            if(word == 'table'){
                for(var i = 0; i < tables.length; i++){
                    namelist.push(tables[i])
                }
                return namelist;
            }
            if(tables.includes(word)){
                var colArray = columns[word];
                for(var i = 0; i < colArray.length; i++){
                    namelist.push(word+"."+colArray[i])
                }
                return namelist;
            }
            if(word!='table' & !tables.includes(word)){
                namelist.push(word + ".");
                return namelist;
            }
            return namelist;
        }


        getDefinitions = function(e) {
            var temp = editor.getValue();
            var $this = $(e);
            $this.button('loading');
            if(temp === ""){
                $this.button('reset');
                buildSelect;
            }
            else {
                $.when($.get('/api/v1/datacombination/data/info', {
                    queryinfo: temp
                })).done(function () {
                    $this.button('reset');
                    buildSelect;
                });
            }
        };

    buildClassifierSelect = function(element, options){
        removeOptions(document.getElementById(element));
        var o = new Option('select', 'select');
        $('#'+ element).append(o);
        for(var c in options){
            var o = new Option(options[c], options[c]);
            $(o).html(options[c]);
            $('#'+element).append(o);
        }
    }

    buildSelect = function(element){
        $.get('/api/v1/datacombination/data/availableModels',{},function(options) {
                removeOptions(document.getElementById(element));
                var o = new Option("Select", "");
                o.selected = true;
                o.disabled = true;
                o.hidden = true;
                $('#'+element).append(o);
                for (var val in options) {
                    var o = new Option(options[val], options[val]);
                    $(o).html(options[val]);
                    $('#'+element).append(o);
                }
            }
        )
    };

    buildSelectTables = function(element){
        $.get('/api/v1/datacombination/data/availableTables',{},function(options) {
                removeOptions(document.getElementById(element));
                var o = new Option("Select", "");
                o.selected = true;
                o.disabled = true;
                o.hidden = true;
                $('#'+element).append(o);
                for (var val in options) {
                    var o = new Option(options[val], options[val]);
                    $(o).html(options[val]);
                    $('#'+element).append(o);
                }
            }
        )
    };

    removeOptions = function(selectbox)
    {
        for(let i = selectbox.options.length - 1 ; i >= 0 ; i--)
        {
            selectbox.options.remove(i);
        }
    }



    setTable = function(selectedObject){
        table = selectedObject.value;
        $.get("/api/v1/datacombination/data/columnnames",{
            table: selectedObject.value
        },function(options){
            removeOptions(document.getElementById('columnnames'));
            // var option = $('<option>');
            var o = new Option('select', 'select');
            // option.attr('select', 'select').text('select');
            $('#columnnames').append(o);
            var checkboxes = document.getElementById("checkboxes");
            var parser = new DOMParser()
            for (var val in options) {
                var o = new Option(options[val], options[val]);
                $(o).html(options[val]);
                // option.attr(options[val], options[val]).text(options[val]);
                $('#columnnames').append(o);
                var docfrag = "<label for = " + val.toString() + "><input type=\"checkbox\" id = "+ val.toString()+ " />" + options[val] + "</label>";
                var doc = document.createRange().createContextualFragment(docfrag);
                checkboxes.appendChild(doc.firstChild);
                allColumns.push(options[val]);
            }
            // $('#' + element).multiselect();
        });
    };


    setSelectedTable = function(selectedObject){
        table = selectedObject.value;
    };

    getSelectedText = function(elementId) {
        var elt = document.getElementById(elementId);

        if (elt.selectedIndex == -1)
            return "Empty";

        return elt.options[elt.selectedIndex].text;
    };

    getSelectedTextList = function(elementId) {
        var elt = document.getElementById(elementId);

        if (elt.selectedIndex == -1)
            return "Empty";

        var sel = [];
        for(let i in elt.options){
            if(elt.options[i].selected ===true){
                sel.push(elt.options[i].text);
            }
        }

        return sel;
    };

    createLabelList = function(selectedObject){
        $.get("/api/v1/datacombination/data/columnvalues",{
            tablename: table, columnname: selectedObject[selectedObject.value].text
        },function(options){
            removeOptions(document.getElementById('labels'));
            let id = 0;
            let o = new Option("Multi-Class", id);
            $(o).html("Multi-Class");
            $('#labels').append(o);
            if(getSelectedText('target') === "All"){
                let o = new Option("Binary", id);
                $(o).html("Binary");
                $('#labels').append(o);
            }
            else{
                for (let val in options) {
                    let o = new Option(options[val], val);
                    $(o).html(options[val]);
                    $('#labels').append(o);
                }
            }
        });
    };


    buildColumnSelect = function(element){
        $.get('/api/v1/datacombination/data/columnnames',{},function(options) {
                removeOptions(document.getElementById(element));
                // var option = $('<option>');
                var o = new Option('select', 'select');
                // option.attr('select', 'select').text('select');
                $('#'+ element).append(o);
                for (var val in options) {
                    var o = new Option(options[val], options[val]);
                    $(o).html(options[val]);
                    // option.attr(options[val], options[val]).text(options[val]);
                    $('#'+element).append(o);
                }
                $('#' + element).multiselect();
            }
        )
    };

    setColumn = function(selectedObject){
        column = selectedObject.value;
    };

    activateRun = function(e){
        var cbs = document.getElementsByName("cb");
        for (var i = 0; i < cbs.length; i++) {
            cbs[i].checked = false;
        }
        e.checked = true;
       $('#runButton').removeAttr('disabled');
       metric = e.id;
    };



        buildSelect('models');
        buildSelectTables('tables');

        viewTable = function(){
            $.get('/api/v1/datacombination/data/decisiontree/results', {column: column}, function (result) {
                        $('#analysePanel').html($.cfi.toTable(result))
                    });
        };






        var dataSummary;
            var summary;

            $('#importBtn').click(e => {
                $('#status').html('Uploading data...')
                const formData = new FormData();
                const name = $('#name').val();
                formData.append("name", name);
                formData.append("description", $('#description').val());
                formData.append("source", $('#source').val());
                formData.append("createdYear", $('#createdYear').val());
                formData.append("updated", $('#updated').val());
                formData.append("whyCreated", $('#whyCreated').val());
                formData.append("file", $('#csvFile')[0].files[0]);

                $.ajax({
                    type: "POST",
                    url: "/api/v1/datacombination/data/import",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: (a) => {
                        dataSummary = a;
                        let tabName = a['tableName'];
                        let cols = a['columns'];
                        summary = {'table':tabName,'# of columns':cols.length}
                        $('#status').html($.cfi.toTable(summary))
                        $.cfi.notify('Data uploaded');
                        listGenerator(cols);
                    },
                    error: (a) => {
                        $.cfi.notify('Error ' + a, 'error')
                    }
                })
            });

            var listGenerator = function(columns){
                var node = document.createElement("li");                 // Create a <li> node
                node.setAttribute("class", "k-nav__item")
                var anode = document.createElement("a");
                var textnode = document.createTextNode('Overall');         // Create a text node
                anode.appendChild(textnode);
                anode.setAttribute("href", "#");
                anode.setAttribute("id", "overall");
                anode.setAttribute("class", "summary");
                node.appendChild(anode);
                document.getElementById("mylist").appendChild(node);
                for(c in columns) {
                    var node = document.createElement("li");                 // Create a <li> node
                    node.setAttribute("class", "k-nav__item")
                    var anode = document.createElement("a");
                    var textnode = document.createTextNode(columns[c]['columnName']);         // Create a text node
                    anode.appendChild(textnode);
                    anode.setAttribute("href", "#");
                    anode.setAttribute("id", columns[c]['columnName']);
                    anode.setAttribute("class", 'summary');
                    node.appendChild(anode);
                    document.getElementById("mylist").appendChild(node);
                }
            };


            $(document).on('click', 'a.summary', function(e) {
                let colName = e.target.id;
                if(colName != 'overall') {
                    let cols = dataSummary['columns'];
                    let colOfInterest = cols.filter(c => c['columnName'] === colName);
                    $('#status').html($.cfi.toTable(colOfInterest[0]))
                }
                else{
                    $('#status').html($.cfi.toTable(summary))
                }
            });


            createTable = function(data){
                const newTable = '<table class="table table-bordered table-condensed table-striped" id="predTable" data-filtering="true" data-paging="true"/>';
                let tab;
                tab = $(newTable);

                //create table header from object keys
                const row = $('<tr />');
                $.each( data[0], function( key, value ) {
                    row.append($('<th />').append(key));
                });
                tab.append($('<thead />').append(row));

                //create table body from object values
                const bdy = $('<tbody />');
                $.each( data, function( key, value ) {
                    const bodyrow = $('<tr />');
                    $.each( value, function( k, v) {
                        bodyrow.append($('<td />').append(v));
                    });
                    bdy.append(bodyrow);
                });
                tab.append(bdy);

                return tab;
            }

    </script>


</#macro>