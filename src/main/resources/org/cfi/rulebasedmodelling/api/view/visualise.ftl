<#import "keen.ftl" as k>

<#macro pageTitle>
    Lookout: Combining Data Sources
</#macro>

<#macro pageHead>
</#macro>


<#macro pageContent>
    <@k.subheader pagetitle="Overview" level1="Analyse" level2="overview"></@k.subheader>


    <!-- begin:: Content -->
    <div class="k-content k-grid__item k-grid__item--fluid" id="k_content">
    <!--begin::Dashboard 1-->
    <div class="row">
    <div class="col-lg-12">

    <@k.basicportlet title="Timeline">
        <div id="timelinePlot" style="border: 1px solid #e7ecf1; height: 400px"></div>
    </@k.basicportlet>

    </div>
    </div>
    </div>
</#macro>

<#macro pageScript>
    <script src="/assets/vendors/custom/plotly/plotly.min.js" type="text/javascript"></script>

    <script>

        $.get('/api/v1/visualise/data/retrievetable', {}, function(data) {
            const trace = {
                y: data.map(tv => tv.value),
                x: data.map(tv => tv.t.substring(0, 10)),
                name: 'fatalities',
                mode: 'markers',
                marker: {
                    color: '#E43A45',
                    size: 10,
                    symbol: 'diamond'
                },
                type: 'scatter'
            };

            const layout = {
                paper_bgcolor: "rgb(255,255,255)",
                plot_bgcolor: "rgb(229,229,229)",
                title: 'Timeline',
                xaxis: {title: 'date'},
                yaxis: {title: 'count'},

            };

            var datapoints = [trace]

            Plotly.newPlot('timelinePlot', datapoints, layout);
        });
    </script>
</#macro>