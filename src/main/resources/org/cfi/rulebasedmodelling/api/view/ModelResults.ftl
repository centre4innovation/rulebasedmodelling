<#import "keen.ftl" as k>

<#macro pageTitle>
    Data Modelling: Decision Tree Results
</#macro>

<#macro pageHead>
    <style>
        #cy {
            width: 100%;
            height: 100%;
            position: absolute;
            left: 0;
            top: 0;
            z-index: 999;
        }

        .multiselect {
            width: 200px;
        }

        .selectBox {
            position: relative;
        }

        .selectBox select {
            width: 100%;
            font-weight: bold;
        }

        .overSelect {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
        }

        #checkboxes {
            display: none;
            border: 1px #dadada solid;
        }

        #checkboxes label {
            display: block;
        }

        #checkboxes label:hover {
            background-color: #1e90ff;
        }

        .table-scrollable{
            max-width: 100%;
        }


        #myProgress {
          width: 100%;
          background-color: #ddd;
        }

        #myBar {
          width: 0%;
          height: 50px;
          background-color: #9e42f4;
          text-align: center;
          line-height: 50px;
          color: white;
        }

        #myBaselineBar {
          width: 0%;
          height: 50px;
          background-color: #4286f4;
          text-align: center;
          line-height: 50px;
          color: white;
        }

        @media print
        {
            .no-print, .no-print *
            {
                display: none !important;
            }
        }

        .tooltip {
          position: relative;
          display: inline-block;
          border-bottom: 1px dotted black;
        }

        .tooltip .tooltiptext {
          visibility: hidden;
          width: 120px;
          background-color: #555;
          color: #fff;
          text-align: center;
          border-radius: 6px;
          padding: 5px 0;
          position: absolute;
          z-index: 1;
          bottom: 125%;
          left: 50%;
          margin-left: -60px;
          opacity: 0;
          transition: opacity 0.3s;
        }

        .tooltip .tooltiptext::after {
          content: "";
          position: absolute;
          top: 100%;
          left: 50%;
          margin-left: -5px;
          border-width: 5px;
          border-style: solid;
          border-color: #555 transparent transparent transparent;
        }

        .tooltip:hover .tooltiptext {
          visibility: visible;
          opacity: 1;
        }



    </style>

    <link href="/assets/vendors/custom/slider/css/bootstrap-slider.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/assets/vendors/custom/multiselect/css/bootstrap-multiselect.css" type="text/css"/>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">


</#macro>

<#macro pageContent>
    <@k.subheader pagetitle="Model Results" level1="Decision Tree" level2="Analysis"></@k.subheader>

    <div class="k-content k-grid__item k-grid__item--fluid" id="k_content">
    <div class="row no-print">
        <div class="col-lg-12">
            <@k.basicportlet title="Run Analysis">
                <form>
                    <div class="row" style="margin-bottom: 5%">
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    Table
                                    <i class="fa fa-info-circle" title="Data sets pre-loaded into tool"></i>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <select class="form-control" name="tables" id="tables" onchange="setSelectedTable(this)">
                                        <option>Select</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    Features To Include
                                    <i class="fa fa-info-circle" title="Columns of data set to be used in model"></i>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <select class="form-control" name="columns" id="columns">
                                        <option>Select</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    Feature Of Interest
                                    <i class="fa fa-info-circle" title="Particular column that you are trying to explain with the other columns"></i>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <select class="form-control" name="target" id="target" onchange="createLabelList(this)">
                                        <option>Select</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12">
                                    Feature Instance
                                    <i class="fa fa-info-circle" title="Perhaps you are interested in explaining only one value from the column of interet"></i>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <select class="form-control"  name="labels" id="labels">
                                        <option>Select</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-9">
                                    <span>Minimum Leaf Size: <span id="depthSliderVal"></span></span>
                                    <i class="fa fa-info-circle" title="Use this to change the depth of your decision tree"></i>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <b style="margin-right: 3%">Shallow</b><input id="depth" type="text", data-slider-min="Shallow", data-slider-max="Deep"><b style="margin-left: 3%">Deep</b>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 5%">
                                <div class="col-md-12">
                                    <input name="datecb" id="dateproperties" type="checkbox" class="btn btn-primary btn-sm" onclick="dateProperties()">Use Properties of Date</input>
                                    <i class="fa fa-info-circle" title="The model will use for example... Day of Week, Week of Year "></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <b>Choose Metric of Interest</b>
                            <i class="fa fa-info-circle" title="It is important to be aware of the question you are trying to answer"></i>
                            <div class="row">
                                <div class="col-md-12">
                                    <input name="cb" id="precision" type="checkbox" class="btn btn-primary btn-sm" onclick="activateRun(this)">Precision</input>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <input name="cb" id="recall" type="checkbox" class="btn btn-primary btn-sm" onclick="activateRun(this)">Recall</input>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <input name="cb" id="precisionRecall" type="checkbox" class="btn btn-primary btn-sm" onclick="activateRun(this)">Precision and Recall</input>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <button type="button" id="runButton" class="btn btn-primary btn-lg" onclick="runTree()">Run Decision Tree</button>
                        </div>
                    </div>
                </form>
            </@k.basicportlet>
        </div>
    </div>
    <div class="row" id="dvContainer">
        <div class="col-md-12">
            <@k.basicportlet title="View Results">
                <div class="row">
                    <div class="col-md-6">
                        <div id="featureImportancePlot" style="border: 1px solid #e7ecf1; height: 400px"></div>
                    </div>
                    <div class="col-md-6">
                        <div id="precisionRecallPlot" style="border: 1px solid #e7ecf1; height: 400px"></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <@k.basicportlet title="Accuracy">
                            <div class="row">
                                <div class="col-md-12">
                                Model Accuracy
                                    <div id="myProgress">
                                      <div id="myBar">0%</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                Baseline Accuracy
                                    <div id="myProgress">
                                      <div id="myBaselineBar">0%</div>
                                    </div>
                                </div>
                            </div>
                        </@k.basicportlet>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <@k.heightportlet title="Tree" height="300px">
                            <div id="cy"></div>
                        </@k.heightportlet>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="accuracy">Accuracy: <span id="acc"></span></div>
                        <div id="precision">Precision: <span id="pre"></span></div>
                        <div id="recall">Recall: <span id="rec"></span></div>
                    </div>
                </div>
            </@k.basicportlet>
        </div>
    </div>
    <#--<input type="button" value="Print Div Contents" id="btnPrint" />-->
    <div class="row">
        <div class="col-lg-12">
            <@k.basicportlet title="Save Model">
                <div class="row">
                    <div class="col-md-3">
                        <button type="button" id="saveButton" class="btn btn-primary btn-lg" onclick="saveTree()">Save Decision Tree</button>
                    </div>
                </div>
            </@k.basicportlet>
        </div>
    </div>



</#macro>

<#macro pageScript>
    <script src="/assets/vendors/custom/slider/bootstrap-slider.js" type="text/javascript"></script>
    <script type="text/javascript" src="/assets/vendors/custom/multiselect/js/bootstrap-multiselect.js"></script>
    <script src="/assets/vendors/custom/plotly/plotly.min.js" type="text/javascript"></script>
    <script src="/assets/vendors/custom/jpalette/jpalette.js"></script>
    <script src="/assets/vendors/custom/cytoscape/cytoscape.min.js"></script>
    <script src="/assets/vendors/custom/cytoscape/dagre.js"></script>
    <script src="/assets/vendors/custom/cytoscape/cytoscape-dagre.js"></script>
<!-- Code Editor scripts -->
    <script>

        var maxMin = 1000;

        var metric;

        $("#runButton").attr('disabled','disabled');

        $("#depth").slider({step: 2, min: 0, max: maxMin - 1, value: 1});
        $("#depthSliderVal").text(maxMin - 1);
        $("#depth").on("slide", function(slideEvt) {
            $("#depthSliderVal").text(maxMin - slideEvt.value);
        });

        var tables;
        var columns;
        $.get('/api/v1/datacombination/data/datasetInfo', {}, function(data) {
            tables = Object.keys(data);
            columns = data;
        });


        findTable = function(word, namelist){
            if(word == 'table'){
                for(var i = 0; i < tables.length; i++){
                    namelist.push(tables[i])
                }
                return namelist;
            }
            if(tables.includes(word)){
                var colArray = columns[word];
                for(var i = 0; i < colArray.length; i++){
                    namelist.push(word+"."+colArray[i])
                }
                return namelist;
            }
            if(word!='table' & !tables.includes(word)){
                namelist.push(word + ".");
                return namelist;
            }
            return namelist;
        }


        getDefinitions = function(e) {
            var temp = editor.getValue();
            var $this = $(e);
            $this.button('loading');
            if(temp === ""){
                $this.button('reset');
                buildSelect;
            }
            else {
                $.when($.get('/api/v1/datacombination/data/info', {
                    queryinfo: temp
                })).done(function () {
                    $this.button('reset');
                    buildSelect;
                });
            }
        };

    buildClassifierSelect = function(element, options){
        removeOptions(document.getElementById(element));
        var o = new Option('select', 'select');
        $('#'+ element).append(o);
        for(var c in options){
            var o = new Option(options[c], options[c]);
            $(o).html(options[c]);
            $('#'+element).append(o);
        }
    }

    buildSelect = function(element){
        $.get('/api/v1/datacombination/data/availableTables',{},function(options) {
                removeOptions(document.getElementById(element));
                var o = new Option("Select", "");
                o.selected = true;
                o.disabled = true;
                o.hidden = true;
                $('#'+element).append(o);
                for (var val in options) {
                    var o = new Option(options[val], options[val]);
                    $(o).html(options[val]);
                    $('#'+element).append(o);
                }
            }
        )
    }

    removeOptions = function(selectbox)
    {
        for(let i = selectbox.options.length - 1 ; i >= 0 ; i--)
        {
            selectbox.options.remove(i);
        }
    }

    var table;
    var column;
    var allColumns = [];

    setTable = function(selectedObject){
        table = selectedObject.value;
        $.get("/api/v1/datacombination/data/columnnames",{
            table: selectedObject.value
        },function(options){
            removeOptions(document.getElementById('columnnames'));
            // var option = $('<option>');
            var o = new Option('select', 'select');
            // option.attr('select', 'select').text('select');
            $('#columnnames').append(o);
            var checkboxes = document.getElementById("checkboxes");
            var parser = new DOMParser()
            for (var val in options) {
                var o = new Option(options[val], options[val]);
                $(o).html(options[val]);
                // option.attr(options[val], options[val]).text(options[val]);
                $('#columnnames').append(o);
                var docfrag = "<label for = " + val.toString() + "><input type=\"checkbox\" id = "+ val.toString()+ " />" + options[val] + "</label>";
                var doc = document.createRange().createContextualFragment(docfrag);
                checkboxes.appendChild(doc.firstChild);
                allColumns.push(options[val]);
            }
            // $('#' + element).multiselect();
        });
    };


    setSelectedTable = function(selectedObject){
        table = selectedObject.value;
        $.get("/api/v1/datacombination/data/columnnames",{
            table: selectedObject.value
        },function(options){
            removeOptions(document.getElementById('columns'));
            removeOptions(document.getElementById('target'));
            removeOptions(document.getElementById('labels'));

            let l = new Option("Select", 0);
            $('#labels').append(l);

            let el = document.getElementById('columns');
            let id = 0;
            let t = new Option("All", id);
            $(t).html("All");
            $('#target').append(t);
            let hml = el.outerHTML;
            if(!hml.includes('multiple')){
                hml = hml.replace('\"columns\">','\"columns\" multiple = \"multiple\">');
                document.getElementById('columns').outerHTML = hml;
            }
            for (let val in options) {
                id++;
                let o = new Option(options[id - 1], id - 1);
                let t = new Option(options[id - 1], id);
                $(o).html(options[val]);
                $(t).html(options[val]);
                $('#columns').append(o);
                $('#target').append(t);
            }
            let o = new Option("Multi-Class", 1);
            $(o).html("Multi-Class");
            $('#labels').append(o);
            if(getSelectedText('target') === "All"){
                let o = new Option("Binary", 2);
                $(o).html("Binary");
                $('#labels').append(o);
            }
            $('#columns').multiselect({includeSelectAllOption: true, maxHeight: 300});
            $('#columns').multiselect('rebuild');

            $.get("/api/v1/datacombination/data/tablerows",{
                table: selectedObject.value
            },function(rows){
                maxMin =  Math.round(rows * 0.25);
                $("#depth").slider({step: 2, min: 0, max: maxMin - 1, value: 1});
                $("#depthSliderVal").text(maxMin - 1);
            });

            var cbs = document.getElementsByName("cb");
            for (var i = 0; i < cbs.length; i++) {
                cbs[i].checked = false;
            }
            $("#runButton").attr('disabled','disabled');
       });

    };

    getSelectedText = function(elementId) {
        var elt = document.getElementById(elementId);

        if (elt.selectedIndex == -1)
            return "Empty";

        return elt.options[elt.selectedIndex].text;
    };

    getSelectedTextList = function(elementId) {
        var elt = document.getElementById(elementId);

        if (elt.selectedIndex == -1)
            return "Empty";

        var sel = [];
        for(let i in elt.options){
            if(elt.options[i].selected ===true){
                sel.push(elt.options[i].text);
            }
        }

        return sel;
    };

    createLabelList = function(selectedObject){
        $.get("/api/v1/datacombination/data/columnvalues",{
            tablename: table, columnname: selectedObject[selectedObject.value].text
        },function(options){
            removeOptions(document.getElementById('labels'));
            let id = 0;
            let o = new Option("Multi-Class", id);
            $(o).html("Multi-Class");
            $('#labels').append(o);
            if(getSelectedText('target') === "All"){
                let o = new Option("Binary", id);
                $(o).html("Binary");
                $('#labels').append(o);
            }
            else{
                for (let val in options) {
                    let o = new Option(options[val], val);
                    $(o).html(options[val]);
                    $('#labels').append(o);
                }
            }
        });
    };


    buildColumnSelect = function(element){
        $.get('/api/v1/datacombination/data/columnnames',{},function(options) {
                removeOptions(document.getElementById(element));
                // var option = $('<option>');
                var o = new Option('select', 'select');
                // option.attr('select', 'select').text('select');
                $('#'+ element).append(o);
                for (var val in options) {
                    var o = new Option(options[val], options[val]);
                    $(o).html(options[val]);
                    // option.attr(options[val], options[val]).text(options[val]);
                    $('#'+element).append(o);
                }
                $('#' + element).multiselect();
            }
        )
    };

    setColumn = function(selectedObject){
        column = selectedObject.value;
    };

    activateRun = function(e){
        var cbs = document.getElementsByName("cb");
        for (var i = 0; i < cbs.length; i++) {
            cbs[i].checked = false;
        }
        e.checked = true;
       $('#runButton').removeAttr('disabled');
       metric = e.id;
    };

    var dateprops = true;

    dateProperties =function(){
        let ck = document.getElementById("dateproperties").checked;
        if(ck){
            dateprops = false;
        }
        else{
            dateprops = true;
        }
    };

     accuracyBar = function(accu, emtName) {
                      var elem = document.getElementById(emtName);
                      var width = 0;
                      var id = setInterval(frame, 10);
                      function frame() {
                        if (width >= 100) {
                          clearInterval(id);
                        } else if(width < accu) {
                          width++;
                          elem.style.width = width + '%';
                          elem.innerHTML = width * 1  + '%';
                        }
                      }
                    };

    saveTree = function(){
        $.get('/api/v1/datacombination/data/savedecisiontree')
    };


    viewTree = function(target, instance){
        $.get('/api/v1/datacombination/data/decisiontree/jsontree',{
            column: target,
            target: instance
        },function(data){
            initialTree.elements.nodes = [];
            initialTree.elements.edges = [];
            classColours = new Map();
            colourNum = new Set();
            createTree(data, initialTree);
            var cy = window.cy = cytoscape(initialTree);
        })
    };
    var refresh = false;

    runTree = function(){
        var selectedColumns = [];
        for(var i = 0; i < allColumns.length; i++){
            if(document.getElementById(i.toString()).checked === true){
                selectedColumns.push(allColumns[i]);
            }
        }

        var myNode = document.getElementById("featureImportancePlot");
        while (myNode.firstChild) {
            myNode.removeChild(myNode.firstChild);
        }

        let table = getSelectedText('tables').toString();
        let selectedFeatures = getSelectedTextList('columns').toString();
        let target = getSelectedText('target').toString();
        let instance = getSelectedText('labels').toString();
        let depth = maxMin - document.getElementById('depth').value;
        $.get('/api/v1/datacombination/data/decisiontree',{
            table: table,
            selectedFeatures: selectedFeatures,
            target: target,
            instance: instance,
            depth: depth,
            dateprop: dateprops
        },function(data){
            let key = Object.keys(data)[0];
            let col = Object.values(data)[0];
            let colKeys = Object.keys(col)[0];
            let vals = Object.values(col)[0];
            let fI = vals.featureImportance;
            let p = parseFloat(vals.precision);
            let rc = parseFloat(vals.recall);



            var data = [{
                x: Object.values(fI),
                y: Object.keys(fI),
                type: 'bar',
                orientation: 'h'
            }];

            var layout = {
                title: 'Feature Importance',
                height: 400,
                width: 400,
                margin: {
                    l: 180,
                    r: 30,
                    b: 30,
                    t: 50,
                    pad: 4
                },
                yaxis: {
                    autorange: "reversed"
                },
            };

            Plotly.newPlot('featureImportancePlot', data, layout);

            var size = 100, x = new Array(size), y = new Array(size), z = new Array(size), i, j;

            for(let i = 0; i < size; i++) {
                x[i] = y[i] = i/size;
                z[i] = new Array(size);
            }

            var pM = 1;
            var rM = 1;
            var idealLabel = 1.39;

            if(metric === 'precision'){
                pM = 1;
                rM = 2.5;
                idealLabel = 1.84;
            }
            else if(metric == 'recall'){
                pM = 2.5;
                rM = 1;
                idealLabel = 1.84;
            }

            for(let i = 0; i < size; i++) {
                for(j = 0; j < size; j++) {
                    var r = Math.sqrt(pM*x[i]*x[i] + rM*y[j]*y[j]);
                    z[i][j] = r;
                }
            }

            let color_names = ['Unusable', 'Ideal'];
            let color_vals = [0.08, idealLabel];

            let hm = {
                    z: z,
                    x: x,
                    y: y,
                    type: 'heatmap',
                    zsmooth: 'best',
                    colorbar: {
                        tickvals: color_vals,
                        ticktext: color_names
                    }
                };

            let dataPoint = {
                    x:[p],
                    y:[rc],
                    mode: 'markers',
                    type: 'scatter',
                    marker: {
                        symbol: 'circle',
                        size: 16,
                        color: 'black'
                    }
                };

            let heat_data = [hm, dataPoint];

            var heat_layout = {
              title: 'Precision - Recall',
              xaxis: {title: 'Precision'},
              yaxis: {title: 'Recall'},
              width: 400,
              height: 400,
            };

            Plotly.newPlot('precisionRecallPlot', heat_data, heat_layout);

            let accy  = parseFloat(vals.accuracy).toFixed(2);
            let baccy  = parseFloat(vals.baselineAccuracy).toFixed(2);

            accuracyBar(accy, "myBar");
            accuracyBar(baccy, "myBaselineBar");

            viewTree(target, instance);


            let pecn = 100*parseFloat(vals.precision).toFixed(2);
            let recl = 100*parseFloat(vals.recall).toFixed(2);
            $('#acc').text(accy+"%");
            $('#pre').text(pecn+"%");
            $('#rec').text(recl+"%");
        });
    }


        var expanded = false;

        function showCheckboxes() {
            var checkboxes = document.getElementById("checkboxes");
            if (!expanded) {
                checkboxes.style.display = "block";
                expanded = true;
            } else {
                checkboxes.style.display = "none";
                expanded = false;
            }
        }

        buildSelect('tables');

        viewTable = function(){
            $.get('/api/v1/datacombination/data/decisiontree/results', {column: column}, function (result) {
                        $('#analysePanel').html(toTable(result))
                    });
        }

        $("#btnPrint").on("click", function () {
            var divContents = $("#dvContainer").html();
            var printWindow = window.open('', '', 'height=800,width=1500');
            printWindow.document.write('<html><head><title>DIV Contents</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
        });


        setColumn = function(selectedObject){
            column = selectedObject.value;
        };

        const getNestedObject = (nestedObj, pathArr) => {
            return pathArr.reduce((obj, key) =>
                (obj && obj[key] !== 'undefined') ? obj[key] : undefined, nestedObj);
        };

        var classColours = new Map();
        var colourNum = new Set();
        var colourMap = jPalette.ColorMap.get('rgb')(50);

        selectColour = function(classname){
            let num = Math.floor(Math.random() * 50);
            while(colourNum.has(num)){
                num = Math.floor(Math.random() * 50);
            }
            if(!classColours.has(classname)){
                colourNum.add(num);
                classColours.set(classname,colourMap['map'][num].rgb());
            }
            return classColours.get(classname);
        };

        createTree = function(data, element){
            let children = getNestedObject(data,['children']);
            let edgeLabel = getNestedObject(data,['value','op']) + " " + getNestedObject(data,['value','val']);
            let nodeId = getNestedObject(data,['id']);
            let sourceId = getNestedObject(data,['parent']);

            let shape = getNestedObject(data,['leaf']) === true ? 'rectangle' : 'ellipse';

            let colour = getNestedObject(data,['leaf']) === true ? selectColour(getNestedObject(data,['value','class'])) : '#11479e';

            let leafLabel = getNestedObject(data,['leaf']) === true ? "Class: " + getNestedObject(data,['value','class']) + "\n Number of data points: " +
                getNestedObject(data,['value','count']) + "\n Number of errors: " +
                getNestedObject(data,['value','errors']) : null;
            let splitOn = getNestedObject(data,['children',0,'value','attr']) != null ? getNestedObject(data,['children',0,'value','attr']): leafLabel;

            element.elements.nodes.push({ "data": { "id": 'n' + nodeId.toString(), "nodeType": shape, "nodeColour": colour, "splitOn": splitOn} });
            if(sourceId != null) {
                element.elements.edges.push({"data": {"source": 'n' + sourceId.toString(), "target": 'n' + nodeId.toString(), "label": edgeLabel}});
            }
            for(let child in children) {
                createTree(children[child], element);
            }
        };

        var nodeArray = [];
        var edgeArray= [];


        var initialTree  = {
            container: document.getElementById('cy'),

            boxSelectionEnabled: false,
            autounselectify: true,

            layout: {
                name: 'dagre'
            },

            style: [
                {
                    selector: 'node',
                    style: {
                        'content': 'data(splitOn)',
                        'shape': 'data(nodeType)',
                        'width': 110,
                        'background-opacity': 0.3,
                        'font-size': 9,
                        'text-opacity': 1,
                        'text-valign': 'center',
                        'text-halign': 'center',
                        "text-wrap": "wrap",
                        'background-color': 'data(nodeColour)'
                    }
                },

                {
                    selector: 'edge',
                    style: {
                        'label': 'data(label)',
                        'font-size': 7,
                        'curve-style': 'bezier',
                        'width': 4,
                        'target-arrow-shape': 'triangle',
                        'text-halign': 'left',
                        'line-color': '#9dbaea',
                        'target-arrow-color': '#9dbaea'
                    }
                }
            ],

            elements: {
                nodes: nodeArray,
                edges: edgeArray
            },
        };


    </script>





</#macro>