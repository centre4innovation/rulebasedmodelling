<#-- @ftlvariable name="" type="org.cfi.rulebasedmodelling.api.view.PageView" -->
<#import "${contentFtl}" as content>

<!DOCTYPE html>

<html lang="en" >
<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>
        Rule Based Modelling Dashboard
    </title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    <link href="/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<#--<link href="/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />-->
    <link href="/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <!--begin::Base Styles -->
    <!--begin::Page Vendors -->
    <link href="/assetz/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Page Vendors -->
    <link href="/assetz/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
    <link href="/assetz/demo/demo11/base/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Base Styles -->
    <link rel="shortcut icon" href="/assetz/demo/demo11/media/img/logo/favicon.ico" />

    <@content.pageHead/>
    <style>
        .portlet-shape{
            min-height: 250px;
            max-width: 250px;
            -webkit-border-radius: 100%;
            -moz-border-radius: 100%;
            border-radius: 100%;
            margin-left: 2%;
        }

        .portlet-override{
            background-color: #fff;
            box-shadow:none;
            -webkit-box-shadow:none;
        }
</style>
</head>
<!-- end::Head -->
<!-- end::Body -->
<body  class="m-page--loading-enabled m-page--loading m-content--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-light m-aside-left--fixed m-aside--offcanvas-default"  >
<!-- begin::Page loader -->
<div class="m-page-loader m-page-loader--base">
    <div class="m-spinner m-spinner--brand"></div>
</div>
<!-- end::Page Loader -->
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <!-- BEGIN: Header -->
    <header id="m_header" class="m-grid__item    m-header "  m-minimize-offset="200" m-minimize-mobile-offset="200" >
        <div class="m-container m-container--fluid m-container--full-height">
            <div class="m-stack m-stack--ver m-stack--desktop">
                <!-- BEGIN: Brand -->
                <div class="m-stack__item m-brand  m-brand--skin-light ">
                    <div class="m-stack m-stack--ver m-stack--general m-stack--fluid">
                        <div class="m-stack__item m-stack__item--center m-stack__item--middle m-brand__logo">
                            <a href="/index.html" class="m-brand__logo-wrapper">
                                <img alt="" src="/assetz/demo/demo11/media/img/logo/lighthouse.png"/>
                            </a>
                        </div>
                        <div class="m-stack__item m-stack__item--left m-stack__item--middle m-brand__tools">
                                <h3 style="color: #6167e6;font-family: Calibri;font-size: 5em;">
                                    Rule Based Modelling
                                </h3>
                        </div>
                    </div>
                </div>
                <!-- END: Brand -->
            </div>
        </div>
    </header>
    <!-- END: Header -->

    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <!-- BEGIN: Left Aside -->
        <button class="m-aside-left-close  m-aside-left-close--skin-light " id="m_aside_left_close_btn">
            <i class="la la-close"></i>
        </button>
        <div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-light ">
            <!-- BEGIN: Aside Menu -->
            <div
                    id="m_ver_menu"
                    class="m-aside-menu  m-aside-menu--skin-light m-aside-menu--submenu-skin-light "
                    m-menu-vertical="1"
                    m-menu-scrollable="1" m-menu-dropdown-timeout="500"
            >
                <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
                    <li class="m-menu__section m-menu__section--first">
                        <h4 class="m-menu__section-text">
                            Data Management
                        </h4>
                        <i class="m-menu__section-icon flaticon-more-v3"></i>
                    </li>
                    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
                        <a  href="loaddata" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon flaticon-layers"></i>
                            <span class="m-menu__link-text">
										Load New Data Set
									</span>
                            <i class="m-menu__ver-arrow la la-angle-right"></i>
                        </a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
                        <a  href="combinationRules" class="m-menu__link ">
                            <i class="m-menu__link-icon flaticon-suitcase"></i>
                            <span class="m-menu__link-text">
										Combine Data
									</span>
                        </a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
                        <a  href="datacombine" class="m-menu__link ">
                            <i class="m-menu__link-icon flaticon-suitcase"></i>
                            <span class="m-menu__link-text">
										Combine Data using Blockly
									</span>
                        </a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
                        <a  href="viewData" class="m-menu__link ">
                            <i class="m-menu__link-icon flaticon-suitcase"></i>
                            <span class="m-menu__link-text">
										View Data
									</span>
                        </a>
                    </li>

                    <li class="m-menu__section ">
                        <h4 class="m-menu__section-text">
                            Data Visualisation
                        </h4>
                        <i class="m-menu__section-icon flaticon-more-v3"></i>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
                        <a  href="visualise" class="m-menu__link ">
                            <i class="m-menu__link-icon flaticon-suitcase"></i>
                            <span class="m-menu__link-text">
										Overview
									</span>
                        </a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
                        <a  href="inner.html" class="m-menu__link ">
                            <i class="m-menu__link-icon flaticon-network"></i>
                            <span class="m-menu__link-text">
										Comparison
									</span>
                        </a>
                    </li>
                    <li class="m-menu__section ">
                        <h4 class="m-menu__section-text">
                            Data Exploration
                        </h4>
                        <i class="m-menu__section-icon flaticon-more-v3"></i>
                    </li>
                    <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class="m-menu__link m-menu__toggle">
                            <i class="m-menu__link-icon flaticon-clipboard"></i>
                            <span class="m-menu__link-text">Machine Learning Tools</span>
                            <i class="m-menu__ver-arrow la la-angle-right"></i>
                            <div class="m-menu__submenu " m-hidden-height="160" style="">
                                <#--<span class="m-menu__arrow"></span>-->
                                    <ul class="m-menu__subnav">
                                        <li class="m-menu__item " aria-haspopup="true">
                                            <a href="decisiontree" class="m-menu__link ">
                                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                            <span class="m-menu__link-text">Decision Trees</span>
                                            </a>
                                        </li>
                                    </ul>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- END: Aside Menu -->
        </div>
        <!-- END: Left Aside -->

        <@content.pageContent />

    </div>
    <!-- end:: Body -->
    <!-- begin::Footer -->
    <footer class="m-grid__item	 m-footer ">
        <div class="m-container m-container--fluid m-container--full-height m-page__container">
            <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
							<span class="m-footer__copyright">
								2017 &copy; Leiden University
								<a href="https://www.humanityx.nl/" class="m-link">
									Centre For Innovation
								</a>
							</span>
                </div>
            </div>
        </div>
    </footer>
    <!-- end::Footer -->
</div>
<!-- end:: Page -->
<!-- begin::Scroll Top -->
<div id="m_scroll_top" class="m-scroll-top">
    <i class="la la-arrow-up"></i>
</div>
<!-- end::Scroll Top -->		    <!-- begin::Quick Nav -->
<ul class="m-nav-sticky" style="margin-top: 30px;">
    <!--
    <li class="m-nav-sticky__item" data-toggle="m-tooltip" title="Showcase" data-placement="left">
        <a href="">
            <i class="la la-eye"></i>
        </a>
    </li>
    <li class="m-nav-sticky__item" data-toggle="m-tooltip" title="Pre-sale Chat" data-placement="left">
        <a href="" >
            <i class="la la-comments-o"></i>
        </a>
    </li>
    -->
    <li class="m-nav-sticky__item" data-toggle="m-tooltip" title="Purchase" data-placement="left">
        <a href="https://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes" target="_blank">
            <i class="la la-cart-arrow-down"></i>
        </a>
    </li>
    <li class="m-nav-sticky__item" data-toggle="m-tooltip" title="Documentation" data-placement="left">
        <a href="https://keenthemes.com/metronic/documentation.html" target="_blank">
            <i class="la la-code-fork"></i>
        </a>
    </li>
    <li class="m-nav-sticky__item" data-toggle="m-tooltip" title="Support" data-placement="left">
        <a href="https://keenthemes.com/forums/forum/support/metronic5/" target="_blank">
            <i class="la la-life-ring"></i>
        </a>
    </li>
</ul>
<#--<script src="/global/plugins/jquery.min.js" type="text/javascript"></script>-->
<script src="/assetz/vendors/base/vendors.bundle.js" type="text/javascript"></script>
<script src="/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>

<script src="/global/plugins/moment.min.js" type="text/javascript"></script>
<script src="/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>

<#--<script src="/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>-->
<script src="/global/plugins/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="/global/scripts/app.js" type="text/javascript"></script>
<#--<script src="/global/arif.js" type="text/javascript"></script>-->
<!--begin::Base Scripts -->

<script src="/assetz/demo/demo11/base/scripts.bundle.js" type="text/javascript"></script>
<!--end::Base Scripts -->
<!--begin::Page Vendors -->
<#--<script src="/assetz/vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>-->
<!--end::Page Vendors -->

<script>
    $(window).on('load', function() {
        $('body').removeClass('m-page--loading');
    });
</script>
<!-- end::Page Loader -->
<@content.pageScript />

</body>

</html>