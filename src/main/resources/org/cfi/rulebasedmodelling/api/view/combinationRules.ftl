<#import "keen.ftl" as k>

<#macro pageTitle>
    Lookout: Combining Data Sources
</#macro>

<#macro pageHead>
    <link rel="stylesheet" href="/assets/vendors/custom/codemirror/lib/codemirror.css">
    <link rel="stylesheet" href="/assets/vendors/custom/codemirror/theme/duotone-light.css">
    <link rel="stylesheet" href="/assets/vendors/custom/codemirror/addon/hint/show-hint.css">
    <style>
        .cm-style1 { color: #9c8f3d; }
        .cm-style2 { color: #1BBC9B; }
        .cm-style3 { color: #22bb44; }
        .cm-style4 { color: #0c91e5; }

    </style>
</#macro>

<#macro pageContent>
    <@k.subheader pagetitle="Data combination editor" level1="Prepare" level2="Recombine script"></@k.subheader>
    <!-- begin:: Content -->

    <div class="k-content k-grid__item k-grid__item--fluid" id="k_content">

        <div class="row">
            <div class="col-md-12">
                <div class="k-portlet">
                    <div class="k-portlet__head">
                        <div class="k-portlet__head-label">
                            <h3 class="k-portlet__head-title">
                                <input type="text" value="My Script" id="scriptName"
                                       style="width: 100%;height: 100%;border: none;overflow: hidden;text-overflow: ellipsis;font-family: Poppins;"
                                       readonly>
                            </h3>
                        </div>
                        <div class="k-portlet__head-toolbar">
                            <div class="k-portlet__head-group">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-pill btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Actions
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(89px, 32px, 0px);">
                                        <a href="#" class="dropdown-item" id="newScriptBtn" ><i class="la la-file"></i> New Script</a>
                                        <a href="#" class="dropdown-item" id="openScriptBtn"  data-toggle="modal" data-target="#openScriptModal"><i class="la la-folder-open-o"></i> Open Script</a>
                                        <a href="#" class="dropdown-item" id="openVersionBtn" data-toggle="modal" data-target="#openVersionModal"><i class="la la-history"></i> Open previous version</a>
                                        <a href="#" class="dropdown-item" id="saveScriptBtn"  data-toggle="modal" data-target="#saveScriptModal"><i class="la la-floppy-o" data-toggle="modal" data-target="#saveScriptModal"></i> Save script</a>
                                        <a href="#" class="dropdown-item" id="downloadScriptBtn" ><i class="la la-download"></i> Download script</a>
                                        <div class="dropdown-divider"></div>
                                        <a href="#" class="dropdown-item" onclick="getdemo()">Demo script 1</a>
                                        <a href="#" class="dropdown-item" onclick="getdemo2()">Demo script 2</a>
                                        <a href="#" class="dropdown-item" onclick="getdemo3()">Demo script 3</a>
                                    </div>
                                </div>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-pill btn-primary btn-sm" id="load">
                                        Create table
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="k-portlet__body">
                        <div class="k-portlet__content">
                            <div id="scriptEditor"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>

</#macro>

<#macro pageScript>
    <script src="/assets/vendors/custom/codemirror/lib/codemirror.js"></script>
    <script src="/assets/vendors/custom/codemirror/addon/mode/overlay.js"></script>
    <script src="/assets/vendors/custom/codemirror/addon/edit/matchbrackets.js"></script>
    <script src="/assets/vendors/custom/codemirror/mode/yaml/yaml.js"></script>
    <script src="/assets/vendors/custom/codemirror/mode/javascript/javascript.js"></script>
    <script src="/assets/vendors/custom/codemirror/addon/hint/show-hint.js"></script>
    <script src="/assets/vendors/custom/codemirror/addon/hint/anyword-hint.js"></script>
    <!-- Code Editor scripts -->
    <script>

        CodeMirror.defineMode("keywordYaml", function(config,parserConfig){
            var keywordYamlOverlay = {
                token: function(stream,state) {
                    if (stream.match("MULTIPLY") ) {
                        return "style1";
                    } else if (stream.match("SUM") ) {
                        return "style1";
                    } else if (stream.match("AVG") ) {
                        return "style2";
                    } else if (stream.match("COUNT") ) {
                        return "style2";
                    } else if (stream.match("COMBINE") ) {
                        return "style3";
                    } else {
                        stream.next();
                        return null;
                    }
                }
            };
            return CodeMirror.overlayMode(CodeMirror.getMode(config, parserConfig.backdrop || "text/yaml"), keywordYamlOverlay);
        });

        var editor = CodeMirror(document.getElementById("scriptEditor"),{
            mode:"application/json",
            theme:"duotone-light",
            lineNumbers: true,
            tabSize:7,
            matchBrackets: true,
            // extraKeys: {".": "autocomplete"}
        });

        editor.setSize(null,500);

        function setScriptName(name) {
            $('#scriptName').val(name)
            localStorage.setItem('scriptName', name);
        }

        function getText(){
            return editor.getValue()
        }

        function setText(code){
            return editor.getDoc().setValue(code)
        }

        $('#newScriptBtn').click(function () {
            editor.setText("")
                setScriptName("New script")
            }
        )

        $('#downloadScriptBtn').click(e =>
            saveAs(new Blob([getText()], {type: "text/plain;charset=utf-8"}), $('#scriptName').val()+".ccs")
        )




        let openDocDialog = new OpenDocDialog('body', 'openScriptModal', 'ccs', 'Open script', function (name, src) {
            setText(src)
            setScriptName(name)
        });

        let openVerionDocDialog = new OpenHistoryDocDialog('body', 'openVersionModal', 'ccs', function () {
            return openDocDialog.getOpenDocName()
        }, 'Open previous version', function (name, src) {
            setText(src)
        });

        let saveDocDialog = new SaveDocDialog('body', 'saveScriptModal', 'ccs', 'Save script',
            function () {
                return getText()
            },
            function () {
                return $('#scriptName').val()
            },
            function (name) {
                setScriptName(name)
                $.cfi.notify('Script saved: ' + name, 'success')
            },
            function (name, e) {
                const resp = JSON.parse(e.responseText);
                if (resp.message) {
                    const message = resp.message;
                    let lineNr = resp.line - 1;

                    $.cfi.notify('There is an error in your script.<br>' + resp.message, 'danger')
                    editor.setErrorMessage(lineNr, message)
                } else {
                    $.cfi.notify('The script could not be saved.<br>' + e, 'danger')
                }
            }
        );
        setScriptName(localStorage.getItem('scriptName') || 'New script')
        


        var tables;
        var columns;
        $.get('/api/v1/datacombination/data/datasetInfo', {}, function(data) {
            tables = Object.keys(data);
            columns = data;
        });


        findTable = function(word, namelist){
            if(word == 'table'){
                for(var i = 0; i < tables.length; i++){
                    namelist.push(tables[i])
                }
                return namelist;
            }
            if(tables.includes(word)){
                var colArray = columns[word];
                for(var i = 0; i < colArray.length; i++){
                    namelist.push(word+"."+colArray[i])
                }
                return namelist;
            }
            if(word!='table' & !tables.includes(word)){
                namelist.push(word + ".");
                return namelist;
            }
            return namelist;
        }




        // var orig = CodeMirror.hint.anyword;
        // CodeMirror.hint.anyword = function(cm) {
        //     var fulltext = cm.getValue().split("\n");
        //     var inner = orig(cm) || {from: cm.getCursor(), to: cm.getCursor(), list: []};
        //     var lastWord = fulltext[fulltext.length - 1].split(" ");
        //     var interest = lastWord[lastWord.length - 1];
        //     inner.list = findTable(interest,inner.list);
        //     return inner;
        // };

        getDefinitions = function(e) {
            var temp = editor.getValue();
            var $this = $(e);
            $this.button('loading');
            if(temp === ""){
                $this.button('reset');
                // buildSelect;
            }
            else {
                $.when($.get('/api/v1/datacombination/data/info', {
                    queryinfo: temp
                })).done(function () {
                    $this.button('reset');
                    // buildSelect;
                });
            }
        };

        getdemo = function() {
            var txt = editor.getValue();
            if(txt != undefined){
                txt = "";
            }

            txt = "{\n" +
                "  \"combine_1\":\n" +
                "  {\n" +
                "    \"tablelist\":[\n" +
                "      {\n" +
                "        \"table\":\"acled\",\n" +
                "        \"columns\":[\"event_date\", \"fatalities\", \"event_type\"],\n" +
                "        \"by\":\n" +
                "              { \n" +
                "                \"join\" : [\"event_date\"],\n" +
                "                \"agg\" : [\"event_date.week\", \"event_type\"]\n" +
                "              },\n" +
                "        \"function\":\"SUM\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"table\":\"yemen_rainfall\",\n" +
                "        \"columns\":[\"date\",\"mean\"],\n" +
                "        \"by\":\n" +
                "              { \n" +
                "                \"join\" : [\"date\"],\n" +
                "                \"agg\" : [\"date.week\"]\n" +
                "              },\n" +
                "        \"function\":\"AVG\"\n" +
                "      }\n" +
                "    ],\n" +
                "    \"by\":[\"date\"],\n" +
                "    \"tableName\":\"rainfall_and_events\"\n" +
                "  },\n" +
                " \"filter_1\":\n" +
                "  {\n" +
                "    \"tableToFilter\":\"rainfall_and_events\",\n" +
                "    \"tableName\":\"rainfall_and_events_filtered\",\n" +
                "    \"pathlist\":[\n" +
                "      [\n" +
                "        {\n" +
                "          \"column\":\"date\",\n" +
                "          \"filter\":\"BETWEEN\",\n" +
                "          \"value\":[\"2010-01-01\",\"2018-06-30\"]\n" +
                "        },\n" +
                "        {\n" +
                "          \"column\":\"fatalities\",\n" +
                "          \"filter\":\">\",\n" +
                "          \"value\":\"5\"\n" +
                "        }\n" +
                "      ],\n" +
                "      [\n" +
                "        {\n" +
                "          \"column\":\"date\",\n" +
                "          \"filter\":\"BETWEEN\",\n" +
                "          \"value\":[\"2010-01-01\",\"2018-06-30\"]\n" +
                "        },\n" +
                "        {\n" +
                "          \"column\":\"mean\",\n" +
                "          \"filter\":\">\",\n" +
                "          \"value\":\"5\"\n" +
                "        }\n" +
                "      ]\n" +
                "    ]\n" +
                "  },\n" +
                "  \"derived_data_1\":\n" +
                "    {\n" +
                "      \"table\":\"rainfall_and_events_filtered\",\n" +
                "      \"columnName\":\"class\",\n" +
                "      \"datatype\":\"text\",\n" +
                "      \"conditionals\":[\n" +
                "        {\n" +
                "          \"green\":[\n" +
                "                [\n" +
                "                  {\n" +
                "                    \"column\":\"fatalities\",\n" +
                "                    \"filter\":\"<\",\n" +
                "                    \"value\":\"2500\"\n" +
                "                  }\n" +
                "                ]\n" +
                "         ]\n" +
                "        },\n" +
                "        {\n" +
                "          \"orange\":[\n" +
                "                [\n" +
                "                  {\n" +
                "                    \"column\":\"fatalities\",\n" +
                "                    \"filter\":\">=\",\n" +
                "                    \"value\":\"2500\"\n" +
                "                  },\n" +
                "                  {\n" +
                "                    \"column\":\"mean\",\n" +
                "                    \"filter\":\"BETWEEN\",\n" +
                "                    \"value\":[0,5]\n" +
                "                  }\n" +
                "                ]\n" +
                "          ]\n" +
                "        },\n" +
                "        {\n" +
                "          \"red\":[\n" +
                "                [\n" +
                "                  {\n" +
                "                    \"column\":\"fatalities\",\n" +
                "                    \"filter\":\">=\",\n" +
                "                    \"value\":\"2500\"\n" +
                "                  },\n" +
                "                  {\n" +
                "                    \"column\":\"mean\",\n" +
                "                    \"filter\":\">\",\n" +
                "                    \"value\":\"5\"\n" +
                "                  }\n" +
                "                ]\n" +
                "          ]\n" +
                "        }\n" +
                "      ]\n" +
                "    }\n" +
                "}";
            editor.setValue(txt);
        }

        getdemo2 = function() {
            var txt = editor.getValue();
            if(txt != undefined){
                txt = "";
            }

            txt = "{\n" +
                "  \"combine_1\":\n" +
                "  {\n" +
                "    \"tablelist\":[\n" +
                "      {\n" +
                "        \"table\":\"acled\",\n" +
                "        \"columns\":[\"event_date\", \"fatalities\", \"event_type\"],\n" +
                "        \"by\":\n" +
                "              { \n" +
                "                \"join\" : [\"event_date\"],\n" +
                "                \"agg\" : [\"event_date.week\", \"event_type\"]\n" +
                "              },\n" +
                "        \"function\":\"SUM\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"table\":\"yemen_rainfall\",\n" +
                "        \"columns\":[\"date\",\"mean\"],\n" +
                "        \"by\":\n" +
                "              { \n" +
                "                \"join\" : [\"date\"],\n" +
                "                \"agg\" : [\"date.week\"]\n" +
                "              },\n" +
                "        \"function\":\"AVG\"\n" +
                "      }\n" +
                "    ],\n" +
                "    \"by\":[\"date\"],\n" +
                "    \"tableName\":\"rainfall_and_events\"\n" +
                "  },\n" +
                " \"filter_1\":\n" +
                "  {\n" +
                "    \"tableToFilter\":\"rainfall_and_events\",\n" +
                "    \"tableName\":\"rainfall_and_events_filtered\",\n" +
                "    \"pathlist\":[\n" +
                "      [\n" +
                "        {\n" +
                "          \"column\":\"date\",\n" +
                "          \"filter\":\"BETWEEN\",\n" +
                "          \"value\":[\"2018-01-01\",\"2018-06-30\"]\n" +
                "        },\n" +
                "        {\n" +
                "          \"column\":\"fatalities\",\n" +
                "          \"filter\":\">\",\n" +
                "          \"value\":\"5\"\n" +
                "        }\n" +
                "      ],\n" +
                "      [\n" +
                "        {\n" +
                "          \"column\":\"date\",\n" +
                "          \"filter\":\"BETWEEN\",\n" +
                "          \"value\":[\"2018-01-01\",\"2018-06-30\"]\n" +
                "        },\n" +
                "        {\n" +
                "          \"column\":\"mean\",\n" +
                "          \"filter\":\">\",\n" +
                "          \"value\":\"0.2\"\n" +
                "        }\n" +
                "      ]\n" +
                "    ]\n" +
                "  },\n" +
                "  \"derived_data_1\":\n" +
                "    {\n" +
                "      \"table\":\"rainfall_and_events\",\n" +
                "      \"columnName\":\"calc\",\n" +
                "      \"datatype\":\"text\",\n" +
                "      \"operators\":\n" +
                "      {\n" +
                "        \"column\": \"fatalities\",\n" +
                "        \"operator\": \"*\",\n" +
                "        \"value\": \n" +
                "        {\n" +
                "          \"column\": \"mean\",\n" +
                "          \"operator\": \"+\",\n" +
                "          \"value\": \"fatalities\"\n" +
                "        }\n" +
                "      }\n" +
                "    },\n" +
                "  \"derived_data_2\":\n" +
                "    {\n" +
                "      \"table\":\"rainfall_and_events_filtered\",\n" +
                "      \"columnName\":\"class\",\n" +
                "      \"datatype\":\"text\",\n" +
                "      \"conditionals\":[\n" +
                "        {\n" +
                "          \"green\":[\n" +
                "                [\n" +
                "                  {\n" +
                "                    \"column\":\"fatalities\",\n" +
                "                    \"filter\":\"<\",\n" +
                "                    \"value\":\"2500\"\n" +
                "                  }\n" +
                "                ]\n" +
                "         ]\n" +
                "        },\n" +
                "        {\n" +
                "          \"orange\":[\n" +
                "                [\n" +
                "                  {\n" +
                "                    \"column\":\"fatalities\",\n" +
                "                    \"filter\":\">=\",\n" +
                "                    \"value\":\"2500\"\n" +
                "                  },\n" +
                "                  {\n" +
                "                    \"column\":\"mean\",\n" +
                "                    \"filter\":\"BETWEEN\",\n" +
                "                    \"value\":[0,2]\n" +
                "                  }\n" +
                "                ]\n" +
                "          ]\n" +
                "        },\n" +
                "        {\n" +
                "          \"red\":[\n" +
                "                [\n" +
                "                  {\n" +
                "                    \"column\":\"fatalities\",\n" +
                "                    \"filter\":\">=\",\n" +
                "                    \"value\":\"2500\"\n" +
                "                  },\n" +
                "                  {\n" +
                "                    \"column\":\"mean\",\n" +
                "                    \"filter\":\">\",\n" +
                "                    \"value\":\"2\"\n" +
                "                  }\n" +
                "                ]\n" +
                "          ]\n" +
                "        }\n" +
                "      ]\n" +
                "    }\n" +
                "}";
            editor.setValue(txt);
        }

        getdemo3 = function() {
            var txt = editor.getValue();
            if(txt != undefined){
                txt = "";
            }

            txt = "{\n" +
                "   \"filter_1\":\n" +
                "  {\n" +
                "    \"tableToFilter\":\"yemen_market\",\n" +
                "    \"tableName\":\"yemen_market_wheat\",\n" +
                "    \"pathlist\":[\n" +
                "      [\n" +
                "        {\n" +
                "          \"column\":\"cmname\",\n" +
                "          \"filter\":\"=\",\n" +
                "          \"value\": \"\\\\'Wheat flour - Retail\\\\'\"\n" +
                "        }\n" +
                "      ],\n" +
                "      [\n" +
                "        {\n" +
                "          \"column\":\"cmname\",\n" +
                "          \"filter\":\"=\",\n" +
                "          \"value\": \"\\\\'Wheat - Retail\\\\'\"\n" +
                "        }\n" +
                "      ]\n" +
                "     ]\n" +
                "  },\n" +
                "  \"filter_2\":\n" +
                "  {\n" +
                "    \"tableToFilter\":\"yemen_market\",\n" +
                "    \"tableName\":\"yemen_market_salt\",\n" +
                "    \"pathlist\":[\n" +
                "      [\n" +
                "        {\n" +
                "          \"column\":\"cmname\",\n" +
                "          \"filter\":\"=\",\n" +
                "          \"value\": \"\\\\'Salt - Retail\\\\'\"\n" +
                "        }\n" +
                "      ]\n" +
                "     ]\n" +
                "  },\n" +
                "  \"combine_1\":\n" +
                "  {\n" +
                "    \"tablelist\":[\n" +
                "      {\n" +
                "        \"table\":\"yemen_market_wheat\",\n" +
                "        \"columns\":[\"date\", \"price\", \"cmname\", \"mktname\"],\n" +
                "        \"by\":\n" +
                "              { \n" +
                "                \"join\" : [\"date\"],\n" +
                "                \"agg\" : [\"date\", \"cmname\", \"mktname\"]\n" +
                "              },\n" +
                "        \"function\":\"AVG\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"table\":\"yemen_market_salt\",\n" +
                "        \"columns\":[\"date\", \"price\", \"cmname\", \"mktname\"],\n" +
                "        \"by\":\n" +
                "              { \n" +
                "                \"join\" : [\"date\"],\n" +
                "                \"agg\" : [\"date\",\"cmname\", \"mktname\"]\n" +
                "              },\n" +
                "        \"function\":\"AVG\"\n" +
                "      }\n" +
                "    ],\n" +
                "    \"by\":[\"date\"],\n" +
                "    \"tableName\":\"wheat_and_salt\"\n" +
                "  },\n" +
                "  \"filter_3\":\n" +
                "  {\n" +
                "    \"tableToFilter\":\"wheat_and_salt\",\n" +
                "    \"tableName\":\"yemen_wheat_and_salt\",\n" +
                "    \"pathlist\":[\n" +
                "      [\n" +
                "        {\n" +
                "          \"column\":\"mktname\",\n" +
                "          \"filter\":\"=\",\n" +
                "          \"value\": \"mktname_1\"\n" +
                "        }\n" +
                "      ]\n" +
                "     ]\n" +
                "  }\n" +
                "}";
            editor.setValue(txt);
        }


        // buildSelect = function(){
        //     $.get('/api/v1/datacombination/data/availableTables',{},function(options) {
        //                 removeOptions(document.getElementById("tables"));
        //                 // var option = $('<option>');
        //                 var o = new Option('select', 'select');
        //                 // option.attr('select', 'select').text('select');
        //                 $('#tables').append(o);
        //                 for (var val in options) {
        //                     var o = new Option(options[val], options[val]);
        //                     $(o).html(options[val]);
        //                     // option.attr(options[val], options[val]).text(options[val]);
        //                     $('#tables').append(o);
        //                 }
        //             }
        //         )
        // }
        //
        // removeOptions = function(selectbox)
        // {
        //     var i;
        //     for(i = selectbox.options.length - 1 ; i >= 0 ; i--)
        //     {
        //         selectbox.remove(i);
        //     }
        // }
        //
        // buildSelect();


    </script>

</#macro>