Blockly.defineBlocksWithJsonArray(
    [{
        "type": "combine",
        "message0": "Combine Tables %1 Table 1: %2 Table 2: %3 %4 %5 join by: %6",
        "args0": [
            {
                "type": "input_dummy"
            },
            {
                "type": "input_value",
                "name": "TABLE1",
                "check": "table",
                "align": "RIGHT"
            },
            {
                "type": "input_value",
                "name": "TABLE2",
                "check": "table",
                "align": "RIGHT"
            },
            {
                "type": "field_input",
                "name": "CombinedName",
                "text": "New Table Name"
            },
            {
                "type": "input_dummy"
            },
            {
                "type": "field_input",
                "name": "JOINBY",
                "text": "Join By"
            }
        ],
        "inputsInline": false,
        "nextStatement": null,
        "colour": 230,
        "tooltip": "Add tables to combine",
        "helpUrl": ""
    },
        {
            "type": "table",
            "message0": "%1 %2",
            "args0": [
                {
                    "type": "field_input",
                    "name": "TableName",
                    "text": "table name"
                },
                {
                    "type": "input_value",
                    "name": "columns",
                    "check": "columns"
                }
            ],
            "inputsInline": false,
            "output": null,
            "colour": 315,
            "tooltip": "Table to be combined",
            "helpUrl": ""
        },
        {
            "type": "columns",
            "message0": "%1 %2",
            "args0": [
                {
                    "type": "field_input",
                    "name": "columnList",
                    "text": "column list"
                },
                {
                    "type": "input_value",
                    "name": "ColumnList",
                    "check": "by"
                }
            ],
            "inputsInline": false,
            "output": null,
            "colour": 65,
            "tooltip": "",
            "helpUrl": ""
        },
        {
            "type": "by",
            "message0": "%1",
            "args0": [
                {
                    "type": "field_input",
                    "name": "joinColumn",
                    "text": "column to join on"
                }
            ],
            "inputsInline": false,
            "output": null,
            "colour": 90,
            "tooltip": "",
            "helpUrl": ""
        },
        {
            "type": "condition",
            "lastDummyAlign0": "CENTRE",
            "message0": "%1 %2 %3 %4 %5 %6 Condtion",
            "args0": [
                {
                    "type": "field_input",
                    "name": "COLUMN",
                    "text": "Column"
                },
                {
                    "type": "input_dummy"
                },
                {
                    "type": "field_dropdown",
                    "name": "RELATION",
                    "options": [
                        [
                            ">",
                            "GreaterThan"
                        ],
                        [
                            ">=",
                            "GreaterThanOrEqual"
                        ],
                        [
                            "<",
                            "LessThan"
                        ],
                        [
                            "<=",
                            "LessThanOrEqual"
                        ],
                        [
                            "=",
                            "Equals"
                        ],
                        [
                            "BETWEEN",
                            "BETWEEN"
                        ]
                    ]
                },
                {
                    "type": "input_dummy",
                    "align": "CENTRE"
                },
                {
                    "type": "field_input",
                    "name": "VALUE",
                    "text": "Value"
                },
                {
                    "type": "input_dummy",
                    "align": "CENTRE"
                }
            ],
            "inputsInline": false,
            "output": "",
            "colour": 90,
            "tooltip": "",
            "helpUrl": ""
        },
        {
            "type": "and",
            "message0": "%1 AND %2 %3",
            "args0": [
                {
                    "type": "input_value",
                    "name": "AND",
                    "check": "condition"
                },
                {
                    "type": "input_dummy"
                },
                {
                    "type": "input_value",
                    "name": "NAME",
                    "check": [
                        "and",
                        "condition"
                    ]
                }
            ],
            "inputsInline": true,
            "output": null,
            "colour": 315,
            "tooltip": "",
            "helpUrl": ""
        }
    ]
);

Blockly.Blocks['filter'] = {
    /**
     * Block for creating a list with any number of elements of any type.
     * @this Blockly.Block
     */
    init: function() {
        this.setHelpUrl("");
        this.setColour(Blockly.Msg['LISTS_HUE']);
        this.itemCount_ = 1;
        this.appendDummyInput().appendField('Filter');
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.updateShape_();
        this.setMutator(new Blockly.Mutator(['filter_item']));
        this.setTooltip('Create a filter list');
        this.inputsInline = true;
    },
    /**
     * Create XML to represent list inputs.
     * @return {!Element} XML storage element.
     * @this Blockly.Block
     */
    mutationToDom: function() {
        var container = document.createElement('mutation');
        container.setAttribute('items', this.itemCount_);
        return container;
    },
    /**
     * Parse XML to restore the list inputs.
     * @param {!Element} xmlElement XML storage element.
     * @this Blockly.Block
     */
    domToMutation: function(xmlElement) {
        this.itemCount_ = parseInt(xmlElement.getAttribute('items'), 10);
        this.updateShape_();
    },
    /**
     * Populate the mutator's dialog with this block's components.
     * @param {!Blockly.Workspace} workspace Mutator's workspace.
     * @return {!Blockly.Block} Root block in mutator.
     * @this Blockly.Block
     */
    decompose: function(workspace) {
        var containerBlock = workspace.newBlock('filter_container');
        containerBlock.initSvg();
        // var connection = containerBlock
        var connection = containerBlock.getInput('FILTER').connection;
        for (var i = 0; i < this.itemCount_; i++) {
            var itemBlock = workspace.newBlock('filter_item');
            itemBlock.initSvg();
            connection.connect(itemBlock.previousConnection);
            connection = itemBlock.nextConnection;
        }
        return containerBlock;
    },
    /**
     * Reconfigure this block based on the mutator dialog's components.
     * @param {!Blockly.Block} containerBlock Root block in mutator.
     * @this Blockly.Block
     */
    compose: function(containerBlock) {
        var itemBlock = containerBlock.getInputTargetBlock('FILTER');
        // Count number of inputs.
        var connections = [];
        while (itemBlock) {
            connections.push(itemBlock.valueConnection_);
            itemBlock = itemBlock.nextConnection &&
                itemBlock.nextConnection.targetBlock();
        }
        // Disconnect any children that don't belong.
        for (var i = 0; i < this.itemCount_; i++) {
            var connection = this.getInput('ADD' + i).connection.targetConnection;
            if (connection && connections.indexOf(connection) == -1) {
                connection.disconnect();
            }
        }
        this.itemCount_ = connections.length;
        this.updateShape_();
        // Reconnect any child blocks.
        for (var i = 0; i < this.itemCount_; i++) {
            Blockly.Mutator.reconnect(connections[i], this, 'ADD' + i);
        }
    },
    /**
     * Store pointers to any connected child blocks.
     * @param {!Blockly.Block} containerBlock Root block in mutator.
     * @this Blockly.Block
     */
    saveConnections: function(containerBlock) {
        var itemBlock = containerBlock.getInputTargetBlock('FILTER');
        var i = 0;
        while (itemBlock) {
            var input = this.getInput('ADD' + i);
            itemBlock.valueConnection_ = input && input.connection.targetConnection;
            i++;
            itemBlock = itemBlock.nextConnection &&
                itemBlock.nextConnection.targetBlock();
        }
    },
    /**
     * Modify this block to have the correct number of inputs.
     * @private
     * @this Blockly.Block
     */
    updateShape_: function() {
        if (this.itemCount_ && this.getInput('EMPTY')) {
            this.removeInput('EMPTY');
        } else if (!this.itemCount_ && !this.getInput('EMPTY')) {
        }
        // Add new inputs.
        for (var i = 0; i < this.itemCount_; i++) {
            if (!this.getInput('ADD' + i)) {
                var input = this.appendValueInput('ADD' + i);
            }
        }
        // Remove deleted inputs.
        while (this.getInput('ADD' + i)) {
            this.removeInput('ADD' + i);
            i++;
        }
    }
};

Blockly.Blocks['filter_container'] = {
    /**
     * Mutator block for list container.
     * @this Blockly.Block
     */
    init: function() {
        this.setColour(Blockly.Msg['LISTS_HUE']);
        this.appendDummyInput()
            .appendField('Filter');
        this.appendStatementInput('FILTER');
        this.setTooltip('Create a filter');
        this.contextMenu = false;
    }
};

Blockly.Blocks['filter_item'] = {
    /**
     * Mutator block for adding items.
     * @this Blockly.Block
     */
    init: function() {
        this.setColour(Blockly.Msg['LISTS_HUE']);
        this.appendValueInput('condition_item')
            .appendField('condition');
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip('Conditions to be added');
        this.contextMenu = false;
    }
};