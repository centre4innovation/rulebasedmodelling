Blockly.JavaScript['combine'] = function(block) {
    var value_table1 = Blockly.JavaScript.valueToCode(block, 'TABLE1', Blockly.JavaScript.ORDER_ATOMIC);
    var value_table2 = Blockly.JavaScript.valueToCode(block, 'TABLE2', Blockly.JavaScript.ORDER_ATOMIC);
    var text_combinedname = block.getFieldValue('CombinedName');
    var text_joinby = block.getFieldValue('JOINBY');


    var overall = "{\"tablelist\":[" + value_table1 + "," + value_table2 + "],\"by\":\"" + text_joinby + "\", \"tableName\":\"" + text_combinedname + "\"}";
    var code = overall;
    return code;
};

Blockly.JavaScript['table'] = function(block) {
    var text_tablename = block.getFieldValue('TableName');
    var value_columns = Blockly.JavaScript.valueToCode(block, 'columns', Blockly.JavaScript.ORDER_ATOMIC);

    var cols =  value_columns.split(";")
    var code = "{ \"table\":\"" + text_tablename + "\"," + "\"columns\":\"[" + cols[0] + "]\"" + ", \"by\":\""+ cols[1] +"\"}";
    return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['columns'] = function(block) {
    var text_columnlist = block.getFieldValue('columnList');
    var value_columnlist = Blockly.JavaScript.valueToCode(block, 'ColumnList', Blockly.JavaScript.ORDER_ATOMIC);
    var code = text_columnlist + ";" + value_columnlist;
    return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['by'] = function(block) {
    var text_joincolumn = block.getFieldValue('joinColumn');
    var code = text_joincolumn;
    return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['filter'] = function(block) {
    var text_column = block.getFieldValue('COLUMN');
    var dropdown_relation = block.getFieldValue('RELATION');
    var text_value = block.getFieldValue('VALUE');
    // TODO: Assemble JavaScript into code variable.
    var code = '...';
    // TODO: Change ORDER_NONE to the correct strength.
    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['condition'] = function(block) {
    var text_column = block.getFieldValue('COLUMN');
    var dropdown_relation = block.getFieldValue('RELATION');
    var text_value = block.getFieldValue('VALUE');
    // TODO: Assemble JavaScript into code variable.
    var code = '...';
    // TODO: Change ORDER_NONE to the correct strength.
    return [code, Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript['and'] = function(block) {
    var value_and = Blockly.JavaScript.valueToCode(block, 'AND', Blockly.JavaScript.ORDER_ATOMIC);
    var value_name = Blockly.JavaScript.valueToCode(block, 'NAME', Blockly.JavaScript.ORDER_ATOMIC);
    // TODO: Assemble JavaScript into code variable.
    var code = '...';
    // TODO: Change ORDER_NONE to the correct strength.
    return [code, Blockly.JavaScript.ORDER_NONE];
};
