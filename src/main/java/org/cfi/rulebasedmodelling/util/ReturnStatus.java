package org.cfi.rulebasedmodelling.util;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Return status with message
 * @author Arvid Halma
 * @version 5-4-2016
 */
public class ReturnStatus {
    @JsonProperty
    private boolean success;
    @JsonProperty
    private String message;

    public ReturnStatus(boolean success, String message) {
        this.success = success;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "ReturnStatus{" +
                "success=" + success +
                ", message='" + message + '\'' +
                '}';
    }
}
