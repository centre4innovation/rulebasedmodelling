package org.cfi.rulebasedmodelling.util;

import java.util.Iterator;
import java.util.Objects;
import java.util.function.Predicate;

/**
 * Wrapper for filtering iterators.
 * See: http://codereview.stackexchange.com/questions/112109/filtered-iterator
 * @author Arvid Halma
 * @version 4-8-2016 - 14:11
 */
public final class FilteredIterator<E> implements Iterator<E> {
    private final Iterator<E> iterator;
    private final Predicate<E> filter;

    private boolean hasNext = true;
    private E next;

    public FilteredIterator(final Iterator<E> iterator, final Predicate<E> filter) {
        this.iterator = iterator;
        Objects.requireNonNull(iterator);

        if (filter == null) {
            this.filter = new AcceptAllFilter<E>();
        } else {
            this.filter = filter;
        }

        this.findNext();
    }

    @Override
    public boolean hasNext() {
        return this.next != null;
    }

    @Override
    public E next() {
        E returnValue = this.next;
        this.findNext();
        return returnValue;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }

    private void findNext() {
        while (this.iterator.hasNext()) {
            this.next = iterator.next();
            if (this.filter.test(this.next)) {
                return;
            }
        }
        this.next = null;
        this.hasNext = false;
    }

    private static final class AcceptAllFilter<T> implements Predicate<T> {
        public boolean test(final T type) {
            return true;
        }
    }
}