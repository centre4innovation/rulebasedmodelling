package org.cfi.rulebasedmodelling.ml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.cfi.rulebasedmodelling.api.Config;
import org.cfi.rulebasedmodelling.postgres.DataImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import weka.classifiers.Evaluation;
import weka.core.Attribute;
import weka.core.Debug;
import weka.core.Instances;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.*;
import java.util.stream.Collectors;

public class Label {

    private final static Logger logger = LoggerFactory.getLogger(Label.class);

    private ObjectMapper objectMapper;

    private String name;
    private String featureName;
    private double baselineAccuracy;

    private ModelGenerator mg = new ModelGenerator();
    private J48Batch tree;
    private Tree trainedModel;

    private double accuracy;
    private double error;
    private double truePos;
    private double falsePos;
    private double falseNeg;
    private List<Map.Entry<String, Double>> featureImportance = Lists.newArrayList();
    private Map<String,List<String>> labelPaths = Maps.newLinkedHashMap();

    private String modelName;
    private int targetIndex;
    private String targetValues;
    private List<String> featuresToUse = Lists.newArrayList();


    /**Class used to hold data and manage training related to a feature of interest**/
    public Label(String name, String featureName, Instances data, int minLeafSize,
                 boolean pruned, String modelName, int targetIndex, ObjectMapper objectMapper) throws Exception {
        this.objectMapper = objectMapper;


        this.name = name;
        this.targetIndex = targetIndex;
        this.featureName = featureName;
        this.modelName = modelName + "-" + featureName + "-" + name.replaceAll(" ","_") + "-d" + minLeafSize;
        calculateBaselineAccuracy(data, featureName);
        runModelCreation(data, minLeafSize, pruned);
    }

    /**Class used to hold data and manage training related to a feature of interest**/
    public Label(String featureName, Instances data, int minLeafSize, boolean pruned,
                 String modelName, int targetIndex, ObjectMapper objectMapper) throws Exception {
        this("All Classes", featureName, data, minLeafSize, pruned, modelName, targetIndex, objectMapper);
    }

    /**Get name of value of interest from feature.
     * Could be Multi-Class of a single value if doing binary classification
     * **/
    public String getName() {
        return name;
    }

    /**Calculate the baseline accuracy. This looks for the most commonly occuring value
     * in the feature of interest and uses its count compared to the total number of rows
     * for an accuracy
     * @param data weka Instances created from the data set
     * @param featureName feature of interest
     ***/
    private void calculateBaselineAccuracy(Instances data, String featureName){
        int[] valueCounts = data.attributeStats(data.attribute(featureName).index()).nominalCounts;
        if(name.equals("All Classes")){
            int maxCount = -1;
            int countSum = 0;
            for(int c : valueCounts){
                maxCount = c > maxCount ? c : maxCount;
                countSum += c;
            }
            this.baselineAccuracy = maxCount * 100.0 / countSum;
        }
        else {
            int val = data.attribute(featureName).indexOfValue(this.name);
            this.baselineAccuracy = valueCounts[val] * 100.0 / (valueCounts[0] + valueCounts[1]);
        }
    }

    /**Get baseline accuracy**/
    public double getBaselineAccuracy() {
        return baselineAccuracy;
    }

    /**Method to train decision trees
     * @param data weka Instances of data set
     * @param minLeafSize minimum number of data points allowed in a leaf
     * @param pruned use of post pruning**/
    private void runModelCreation(Instances data, int minLeafSize, boolean pruned) throws Exception {
        logger.info("evaluating model... Feature: " + featureName + ", Label: " + name);
        int trainSize = (int) Math.round(data.numInstances() * 0.8);
        int testSize = data.numInstances() - trainSize;
        data.randomize(new Debug.Random(1));// if you comment this line the accuracy of the model will be dropped from 96.6% to 80%


        Instances training = new Instances(data, 0, trainSize);
        Instances test = new Instances(data, trainSize, testSize);
        training.setClassIndex(training.attribute(featureName).index());
        test.setClassIndex(training.attribute(featureName).index());

        List<String> cvs = Lists.newArrayList();
        Enumeration<Object> classVals = training.classAttribute().enumerateValues();
        while(classVals.hasMoreElements()){
            String cv = (String) classVals.nextElement();
            cvs.add(cv);
        }
        this.targetValues = "{" + String.join(",",cvs) + "}";

        Enumeration<Attribute> ftres = training.enumerateAttributes();
        while(ftres.hasMoreElements()){
            String fn = ftres.nextElement().name();
            if(!fn.equals(featureName)) {
                this.featuresToUse.add(fn);
            }
        }

        this.tree = (J48Batch) this.mg.buildClassifier(training, minLeafSize, pruned);

        this.trainedModel = WekaTreeParser.parse(this.tree.toString());
        this.labelPaths = getLabelPaths();

        this.featureImportance.addAll(featureImportance(tree, training, test));

        KFoldValidationEvaluation kf = new KFoldValidationEvaluation(10);
        kf.evaluateTrainedModel(training, this.tree);

        this.accuracy = kf.getAverageAccuracy();
        this.error = kf.getAccuracySD();
        this.truePos = kf.getTruePositive();
        this.falsePos = kf.getFalsePositive();
        this.falseNeg = kf.getFalseNegative();
    }

    /**Save trained decision tree model
     * @param config extension of dropwizard configuration object
     * @param colVals column names
     * @param colTypes column types
     * @param missingIndicies tracks missing original columns
     * @exception IOException**/
    public void saveModel(Config config, List<String> colVals, List<String> colTypes,
                          List<Integer> missingIndicies, boolean useDateProperties) throws IOException{
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("data/models/" + this.modelName + ".model"));
        oos.writeObject(this.tree);
        oos.flush();
        oos.close();

        int newTarget = targetIndex;
        for(int mi : missingIndicies) {
            if (targetIndex > mi) {
                newTarget--;
            }
        }

        new DataImport(config).writeModelInformation(this.modelName,
                String.join(",", colVals),
                String.join(",",colTypes),
                this.featureName,
                newTarget,
                this.targetValues,
                objectMapper.writeValueAsString(this.labelPaths),
                useDateProperties);
    }

    /**Method to calculate feature importance.
     * Looks at change in accuracy between original data and data with one
     * feature randomized. Largest relative changes in accuracy
     * are the more important features to the model.
     * @param tree trained weka decision tree
     * @param traindata is training data
     * @param testdata is test data
     * @return map of features and importance value
     * @exception Exception thrown from evaluation**/
    public static List<Map.Entry<String, Double>> featureImportance(J48Batch tree, Instances traindata, Instances testdata) throws Exception {
        int classIndex = testdata.classIndex();
        Map<String, Double> accuracy = Maps.newHashMap();

        Evaluation evaluation = new Evaluation(traindata);
        evaluation.evaluateModel(tree, testdata);

        double baseline = evaluation.pctCorrect();
        double maxFeature = -100;
        for(int i =  0; i < traindata.numAttributes(); i++) {
            if(i != classIndex) {
                Evaluation eval = new Evaluation(traindata);
                eval.evaluateModel(tree, shufflesColumn(i, testdata));
                maxFeature = maxFeature < baseline - eval.pctCorrect() ? baseline - eval.pctCorrect() : maxFeature;
                accuracy.put(testdata.attribute(i).name(), baseline - eval.pctCorrect());
            }
        }
        List<Map.Entry<String, Double>> acc =  accuracy.entrySet().stream()
                .sorted(Map.Entry.<String, Double>comparingByValue().reversed()).collect(Collectors.toList());
        for(Map.Entry<String, Double> ac :  acc){
            if(maxFeature !=  0) {
                ac.setValue(ac.getValue() / maxFeature);
            }
            else{
                ac.setValue(0.0);
            }
        }
        System.out.println(" ");
        System.out.println("-----------------------------------------------");
        System.out.println("Feature Importance:");
        System.out.println("-----------------------------------------------");
        for(Map.Entry<String, Double> ac :  acc){
            System.out.println(ac.getKey() + ": " + ac.getValue());
        }
        return acc;
    }

    /**Method to randomize data in one column while keeping all others fixed.
     * @param col column to shuffle
     * @param testdata data instances to be used
     * @return new data set with one randomized column
     * **/
    public static Instances shufflesColumn(int col, Instances testdata){
        List<Double> copyOfColuum = Lists.newArrayList();
        Instances copy = new Instances(testdata);
        double[] column = testdata.attributeToDoubleArray(col);
        Arrays.stream(column).forEach(c -> copyOfColuum.add(c));
        Collections.shuffle(copyOfColuum);
        for(int i = 0; i < copy.numInstances(); i++) {
            copy.instance(i).setValue(col,copyOfColuum.get(i));
        }
        return copy;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public double getError() {
        return error;
    }

    public double getTruePos() {
        return truePos;
    }

    public double getFalsePos() {
        return falsePos;
    }

    public double getFalseNeg() {
        return falseNeg;
    }

    public J48Batch getTree() {
        return tree;
    }

    public Tree getTreeModel(){
        return trainedModel;
    }

    public List<Map.Entry<String, Double>> getFeatureImportance() {
        return featureImportance;
    }

    public PrintableTreeResults getPrintableTreeResults(){
        PrintableTreeResults ptr = new PrintableTreeResults();
        ptr.setAccuracy(Double.toString(this.accuracy));
        ptr.setFeature(this.featureName);
        ptr.setLabel(this.name);
        ptr.setError(Double.toString(this.error));
        ptr.setPrecision(Double.toString(truePos / (truePos + falseNeg)));
        ptr.setRecall(Double.toString(truePos / (truePos + falsePos)));
        ptr.setFeatureImportance(stringifyFeatureImportance());
        ptr.setBaselineAccuracy(Double.toString(this.baselineAccuracy));
        return ptr;
    }

    private Map<String, String> stringifyFeatureImportance(){
        Map<String, String> stringifyFI = Maps.newLinkedHashMap();
        for(Map.Entry<String, Double> fi : this.featureImportance){
            stringifyFI.put(fi.getKey(),Double.toString(fi.getValue()));
        }
        return stringifyFI;
    }

    public Map<String, List<String>> getLabelPaths(){
        StringBuilder sb = new StringBuilder("{");
        Map<String, List<String>> labelPaths = Maps.newHashMap();
        labelPaths = getChildren(this.trainedModel,labelPaths);
        this.labelPaths.putAll(labelPaths);
        for(Map.Entry<String,List<String>> kv: labelPaths.entrySet()){
            System.out.println("Label: " + kv.getKey());
            for(String l : kv.getValue()) {
                System.out.println(l);
            }
        }
        return labelPaths;
    }

    private Map<String, List<String>> getChildren(Tree trainedTree, Map<String, List<String>> labelPaths) {
        try {
            for (Object child : trainedTree.getChildren()) {
                Tree c = (Tree) child;
                if (c.isLeaf()) {
                    String label = (String) ((Map<String, Object>) c.getValue()).get("class");
                    if (!labelPaths.containsKey(label)) {
                        List<String> tmp = Lists.newArrayList();
                        labelPaths.put(label, tmp);
                    }
                    Map<String, WekaTreeParser.ValueTracker> path =
                            (Map<String, WekaTreeParser.ValueTracker>) ((Map<String, Object>) c.getValue()).get("path");
                    StringBuilder sb = new StringBuilder();
                        for (Map.Entry<String, WekaTreeParser.ValueTracker> kv : path.entrySet()) {
                            String p = kv.getKey() + " " + kv.getValue().getRelation();
                            sb.append(p + " AND ");
                        }
                        if (sb.length() > 1) {
                            sb.delete(sb.length() - 5, sb.length());
                            labelPaths.get(label).add(sb.toString());
                        }
                } else {
                    labelPaths = getChildren(c, labelPaths);
                }
            }
            return labelPaths;
        }
        catch(Exception e){
            System.out.println(e);
            return labelPaths;
        }
    }


    public Map<String, List<String>> getPaths() {
        return this.labelPaths;
    }

    public String getModelName() {
        return modelName;
    }
}
