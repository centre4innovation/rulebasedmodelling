package org.cfi.rulebasedmodelling.ml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.cfi.rulebasedmodelling.api.Config;
import org.cfi.rulebasedmodelling.postgres.DataRead;
import org.cfi.rulebasedmodelling.postgres.datamanagement.TableBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import weka.core.Attribute;
import weka.core.Debug;
import weka.core.Instances;
import weka.filters.Filter;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Train a J48 decision tree, given a table.
 * @author Aaron Swaving and Arvid Halma
 */
public class TrainTree {

    private final static Logger logger = LoggerFactory.getLogger(TrainTree.class);

    private ObjectMapper objectMapper;
    private Instances fullData;
    private ModelGenerator mg = new ModelGenerator();
    private boolean findAllClassifiables;
    private String targetCols;
    private boolean isMultiClass;
    private String labelInstance;
    private String table;
    private Map<String, String> columnAndType = Maps.newLinkedHashMap();
    public static int targetIndex = -1;
    private String targetValues;


    /**Initialise data for selected table.
     * Creates temporary .arff file of data. Data then converted to wkea Instances object.
     * @param config extension of dropwizard configuration object
     * @param objectMapper ObjectMapper from Jackson
     * @param table table to analyse
     * @param selectedCols selected columns of table
     * @param targetCol feature to predict
     * @param labelInstance value of target column - can be a single value or all of them (Multi-Class)
     * @exception Exception when creating temp file
     * **/
    public TrainTree(Config config, ObjectMapper objectMapper, String table, List<String> selectedCols,
                     String targetCol, String labelInstance, boolean useDateAsString) throws Exception{

        this.objectMapper = objectMapper;

        this.targetIndex = selectedCols.indexOf(targetCol);

        TableBuilder selectedquery = new DataRead(config).getTable(table, selectedCols);
        logger.info("query: {}", selectedquery.getQuery());

        String filePath = File.createTempFile(table, ".arff").getAbsolutePath();

        /** The following pair has a key of ordered columns used in decision tree
         * and a value of columns excluded since they are strings**/
        Map<String, String> colAndType = config.dao.getReadData()
                .exportArffWithDateFeatureEngineering("SELECT * FROM (" +
                        selectedquery.getQuery() + ") tmp_tab LIMIT 200000", filePath, selectedCols, useDateAsString);

        this.columnAndType.putAll(colAndType);

        logger.info("ARFF exported: {}", filePath);
        Instances dataset = this.mg.loadDataset(filePath);
        dataset.randomize(new Debug.Random(1));// if you comment this line the accuracy of the model will be droped from 96.6% to 80%
        this.fullData = new Instances(dataset);
        this.findAllClassifiables = targetCol == "";
        this.targetCols = targetCol.toLowerCase();
        this.isMultiClass = labelInstance.equals("Multi-Class") ? true : false;
        this.labelInstance = labelInstance.toLowerCase();
        this.table = table;
    }

    /**Initialise data for selected table.
     * Creates temporary .arff file of data. Data then converted to wkea Instances object.
     * @param config extension of dropwizard configuration object
     * @param objectMapper ObjectMapper from Jackson
     * @param table table to analyse
     * @param selectedCols selected columns of table
     * @exception Exception when creating temp file
     * **/
    public TrainTree(Config config, ObjectMapper objectMapper, String table, List<String> selectedCols,
                     boolean useDateAsString) throws Exception {
        this(config, objectMapper, table, selectedCols, "", "Multi-Class", useDateAsString);
    }


    /**Method used to handle training of either a multi-class classification or a binary classification.
     * @param minLeafNumber the minimum allowed number of data points in a leaf
     * @param pruned use post pruning of decision true
     * @exception Exception comes from trainModel or trainBinaryModels
     * **/
    public Map<String,Map<String, Label>> train(int minLeafNumber, boolean pruned) throws Exception {
        if(this.isMultiClass){
            Map<String, Map<String, Label>> classifed = Maps.newLinkedHashMap();
            classifed.put("Multi-Class", trainModel(minLeafNumber, pruned));
            return classifed;
        }
        else{
            return trainBinaryModels(minLeafNumber, pruned);
        }
    }


    /**Train a classification model for all classifiable variables.
     * @param minLeafNumber is the regularizing variable that controls the minimum number of data points
     *                      in each leaf of each tree.
     * @param pruned use post pruning of decision true
     * @exception Exception handling exceptions due to buildClassifier or evaluateTrainedModel
     * **/
    public Map<String, Label> trainModel(int minLeafNumber, boolean pruned) throws Exception{
        logger.info("start training..." );
        Map<String, Label> treeResults = Maps.newLinkedHashMap();
        // build classifier with train dataset
        for(String clas : getAllClassifiables()) {
            treeResults.put(clas.toLowerCase(), new Label(clas, this.fullData, minLeafNumber, pruned,
                    this.table, this.targetIndex, this.objectMapper));
        }
        return treeResults;
    }


    /**Train binary classification models for all classifiable variables.
     * If the number of unique values in a classifiable variable is greater than binary,
     * multiple models are run using one-versus-many.
     * @param minLeafNum is the regularizing variable that controls the minimum number of data points
     *                      in each leaf of each tree.
     * @param pruned use post pruning of decision true
     * @exception Exception handling exceptions due to buildClassifier or evaluateTrainedModel
     * **/
    public Map<String,Map<String, Label>> trainBinaryModels(int minLeafNum, boolean pruned) throws Exception {
        logger.info("start training all possible binary models..." );
        //Use this to hold column of interest and each label's altered data set
        Map<String,Map<String, Label>> classValInstance = Maps.newHashMap();

        for(String col : getAllClassifiables()) {
            //Use this to hold a label of interest an the altered data instances
            Map<String, Label> classLabels = Maps.newHashMap();
            //Get labels for feature
            List<String> labels = Lists.newArrayList();
            Enumeration<Object> enums = this.fullData.attribute(col).enumerateValues();
            while(enums.hasMoreElements()){
                labels.add((String) enums.nextElement());
            }
            int numberOfLabels = labels.size();
            //run through all labels and create new data sets for each one
            for(int i = 0; i < numberOfLabels; i++){
                String label = labels.get(i).toLowerCase();
                if(label.equals(labelInstance) || labelInstance.equals("binary")) {
                    //create filter to replace all but one label as "other"
                    Filter changeValues = createFilter(this.fullData, col, labels, i);
                    //do model evaluation for the relabeled data
                    classLabels.put(labels.get(i).toLowerCase(),
                            new Label(labels.get(i), col, new Instances(Filter.useFilter(this.fullData, changeValues)),
                            minLeafNum, pruned, this.table, this.targetIndex, this.objectMapper));
                }
            }
            System.gc();
            classValInstance.put(col.toLowerCase(), classLabels);
        }
        return classValInstance;
    }

    /**Filter used to change data within Instances
     * @param data weka Instances created from data set
     * @param feature feature of interest
     * @param labels unique values in feature of interest
     * @exception Exception
     * **/
    private Filter createFilter(Instances data, String feature,  List<String> labels, int labelIndex) throws Exception {
        Filter changeValues = new RenameMultipleNominalValues();
        String replaceValues = getValuesToReplace(labels.get(labelIndex),labels);
        ((RenameMultipleNominalValues) changeValues).setOptions(new String[] {"-N",replaceValues});
        ((RenameMultipleNominalValues) changeValues).setSelectedAttributes(feature);
        changeValues.setInputFormat(data);
        return changeValues;
    }


    private String getValuesToReplace(String labelOfInterest, List<String> labels){
        List<String> cl = Lists.newArrayList(labels);
        StringBuilder sb = new StringBuilder();
        cl.remove(labelOfInterest);
        for(String clas : cl){
            sb.append(clas + ":other,");
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }


    /**Find all classifable columns in data.
     * If the attribute is 'nominal' then it is classifiable.
     * @return list of classifiable columns
     * **/
    public List<String> getAllClassifiables(){
        int numCols = this.fullData.numAttributes();
        List<String> classifiables = Lists.newArrayList();
        for(int i = 0; i < numCols; i++) {
            String name = this.fullData.attribute(i).name();
            String type = Attribute.typeToString(this.fullData.attribute(i));
            if(type.equals("nominal")){
                classifiables.add(name);
            }
        }
        if(!this.findAllClassifiables) classifiables = classifiables.stream().filter(c -> c.contains(targetCols)).collect(Collectors.toList());
        return classifiables;
    }


    /**Take model convert to string and pass to WekaTreeParser.
     * @see WekaTreeParser **/
    public static Tree parseTree(J48Batch tree){
        return WekaTreeParser.parse(tree.toString());
    }


    public static Instances createDataForPrediction(String dataset, List<String> selectedCols,
                                                    List<String> columnTypes, Config config,
                                                    String modelName, String targetName,
                                                    String targetValues, boolean useDateAsString) throws IOException {
        TableBuilder selectedquery = new DataRead(config).getTable(dataset, selectedCols);
        logger.info("query: {}", selectedquery.getQuery());

        String targetIndex = config.dao.getReadData().getTargetIndex(modelName);

        String filePath = File.createTempFile(dataset, ".arff").getAbsolutePath();

        /** The following pair has a key of ordered columns used in decision tree
         * and a value of columns excluded since they are strings**/
        config.dao.getReadData()
                .exportPredictionArffWithDateFeatureEngineering("SELECT * FROM (" +
                        selectedquery.getQuery() + ") tmp_tab LIMIT 50000", filePath, selectedCols, columnTypes,
                        Integer.parseInt(targetIndex), targetName, targetValues, useDateAsString);

        targetIndex = !useDateAsString ? String.valueOf(Integer.parseInt(targetIndex) + 8) : targetIndex;

        logger.info("ARFF exported: {}", filePath);
        ModelGenerator mg = new ModelGenerator();
        Instances dset = mg.loadDataset(filePath);
        dset.setClassIndex(Integer.parseInt(targetIndex));
        return dset;
    }

    /**Method retrieves a map with a key of a string made up of columns used in the decision tree
     * in the order they are used and a value of a string made up of columns excluded from training
     * due to being considered unstructured data**/
    public Map<String, String> getColumnAndType() {
        return this.columnAndType;
    }
}
