package org.cfi.rulebasedmodelling.ml;

import com.google.common.collect.Lists;
import org.cfi.rulebasedmodelling.util.Pair;
import weka.classifiers.Evaluation;
import weka.classifiers.trees.J48;
import weka.core.Attribute;
import weka.core.Instances;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;

public class KFoldValidationEvaluation {

    private int numberOfFolds;
    private AtomicInteger count = new AtomicInteger();

    private double truePositive;
    private double falsePositive;
    private double falseNegative;
    private List<Double> accuracyList = Lists.newArrayList();

    private double averageAccuracy;
    private double accuracySD;

    public KFoldValidationEvaluation(int numberOfFolds){
        this.numberOfFolds = numberOfFolds;
    }

    private void calculateMetrics(Instances trainingData, J48 tree, int fold) throws Exception {
        Evaluation evaluation = new Evaluation(splitData(trainingData, fold).getA());
        evaluation.evaluateModel(tree,splitData(trainingData, fold).getB());

        int classIndex = trainingData.classIndex();
        Attribute classAttribute = trainingData.attribute(classIndex);

        if(classAttribute.numValues() > 2) {
            for (int i = 0; i < classAttribute.numValues(); i++) {
                this.truePositive += evaluation.numTruePositives(i);
                this.falsePositive += evaluation.numFalsePositives(i);
                this.falseNegative += evaluation.numFalseNegatives(i);
            }
        }
        else{
            if(classAttribute.indexOfValue("other") == 0){
                this.truePositive += evaluation.numTruePositives(1);
                this.falsePositive += evaluation.numFalsePositives(1);
                this.falseNegative += evaluation.numFalseNegatives(1);
            }
            else{
                this.truePositive += evaluation.numTruePositives(0);
                this.falsePositive += evaluation.numFalsePositives(0);
                this.falseNegative += evaluation.numFalseNegatives(0);
            }
        }
        this.accuracyList.add(evaluation.pctCorrect());
    }

    private Pair<Instances,Instances> splitData(Instances trainingData, int fold){
        Instances train = trainingData.trainCV(this.numberOfFolds,fold);
        Instances test = trainingData.testCV(this.numberOfFolds,fold);
        return new Pair<>(train,test);
    }

    public void evaluateTrainedModel(Instances trainingData, J48 tree) throws Exception, InterruptedException, ExecutionException {

//        int threadNum = this.numberOfFolds;
//        ExecutorService executor = Executors.newFixedThreadPool(threadNum);
//        List<FutureTask<Double>> taskList = new ArrayList<>();

//        for(int j = 0; j < numberOfFolds; j++) {
//            FutureTask<Void> futureTask = new FutureTask<Void>(new Callable<Void>() {
//                @Override
//                public Void call() throws Exception {
//                    calculateMetrics(count.getAndIncrement());
//                    return null;
//                }
//            });
////            taskList.add(futureTask);
//            executor.execute(futureTask);
//        }
        for(int j = 0; j < numberOfFolds; j++) {
            calculateMetrics(trainingData, tree, count.getAndIncrement());
        }

//        executor.shutdown();
        double accuracySum = 0.0;
        double differenceSum = 0.0;
        for(double ac : this.accuracyList){
            accuracySum += ac;
        }
        this.averageAccuracy = accuracySum/numberOfFolds;
        for(double ac : this.accuracyList){
            differenceSum += (ac - averageAccuracy)*(ac - averageAccuracy);
        }
        this.accuracySD = Math.sqrt(differenceSum / (numberOfFolds - 1));
    }

    public double getTruePositive() {
        return truePositive;
    }

    public double getFalsePositive() {
        return falsePositive;
    }

    public double getFalseNegative() {
        return falseNegative;
    }

    public double getAverageAccuracy() {
        return averageAccuracy;
    }

    public double getAccuracySD() {
        return accuracySD;
    }
}
