package org.cfi.rulebasedmodelling.ml;

import com.google.common.collect.Maps;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parse the output of a J48 tree into the {@link Tree} data structure.
 * @author Arvid Halma
 * @version 25-10-18
 */
public class WekaTreeParser {
    private final static Pattern TREE_LINE = Pattern.compile("((\\|   )*)(.+?) (<=|>|=) ([^:]+)(: (.+?) \\(((\\d+)\\.\\d*(/(\\d+)\\.\\d*)?)\\))?");

    public static void main(String[] args) {
        String input = acledTree();
        final Tree<Map<String, Object>> tree = parse(input);
        tree.print();
    }

    public static Tree<Map<String, Object>> parse(String input){


        Tree<Map<String, Object>> tree = new Tree<>();

        Tree<Map<String, Object>> currentNode;
        Tree<Map<String, Object>> lastNode = null;
        Tree<Map<String, Object>> parent = tree;

        Map<String, ValueTracker> path = Maps.newHashMap();
        Map<String, Object> prop = new LinkedHashMap<>();
        prop.put("path", path);
        parent.setValue(prop);

        int lastDepth = 0;
        final String[] split = input.split("\n");

        if(split.length == 7){
            currentNode = new Tree<>();
            Map<String, Object> props = new LinkedHashMap<>();
            props.put("depth", 1);
            String attr = split[2].replaceFirst(":","");
            String[] atAndCE = attr.split("\\(");
            attr = atAndCE[0];
            props.put("attr", attr);
            props.put("op", "");
            props.put("val", "");
            props.put("path", path);

            currentNode.setValue(props);

            String[] ce = atAndCE[1].split("/");
            String count = ce[0];
            String error = ce[1].replaceFirst("\\)","");

            props = currentNode.getValue();
            props.put("class", attr);
            props.put("count", count);
            props.put("errors", error);

            parent.addChild(currentNode);
        }

        for (int i = 3; i < split.length; i++) {
            String line = split[i];
            if(line.isEmpty()){
                break; // end of tree result
            }

            final Matcher matcher = TREE_LINE.matcher(line);
            if (matcher.find()) {
                int depth = matcher.group(1).length() / 4;
                String attr = matcher.group(3);
                String op = matcher.group(4);
                String val = matcher.group(5);

                String child = null;
                int childOccurence = 0;
                int childError = 0;

                String leaf = matcher.group(6);
                if (leaf != null) {
                    child = matcher.group(7);
                    childOccurence = Integer.parseInt(matcher.group(9));
                    final String err = matcher.group(11);
                    if (err != null) {
                        childError = Integer.parseInt(err);
                    }
                }

                //parent is now changed to the last child created
                if (depth > lastDepth) {
                    // push
                    parent = lastNode;
                }
                //parent is now changed to a higher node
                else if(depth < lastDepth){
                    // pop
                    for (int j = depth; j < lastDepth; j++) {
                        parent = parent.getParent();
                    }
                }

                //Track the path taken to each node
                Map<String, ValueTracker> pathTracker = Maps.newHashMap((Map<String, ValueTracker>) parent.getValue().get("path"));
                ValueTracker vt = pathTracker.containsKey(attr) ? new ValueTracker(pathTracker.get(attr)).updateTracker(op,val) :
                        new ValueTracker().updateTracker(op,val);
                pathTracker.put(attr,vt);

                currentNode = new Tree<>();
                Map<String, Object> props = new LinkedHashMap<>();
                props.put("depth", depth);
                props.put("attr", attr);
                props.put("op", op);
                props.put("val", val);
                props.put("path", pathTracker);
                currentNode.setValue(props);

                if (leaf != null) {
                    props = currentNode.getValue();
                    props.put("class", child);
                    props.put("count", childOccurence);
                    props.put("errors", childError);
                }

                parent.addChild(currentNode);

                lastDepth = depth;
                lastNode = currentNode;
            }
        }
        return tree;
    }



    public static class ValueTracker{
        private Double minValue = 0.0;
        private Double maxValue = 0.0;
        private String valueType = "EMPTY";
        private String stringValue = "";

        public ValueTracker(){}

        public ValueTracker(ValueTracker valueTracker){
            this.minValue = valueTracker.getMinValue();
            this.maxValue = valueTracker.getMaxValue();
            this.valueType = valueTracker.getValueType();
            this.stringValue = valueTracker.getStringValue();
        }

        public ValueTracker updateTracker(String rel, String val){
            try{
                double tmpval = Double.parseDouble(val);
                if(rel.equals(">=") || rel.equals(">")){
                    if(valueType.equals("MIN") || valueType.equals("BETWEEN")) {
                        minValue = minValue < tmpval ? tmpval : minValue;
                    }
                    else if(valueType.equals("MAX")){
                        valueType = "BETWEEN";
                        minValue = tmpval;
                    }
                    else{
                        valueType = "MIN";
                        minValue = tmpval;
                    }
                }
                if(rel.equals("<=") || rel.equals("<")){
                    if(valueType.equals("MAX") || valueType.equals("BETWEEN")) {
                        maxValue = maxValue > tmpval ? tmpval : maxValue;
                    }
                    else if(valueType.equals("MIN")){
                        valueType = "BETWEEN";
                        maxValue = tmpval;
                    }
                    else{
                        valueType = "MAX";
                        maxValue = tmpval;
                    }
                }
                if(rel.equals("=")){
                    valueType = "EQUALS";
                    stringValue = Double.toString(tmpval);
                }
                return this;
            }
            catch(NumberFormatException e){
                this.valueType = "STRING";
                this.stringValue = val;
                return this;
            }
        }


        public String getRelation() throws Exception{
            if(valueType.equals("MIN")){
                return " > " + minValue;
            }
            else if(valueType.equals("MAX")) {
                return " < " + maxValue;
            }
            else if (valueType.equals("BETWEEN")){
                return "BETWEEN " + minValue +  " AND " + maxValue;
            }
            else{
                return " = " + stringValue;
            }
        }

        public Double getMinValue() {
            return minValue;
        }

        public Double getMaxValue() {
            return maxValue;
        }

        public String getValueType() {
            return valueType;
        }

        public String getStringValue() {
            return stringValue;
        }
    }

    public static String irisTree(){
        return "J48 unpruned tree\n" +
                "------------------\n" +
                "\n" +
                "petalwidth <= 0.208333: Iris-setosa (39.0)\n" +
                "petalwidth > 0.208333\n" +
                "|   petalwidth <= 0.666667\n" +
                "|   |   petallength <= 0.661017: Iris-versicolor (38.0/1.0)\n" +
                "|   |   petallength > 0.661017\n" +
                "|   |   |   petalwidth <= 0.583333: Iris-virginica (2.0)\n" +
                "|   |   |   petalwidth > 0.583333: Iris-versicolor (3.0/1.0)\n" +
                "|   petalwidth > 0.666667: Iris-virginica (38.0/1.0)";
    }

    public static String acledTree(){
        return "J48 unpruned tree\n" +
                "------------------\n" +
                "\n" +
                "region = Middle Africa\n" +
                "|   admin1 = Karonga: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Kinshasa: Democratic Republic of Congo (1.0)\n" +
                "|   admin1 = Ankara: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Adamawa: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Bujumbura Mairie: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Woqooyi Galbeed: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Helmand: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Skikda: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Ibb: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Isfahan: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Assam: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Kaduna: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Al-Hasakeh: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Babylon: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Oyo: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Lattakia: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = South: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Odisha: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Kermanshah: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Gauteng: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Al Mahwit: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Ogun: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Badakhshan: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Tehran: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Al Asimah: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Jammu and Kashmir: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Conakry: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Tel Aviv: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Makamba: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Diyala: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Sistan and Baluchestan: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = NCT of Delhi: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Deir-ez-Zor: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Dhaka: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Sirnak: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Federal Capital Territory: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Sanaag: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Hama: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Rumphi: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Zamfara: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Dire Dawa: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Galguduud: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Fars: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Erbil: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Asir: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = West Bank: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = West Azarbaijan: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Delta: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Mazandaran: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Cibitoke: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Eastern Cape: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Sala al-Din: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Andhra Pradesh: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Upper Nile: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Maharashtra: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Western Bahr el Ghazal: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Jonglei: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Chlef: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Kordestan: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Damascus: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Katanga: Democratic Republic of Congo (1.0)\n" +
                "|   admin1 = Chittagong: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Rural Damascus: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Brazzaville: Republic of Congo (1.0)\n" +
                "|   admin1 = Warrap: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Capital: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Mambere-Kadei: Central African Republic (1.0)\n" +
                "|   admin1 = Ajdabiya: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Mandera: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Kilifi: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Hormozgan: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Golestan: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Farah: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Orientale: Democratic Republic of Congo (6.0)\n" +
                "|   admin1 = Hiiraan: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Lahij: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Razavi Khorasan: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Sahel: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Khyber Pakhtunkhwa: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Punjab: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Shabwah: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Constantine: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Haut-Ogooue: Gabon (1.0)\n" +
                "|   admin1 = Kassala: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Maputo City: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Meru: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Nuristan: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Borno: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Nairobi: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Ghazni: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Anambra: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Hakkari: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Urozgan: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Cairo: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Gaza Strip: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Thi-Qar: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Brong Ahafo: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Ar Riyad: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Midlands: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Tamil Nadu: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Somali: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Imo: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Northern: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = HaZafon: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Bari: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Chhattisgarh: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Istanbul: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Mopti: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Sulaymaniyah: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Taraba: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Kerala: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Sabha: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Baghdad: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Extreme-Nord: Cameroon (4.0)\n" +
                "|   admin1 = Western: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Haryana: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Kasai-Occidental: Democratic Republic of Congo (2.0)\n" +
                "|   admin1 = Mulanje: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Rivers: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Mount Lebanon: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Benue: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Nabatiye: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Sanaa: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Bujumbura Rural: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = East Azarbaijan: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Oromia: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Central Darfur: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Beirut: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Bejaia: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Gao: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Mpumalanga: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Qom: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Nasarawa: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Erzincan: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Sindh: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Khartoum: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Tunis: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Harare: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Balkh: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Marib: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Baalbek-Hermel: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Amhara: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = North Darfur: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Tillabery: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Darnah: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Nord-Ouest: Cameroon (3.0)\n" +
                "|   admin1 = Idleb: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Afyonkarahisar: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Kirkuk: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Aden: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = West Bengal: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Sud-Ouest: Cameroon (5.0)\n" +
                "|   admin1 = Bangui: Central African Republic (5.0)\n" +
                "|   admin1 = Ninewa: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Sud-Kivu: Democratic Republic of Congo (4.0)\n" +
                "|   admin1 = Balochistan: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Anbar: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Khuzestan: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Lagos: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Najran: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Bayelsa: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = North West: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Lakes: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Tunceli: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Bay: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Plateau: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Qazvin: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Lacs: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Nouakchott: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Dar'a: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Bushehr: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = North: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Kankan: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Uttar Pradesh: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Jizan: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Al Bayda: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Al Balqa: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Karnataka: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Karas: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Zanjan: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Nord-Kivu: Democratic Republic of Congo (4.0)\n" +
                "|   admin1 = Banaadir: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Al Mahrah: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Al Hizam Al Akhdar: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = KwaZulu-Natal: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Puducherry: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = West Java: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Central: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Ouham Pende: Central African Republic (1.0)\n" +
                "|   admin1 = Telangana: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Upper East: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = West: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Central Equatoria: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Surt: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Edo: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Mardin: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Al Jawf: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = North Sinai: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Sadah: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Abyan: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Kunduz: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Nangarhar: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Aleppo: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Bekaa: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Ekiti: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Takhar: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Hatay: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Sool: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Konya: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Amanat al Asimah: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Unity: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Gitega: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = HaDarom: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Antananarivo: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Nugaal: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Enugu: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Baringo: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Alborz: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Centre: Cameroon (1.0)\n" +
                "|   admin1 = Basrah: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Suqutra: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Rajasthan: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Hamadan: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Kerman: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Izmir: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Jubbada Hoose: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Al Hudaydah: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Chandigarh: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Hajjah: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Bauchi: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Taizz: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Dakhlet Nouadhibou: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Gilan: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Shabeellaha Hoose: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Kohgiluyeh and Buyer Ahmad: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Homs: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Western Cape: Democratic Republic of Congo (0.0)\n" +
                "|   admin1 = Nana-Gribizi: Central African Republic (1.0)\n" +
                "region = Southern Africa: South Africa (30.0/4.0)\n" +
                "region = Middle East\n" +
                "|   actor1 = LDP: Lebanese Democratic Party: Lebanon (1.0)\n" +
                "|   actor1 = Protesters (Lebanon): Lebanon (2.0)\n" +
                "|   actor1 = Banyamulenge Ethnic Militia (Democratic Republic of Congo): Yemen (0.0)\n" +
                "|   actor1 = Military Forces of Yemen (2012-) Giants Brigade: Yemen (8.0)\n" +
                "|   actor1 = Military Forces of Yemen (2016-) Supreme Political Council\n" +
                "|   |   interaction <= 17: Yemen (34.0)\n" +
                "|   |   interaction > 17: Saudi Arabia (40.0/2.0)\n" +
                "|   actor1 = SPLA/M-In Opposition-Deng Gai: Sudanese Peoples Liberation Army/Movement-In Opposition (Deng Gai Faction): Yemen (0.0)\n" +
                "|   actor1 = Turkana Ethnic Militia (Kenya): Yemen (0.0)\n" +
                "|   actor1 = Militia (Herders): Yemen (0.0)\n" +
                "|   actor1 = Unidentified Armed Group (Lebanon): Lebanon (1.0)\n" +
                "|   actor1 = Issa Mahmud-Reer Yoonis Sub-Clan Militia (Somalia): Yemen (0.0)\n" +
                "|   actor1 = Hamza Brigade: Syria (1.0)\n" +
                "|   actor1 = Military Forces of Somalia (2017-) Special Forces: Yemen (0.0)\n" +
                "|   actor1 = Police Forces of Nigeria (2015-): Yemen (0.0)\n" +
                "|   actor1 = Vigilante Militia (Malawi): Yemen (0.0)\n" +
                "|   actor1 = Pokot Ethnic Militia (Kenya): Yemen (0.0)\n" +
                "|   actor1 = Rioters (Tunisia): Yemen (0.0)\n" +
                "|   actor1 = Military Forces of Ethiopia (1991-): Yemen (0.0)\n" +
                "|   actor1 = Military Forces of Iraq (2014-) Popular Mobilization Forces: Iraq (3.0/1.0)\n" +
                "|   actor1 = Kondorobo Communal Militia (Ivory Coast): Yemen (0.0)\n" +
                "|   actor1 = Rioters (Nigeria): Yemen (0.0)\n" +
                "|   actor1 = Protesters (South Africa): Yemen (0.0)\n" +
                "|   actor1 = Police Forces of India (1949-): Yemen (0.0)\n" +
                "|   actor1 = Allied Syrian and/or Russian Forces: Syria (18.0)\n" +
                "|   actor1 = Settlers (Israel): Palestine (2.0)\n" +
                "|   actor1 = Militia (Kamwina Nsapu): Yemen (0.0)\n" +
                "|   actor1 = Islamic State (Libya): Yemen (0.0)\n" +
                "|   actor1 = Hezbollah: Lebanon (1.0)\n" +
                "|   actor1 = Islamic State (Iraq): Iraq (23.0)\n" +
                "|   actor1 = Protesters (Cameroon): Yemen (0.0)\n" +
                "|   actor1 = Opposition Rebels (Syria): Syria (6.0)\n" +
                "|   actor1 = Protesters (Palestine): Palestine (2.0)\n" +
                "|   actor1 = Rioters (Palestine)\n" +
                "|   |   data_id <= 1604129: Palestine (5.0)\n" +
                "|   |   data_id > 1604129: Israel (5.0)\n" +
                "|   actor1 = Unidentified Armed Group (South Africa): Yemen (0.0)\n" +
                "|   actor1 = Rioters (Democratic Republic of Congo): Yemen (0.0)\n" +
                "|   actor1 = Protesters (Bangladesh): Yemen (0.0)\n" +
                "|   actor1 = Protesters (Ethiopia): Yemen (0.0)\n" +
                "|   actor1 = Police Forces of Saudi Arabia (2015-): Saudi Arabia (1.0)\n" +
                "|   actor1 = Foulata Communal Militia (Guinea): Yemen (0.0)\n" +
                "|   actor1 = Abgal-Daud Clan Militia (Somalia): Yemen (0.0)\n" +
                "|   actor1 = SLM/A-Minnawi: Sudan Liberation Movement/Army (Minnawi Faction): Yemen (0.0)\n" +
                "|   actor1 = Military Forces of Turkey (2016-): Turkey (8.0/1.0)\n" +
                "|   actor1 = Protesters (Indonesia): Yemen (0.0)\n" +
                "|   actor1 = Military Forces of Cameroon (1982-): Yemen (0.0)\n" +
                "|   actor1 = Habar Jeclo Clan Militia (Somalia): Yemen (0.0)\n" +
                "|   actor1 = LeT: Lashkar-e-Taiba: Yemen (0.0)\n" +
                "|   actor1 = Military Forces of Puntland (1998-): Yemen (0.0)\n" +
                "|   actor1 = Rioters (Malawi): Yemen (0.0)\n" +
                "|   actor1 = Rioters (Iran): Iran (2.0)\n" +
                "|   actor1 = Unidentified Caste Militia (India): Yemen (0.0)\n" +
                "|   actor1 = Protesters (Nigeria): Yemen (0.0)\n" +
                "|   actor1 = CNDD-FDD-Imbonerakure: National Council for the Defence of Democracy (Imbonerakure Faction): Yemen (0.0)\n" +
                "|   actor1 = Military Forces of Afghanistan (2004-): Yemen (0.0)\n" +
                "|   actor1 = Unidentified Armed Group (Democratic Republic of Congo): Yemen (0.0)\n" +
                "|   actor1 = Unidentified Armed Group (Turkey): Turkey (1.0)\n" +
                "|   actor1 = Protesters (Mauritania): Yemen (0.0)\n" +
                "|   actor1 = Khalid ibn al-Walid Army: Syria (1.0)\n" +
                "|   actor1 = Rioters (Lebanon): Lebanon (10.0)\n" +
                "|   actor1 = Unidentified Armed Group (Palestine): Palestine (1.0)\n" +
                "|   actor1 = Military Forces of Nigeria (2015-): Yemen (0.0)\n" +
                "|   actor1 = SLM/A-Nur: Sudan Liberation Movement/Army (Abdul Wahid al-Nur Faction): Yemen (0.0)\n" +
                "|   actor1 = Protesters (Central African Republic): Yemen (0.0)\n" +
                "|   actor1 = Unidentified Armed Group (Cameroon): Yemen (0.0)\n" +
                "|   actor1 = Rioters (Somalia): Yemen (0.0)\n" +
                "|   actor1 = Military Forces of Democratic Republic of Congo (2001-): Yemen (0.0)\n" +
                "|   actor1 = Islamic State (Greater Sahara): Yemen (0.0)\n" +
                "|   actor1 = Al Shabaab: Yemen (0.0)\n" +
                "|   actor1 = FRPI: Front for Patriotic Resistance of Ituri: Yemen (0.0)\n" +
                "|   actor1 = Military Forces of Kenya (2013-): Yemen (0.0)\n" +
                "|   actor1 = Military Forces of Syria (2000-) Syrian Arab Air Force: Syria (1.0)\n" +
                "|   actor1 = Protesters (Sudan): Yemen (0.0)\n" +
                "|   actor1 = Operation Restoring Hope: Yemen (73.0)\n" +
                "|   actor1 = Mudah Communal Militia (Nigeria): Yemen (0.0)\n" +
                "|   actor1 = Unidentified Armed Group (Republic of Congo): Yemen (0.0)\n" +
                "|   actor1 = Military Forces of Pakistan (2008-): Yemen (0.0)\n" +
                "|   actor1 = Military Forces of South Sudan (2011-): Yemen (0.0)\n" +
                "|   actor1 = GATIA: Imghad Tuareg and Allies Self-Defense Group: Yemen (0.0)\n" +
                "|   actor1 = Military Forces of Syria (2000-): Syria (58.0)\n" +
                "|   actor1 = Military Forces of Libya (2014-) Haftar Faction: Yemen (0.0)\n" +
                "|   actor1 = Unidentified Armed Group (Iraq): Iraq (18.0)\n" +
                "|   actor1 = Protesters (Libya): Yemen (0.0)\n" +
                "|   actor1 = Protesters (India): Yemen (0.0)\n" +
                "|   actor1 = Police Forces of Bahrain (1999-): Bahrain (4.0)\n" +
                "|   actor1 = Oromo Ethnic Militia (Ethiopia): Yemen (0.0)\n" +
                "|   actor1 = Protesters (Afghanistan): Yemen (0.0)\n" +
                "|   actor1 = Rioters (Israel): Israel (2.0)\n" +
                "|   actor1 = Military Forces of Turkey: Yemen (0.0)\n" +
                "|   actor1 = Protesters (Namibia): Yemen (0.0)\n" +
                "|   actor1 = Military Forces of United Arab Emirates: Yemen (0.0)\n" +
                "|   actor1 = Police Forces of Guinea (2010-): Yemen (0.0)\n" +
                "|   actor1 = Unidentified Armed Group (Pakistan): Yemen (0.0)\n" +
                "|   actor1 = Protesters (Gambia): Yemen (0.0)\n" +
                "|   actor1 = MDC-T: Movement for Democratic Change (Tsvangirai Faction): Yemen (0.0)\n" +
                "|   actor1 = Dahalo Militia: Yemen (0.0)\n" +
                "|   actor1 = Islamic State (Yemen): Yemen (1.0)\n" +
                "|   actor1 = Police Forces of Israel (2009-): Palestine (1.0)\n" +
                "|   actor1 = Ceyr Clan Militia (Somalia): Yemen (0.0)\n" +
                "|   actor1 = Rioters (India): Yemen (0.0)\n" +
                "|   actor1 = Military Forces of Israel (2009-): Palestine (31.0/1.0)\n" +
                "|   actor1 = Police Forces of Zimbabwe (1987-): Yemen (0.0)\n" +
                "|   actor1 = Islamic State (Syria): Syria (3.0)\n" +
                "|   actor1 = Protesters (Israel): Israel (2.0/1.0)\n" +
                "|   actor1 = Protesters (Sri Lanka): Yemen (0.0)\n" +
                "|   actor1 = Protesters (Bahrain): Bahrain (4.0)\n" +
                "|   actor1 = Unidentified Armed Group (Nigeria): Yemen (0.0)\n" +
                "|   actor1 = Unidentified Armed Group (Syria): Syria (18.0)\n" +
                "|   actor1 = Unidentified Armed Group (Niger): Yemen (0.0)\n" +
                "|   actor1 = State of Sinai: Yemen (0.0)\n" +
                "|   actor1 = Police Forces of Afghanistan (2004-): Yemen (0.0)\n" +
                "|   actor1 = Protesters (Nepal): Yemen (0.0)\n" +
                "|   actor1 = Police Forces of Egypt (2014-): Yemen (0.0)\n" +
                "|   actor1 = Protesters (Somalia): Yemen (0.0)\n" +
                "|   actor1 = Protesters (Ghana): Yemen (0.0)\n" +
                "|   actor1 = Civilian JTF: Civilian Joint Task Force: Yemen (0.0)\n" +
                "|   actor1 = Ganyiel Communal Militia (South Sudan): Yemen (0.0)\n" +
                "|   actor1 = Protesters (Iraq): Iraq (3.0)\n" +
                "|   actor1 = Unidentified Armed Group (Somalia): Yemen (0.0)\n" +
                "|   actor1 = Ansaroul Islam: Yemen (0.0)\n" +
                "|   actor1 = Unidentified Armed Group (India): Yemen (0.0)\n" +
                "|   actor1 = PKK: Kurdistan Workers Party\n" +
                "|   |   interaction <= 27: Turkey (2.0)\n" +
                "|   |   interaction > 27: Iraq (2.0)\n" +
                "|   actor1 = Unidentified Armed Group (Afghanistan): Yemen (0.0)\n" +
                "|   actor1 = Rioters (Ghana): Yemen (0.0)\n" +
                "|   actor1 = Unidentified Armed Group (Yemen): Yemen (2.0)\n" +
                "|   actor1 = CPI (Maoist): Communist Party of India (Maoist): Yemen (0.0)\n" +
                "|   actor1 = Boko Haram - Wilayat Gharb Ifriqiyyah: Yemen (0.0)\n" +
                "|   actor1 = Military Forces of Iraq (2014-): Iraq (6.0)\n" +
                "|   actor1 = Protesters (Algeria): Yemen (0.0)\n" +
                "|   actor1 = Protesters (Malawi): Yemen (0.0)\n" +
                "|   actor1 = Police Forces of Somalia (2017-): Yemen (0.0)\n" +
                "|   actor1 = Military Forces of Nepal (2008-): Yemen (0.0)\n" +
                "|   actor1 = AMISOM: African Union Mission in Somalia (2007-): Yemen (0.0)\n" +
                "|   actor1 = Police Forces of Democratic Republic of Congo (2001-): Yemen (0.0)\n" +
                "|   actor1 = Protesters (Zimbabwe): Yemen (0.0)\n" +
                "|   actor1 = FLM: Macina Liberation Movement: Yemen (0.0)\n" +
                "|   actor1 = Protesters (Gabon): Yemen (0.0)\n" +
                "|   actor1 = Unidentified Armed Group (South Sudan): Yemen (0.0)\n" +
                "|   actor1 = Mayi Mayi Militia (Mesandu): Yemen (0.0)\n" +
                "|   actor1 = MSA: Movement for Azawad Salvation: Yemen (0.0)\n" +
                "|   actor1 = Aleppo Defense Corps: Syria (1.0)\n" +
                "|   actor1 = Police Forces of Sudan (1989-) National Intelligence and Security Services: Yemen (0.0)\n" +
                "|   actor1 = Protesters (Pakistan): Yemen (0.0)\n" +
                "|   actor1 = Military Forces of Russia - Military Police: Syria (1.0)\n" +
                "|   actor1 = Garre Ethnic Militia (Ethiopia): Yemen (0.0)\n" +
                "|   actor1 = Police Forces of Ethiopia (1991-): Yemen (0.0)\n" +
                "|   actor1 = AMISOM: African Union Mission in Somalia (2007-) (Kenya): Yemen (0.0)\n" +
                "|   actor1 = Unidentified Communal Militia (Democratic Republic of Congo): Yemen (0.0)\n" +
                "|   actor1 = DPF: Derna Protection Force: Yemen (0.0)\n" +
                "|   actor1 = Islamist Militia (Syria): Syria (2.0)\n" +
                "|   actor1 = Unidentified Clan Militia (Somalia): Yemen (0.0)\n" +
                "|   actor1 = Police Forces of Turkey (2016-): Turkey (5.0)\n" +
                "|   actor1 = Unidentified Armed Group (Sudan): Yemen (0.0)\n" +
                "|   actor1 = Military Forces of Egypt (2014-): Yemen (0.0)\n" +
                "|   actor1 = Global Coalition Against Daesh: Iraq (6.0/2.0)\n" +
                "|   actor1 = Unidentified Armed Group (Libya): Yemen (0.0)\n" +
                "|   actor1 = Shura Council of Darnah Mujahidin: Yemen (0.0)\n" +
                "|   actor1 = Protesters (Turkey): Turkey (8.0)\n" +
                "|   actor1 = APC: All Progressives Congress: Yemen (0.0)\n" +
                "|   actor1 = Chajla Communal Militia (India): Yemen (0.0)\n" +
                "|   actor1 = Protesters (Jordan): Jordan (1.0)\n" +
                "|   actor1 = Liyu Police: Yemen (0.0)\n" +
                "|   actor1 = MINUSCA: United Nations Multidimensional Integrated Stabilization Mission in the Central African Republic (2014-): Yemen (0.0)\n" +
                "|   actor1 = FM: Future Movement: Lebanon (1.0)\n" +
                "|   actor1 = Ambazonian Separatists (Cameroon): Yemen (0.0)\n" +
                "|   actor1 = Protesters (Tunisia): Yemen (0.0)\n" +
                "|   actor1 = Protesters (Yemen): Yemen (10.0)\n" +
                "|   actor1 = Military Forces of Yemen (2012-): Yemen (20.0)\n" +
                "|   actor1 = Police Forces of Kenya (2013-): Yemen (0.0)\n" +
                "|   actor1 = Protesters (Kenya): Yemen (0.0)\n" +
                "|   actor1 = Rioters (Central African Republic): Yemen (0.0)\n" +
                "|   actor1 = Police Forces of Puntland (1998-): Yemen (0.0)\n" +
                "|   actor1 = Owevwe Communal Militia (Nigeria): Yemen (0.0)\n" +
                "|   actor1 = Protesters (Burundi): Yemen (0.0)\n" +
                "|   actor1 = Protesters (Syria): Syria (2.0)\n" +
                "|   actor1 = Private Security Forces (Lebanon): Lebanon (1.0)\n" +
                "|   actor1 = Taliban: Yemen (0.0)\n" +
                "|   actor1 = Military Forces of Somalia (2017-): Yemen (0.0)\n" +
                "|   actor1 = KM5 Communal Militia (Central African Republic): Yemen (0.0)\n" +
                "|   actor1 = Unidentified Armed Group (Central African Republic): Yemen (0.0)\n" +
                "|   actor1 = Unidentified Armed Group (Mali): Yemen (0.0)\n" +
                "|   actor1 = Rioters (South Africa): Yemen (0.0)\n" +
                "|   actor1 = Vigilante Militia (Central African Republic): Yemen (0.0)\n" +
                "|   actor1 = HTS: Hayat Tahrir al-Sham: Syria (1.0)\n" +
                "|   actor1 = Fulani Ethnic Militia (Nigeria): Yemen (0.0)\n" +
                "|   actor1 = Military Forces of Yemen (2012-) Security Belt Forces: Yemen (1.0)\n" +
                "|   actor1 = Protesters (Mozambique): Yemen (0.0)\n" +
                "|   actor1 = Rioters (Kenya): Yemen (0.0)\n" +
                "|   actor1 = QSD: Syrian Democratic Forces: Syria (6.0)\n" +
                "|   actor1 = Vigilante Militia (Nigeria): Yemen (0.0)\n" +
                "|   actor1 = Akook Communal Militia (South Sudan): Yemen (0.0)\n" +
                "|   actor1 = Protesters (Iran): Iran (69.0)\n" +
                "|   actor1 = Military Forces of Afghanistan (2004-) Special Forces: Yemen (0.0)\n" +
                "|   actor1 = Military Forces of the United States: Yemen (0.0)\n" +
                "|   actor1 = BRSC: Shura Council of Benghazi Revolutionaries: Yemen (0.0)\n" +
                "|   actor1 = Tihama Resistance: Yemen (2.0)\n" +
                "|   actor1 = Prison Guards (Ethiopia): Yemen (0.0)\n" +
                "|   actor1 = Protesters (Madagascar): Yemen (0.0)\n" +
                "|   actor1 = Rioters (Syria): Syria (1.0)\n" +
                "|   actor1 = Police Forces of Iran (1989-): Iran (2.0)\n" +
                "|   actor1 = ULFA-I: United Liberation Front of Assam - Independent: Yemen (0.0)\n" +
                "|   actor1 = Tabu Ethnic Militia (Libya): Yemen (0.0)\n" +
                "|   actor1 = Protesters (Kuwait): Kuwait (1.0)\n" +
                "|   actor1 = Police Forces of Somaliland (1991-): Yemen (0.0)\n" +
                "|   actor1 = AQAP: Al Qaeda in the Arabian Peninsula: Yemen (2.0)\n" +
                "|   actor1 = Ezza Ethnic Militia (Nigeria): Yemen (0.0)\n" +
                "|   actor1 = National Resistance Forces: Yemen (2.0)\n" +
                "|   actor1 = Dozo Militia: Yemen (0.0)\n" +
                "|   actor1 = Fulani Ethnic Militia (Central African Republic): Yemen (0.0)\n" +
                "|   actor1 = Rioters (Ethiopia): Yemen (0.0)\n" +
                "region = Eastern Africa\n" +
                "|   admin1 = Karonga: Malawi (1.0)\n" +
                "|   admin1 = Kinshasa: Ethiopia (0.0)\n" +
                "|   admin1 = Ankara: Ethiopia (0.0)\n" +
                "|   admin1 = Adamawa: Ethiopia (0.0)\n" +
                "|   admin1 = Bujumbura Mairie: Burundi (1.0)\n" +
                "|   admin1 = Woqooyi Galbeed: Somalia (2.0)\n" +
                "|   admin1 = Helmand: Ethiopia (0.0)\n" +
                "|   admin1 = Skikda: Ethiopia (0.0)\n" +
                "|   admin1 = Ibb: Ethiopia (0.0)\n" +
                "|   admin1 = Isfahan: Ethiopia (0.0)\n" +
                "|   admin1 = Assam: Ethiopia (0.0)\n" +
                "|   admin1 = Kaduna: Ethiopia (0.0)\n" +
                "|   admin1 = Al-Hasakeh: Ethiopia (0.0)\n" +
                "|   admin1 = Babylon: Ethiopia (0.0)\n" +
                "|   admin1 = Oyo: Ethiopia (0.0)\n" +
                "|   admin1 = Lattakia: Ethiopia (0.0)\n" +
                "|   admin1 = South: Ethiopia (0.0)\n" +
                "|   admin1 = Odisha: Ethiopia (0.0)\n" +
                "|   admin1 = Kermanshah: Ethiopia (0.0)\n" +
                "|   admin1 = Gauteng: Ethiopia (0.0)\n" +
                "|   admin1 = Al Mahwit: Ethiopia (0.0)\n" +
                "|   admin1 = Ogun: Ethiopia (0.0)\n" +
                "|   admin1 = Badakhshan: Ethiopia (0.0)\n" +
                "|   admin1 = Tehran: Ethiopia (0.0)\n" +
                "|   admin1 = Al Asimah: Ethiopia (0.0)\n" +
                "|   admin1 = Jammu and Kashmir: Ethiopia (0.0)\n" +
                "|   admin1 = Conakry: Ethiopia (0.0)\n" +
                "|   admin1 = Tel Aviv: Ethiopia (0.0)\n" +
                "|   admin1 = Makamba: Burundi (1.0)\n" +
                "|   admin1 = Diyala: Ethiopia (0.0)\n" +
                "|   admin1 = Sistan and Baluchestan: Ethiopia (0.0)\n" +
                "|   admin1 = NCT of Delhi: Ethiopia (0.0)\n" +
                "|   admin1 = Deir-ez-Zor: Ethiopia (0.0)\n" +
                "|   admin1 = Dhaka: Ethiopia (0.0)\n" +
                "|   admin1 = Sirnak: Ethiopia (0.0)\n" +
                "|   admin1 = Federal Capital Territory: Ethiopia (0.0)\n" +
                "|   admin1 = Sanaag: Somalia (4.0)\n" +
                "|   admin1 = Hama: Ethiopia (0.0)\n" +
                "|   admin1 = Rumphi: Malawi (1.0)\n" +
                "|   admin1 = Zamfara: Ethiopia (0.0)\n" +
                "|   admin1 = Dire Dawa: Ethiopia (2.0)\n" +
                "|   admin1 = Galguduud: Somalia (1.0)\n" +
                "|   admin1 = Fars: Ethiopia (0.0)\n" +
                "|   admin1 = Erbil: Ethiopia (0.0)\n" +
                "|   admin1 = Asir: Ethiopia (0.0)\n" +
                "|   admin1 = West Bank: Ethiopia (0.0)\n" +
                "|   admin1 = West Azarbaijan: Ethiopia (0.0)\n" +
                "|   admin1 = Delta: Ethiopia (0.0)\n" +
                "|   admin1 = Mazandaran: Ethiopia (0.0)\n" +
                "|   admin1 = Cibitoke: Burundi (1.0)\n" +
                "|   admin1 = Eastern Cape: Ethiopia (0.0)\n" +
                "|   admin1 = Sala al-Din: Ethiopia (0.0)\n" +
                "|   admin1 = Andhra Pradesh: Ethiopia (0.0)\n" +
                "|   admin1 = Upper Nile: South Sudan (5.0)\n" +
                "|   admin1 = Maharashtra: Ethiopia (0.0)\n" +
                "|   admin1 = Western Bahr el Ghazal: South Sudan (3.0)\n" +
                "|   admin1 = Jonglei: South Sudan (1.0)\n" +
                "|   admin1 = Chlef: Ethiopia (0.0)\n" +
                "|   admin1 = Kordestan: Ethiopia (0.0)\n" +
                "|   admin1 = Damascus: Ethiopia (0.0)\n" +
                "|   admin1 = Katanga: Ethiopia (0.0)\n" +
                "|   admin1 = Chittagong: Ethiopia (0.0)\n" +
                "|   admin1 = Rural Damascus: Ethiopia (0.0)\n" +
                "|   admin1 = Brazzaville: Ethiopia (0.0)\n" +
                "|   admin1 = Warrap: South Sudan (1.0)\n" +
                "|   admin1 = Capital: Ethiopia (0.0)\n" +
                "|   admin1 = Mambere-Kadei: Ethiopia (0.0)\n" +
                "|   admin1 = Ajdabiya: Ethiopia (0.0)\n" +
                "|   admin1 = Mandera: Kenya (4.0)\n" +
                "|   admin1 = Kilifi: Kenya (1.0)\n" +
                "|   admin1 = Hormozgan: Ethiopia (0.0)\n" +
                "|   admin1 = Golestan: Ethiopia (0.0)\n" +
                "|   admin1 = Farah: Ethiopia (0.0)\n" +
                "|   admin1 = Orientale: Ethiopia (0.0)\n" +
                "|   admin1 = Hiiraan: Somalia (1.0)\n" +
                "|   admin1 = Lahij: Ethiopia (0.0)\n" +
                "|   admin1 = Razavi Khorasan: Ethiopia (0.0)\n" +
                "|   admin1 = Sahel: Ethiopia (0.0)\n" +
                "|   admin1 = Khyber Pakhtunkhwa: Ethiopia (0.0)\n" +
                "|   admin1 = Punjab: Ethiopia (0.0)\n" +
                "|   admin1 = Shabwah: Ethiopia (0.0)\n" +
                "|   admin1 = Constantine: Ethiopia (0.0)\n" +
                "|   admin1 = Haut-Ogooue: Ethiopia (0.0)\n" +
                "|   admin1 = Kassala: Ethiopia (0.0)\n" +
                "|   admin1 = Maputo City: Mozambique (1.0)\n" +
                "|   admin1 = Meru: Kenya (1.0)\n" +
                "|   admin1 = Nuristan: Ethiopia (0.0)\n" +
                "|   admin1 = Borno: Ethiopia (0.0)\n" +
                "|   admin1 = Nairobi: Kenya (1.0)\n" +
                "|   admin1 = Ghazni: Ethiopia (0.0)\n" +
                "|   admin1 = Anambra: Ethiopia (0.0)\n" +
                "|   admin1 = Hakkari: Ethiopia (0.0)\n" +
                "|   admin1 = Urozgan: Ethiopia (0.0)\n" +
                "|   admin1 = Cairo: Ethiopia (0.0)\n" +
                "|   admin1 = Gaza Strip: Ethiopia (0.0)\n" +
                "|   admin1 = Thi-Qar: Ethiopia (0.0)\n" +
                "|   admin1 = Brong Ahafo: Ethiopia (0.0)\n" +
                "|   admin1 = Ar Riyad: Ethiopia (0.0)\n" +
                "|   admin1 = Midlands: Ethiopia (0.0)\n" +
                "|   admin1 = Tamil Nadu: Ethiopia (0.0)\n" +
                "|   admin1 = Somali: Ethiopia (18.0)\n" +
                "|   admin1 = Imo: Ethiopia (0.0)\n" +
                "|   admin1 = Northern: Ethiopia (0.0)\n" +
                "|   admin1 = HaZafon: Ethiopia (0.0)\n" +
                "|   admin1 = Bari: Somalia (1.0)\n" +
                "|   admin1 = Chhattisgarh: Ethiopia (0.0)\n" +
                "|   admin1 = Istanbul: Ethiopia (0.0)\n" +
                "|   admin1 = Mopti: Ethiopia (0.0)\n" +
                "|   admin1 = Sulaymaniyah: Ethiopia (0.0)\n" +
                "|   admin1 = Taraba: Ethiopia (0.0)\n" +
                "|   admin1 = Kerala: Ethiopia (0.0)\n" +
                "|   admin1 = Sabha: Ethiopia (0.0)\n" +
                "|   admin1 = Baghdad: Ethiopia (0.0)\n" +
                "|   admin1 = Extreme-Nord: Ethiopia (0.0)\n" +
                "|   admin1 = Western: Ethiopia (0.0)\n" +
                "|   admin1 = Haryana: Ethiopia (0.0)\n" +
                "|   admin1 = Kasai-Occidental: Ethiopia (0.0)\n" +
                "|   admin1 = Mulanje: Malawi (1.0)\n" +
                "|   admin1 = Rivers: Ethiopia (0.0)\n" +
                "|   admin1 = Mount Lebanon: Ethiopia (0.0)\n" +
                "|   admin1 = Benue: Ethiopia (0.0)\n" +
                "|   admin1 = Nabatiye: Ethiopia (0.0)\n" +
                "|   admin1 = Sanaa: Ethiopia (0.0)\n" +
                "|   admin1 = Bujumbura Rural: Burundi (2.0)\n" +
                "|   admin1 = East Azarbaijan: Ethiopia (0.0)\n" +
                "|   admin1 = Oromia: Ethiopia (22.0)\n" +
                "|   admin1 = Central Darfur: Ethiopia (0.0)\n" +
                "|   admin1 = Beirut: Ethiopia (0.0)\n" +
                "|   admin1 = Bejaia: Ethiopia (0.0)\n" +
                "|   admin1 = Gao: Ethiopia (0.0)\n" +
                "|   admin1 = Mpumalanga: Ethiopia (0.0)\n" +
                "|   admin1 = Qom: Ethiopia (0.0)\n" +
                "|   admin1 = Nasarawa: Ethiopia (0.0)\n" +
                "|   admin1 = Erzincan: Ethiopia (0.0)\n" +
                "|   admin1 = Sindh: Ethiopia (0.0)\n" +
                "|   admin1 = Khartoum: Ethiopia (0.0)\n" +
                "|   admin1 = Tunis: Ethiopia (0.0)\n" +
                "|   admin1 = Harare: Ethiopia (0.0)\n" +
                "|   admin1 = Balkh: Ethiopia (0.0)\n" +
                "|   admin1 = Marib: Ethiopia (0.0)\n" +
                "|   admin1 = Baalbek-Hermel: Ethiopia (0.0)\n" +
                "|   admin1 = Amhara: Ethiopia (1.0)\n" +
                "|   admin1 = North Darfur: Ethiopia (0.0)\n" +
                "|   admin1 = Tillabery: Ethiopia (0.0)\n" +
                "|   admin1 = Darnah: Ethiopia (0.0)\n" +
                "|   admin1 = Nord-Ouest: Ethiopia (0.0)\n" +
                "|   admin1 = Idleb: Ethiopia (0.0)\n" +
                "|   admin1 = Afyonkarahisar: Ethiopia (0.0)\n" +
                "|   admin1 = Kirkuk: Ethiopia (0.0)\n" +
                "|   admin1 = Aden: Ethiopia (0.0)\n" +
                "|   admin1 = West Bengal: Ethiopia (0.0)\n" +
                "|   admin1 = Sud-Ouest: Ethiopia (0.0)\n" +
                "|   admin1 = Bangui: Ethiopia (0.0)\n" +
                "|   admin1 = Ninewa: Ethiopia (0.0)\n" +
                "|   admin1 = Sud-Kivu: Ethiopia (0.0)\n" +
                "|   admin1 = Balochistan: Ethiopia (0.0)\n" +
                "|   admin1 = Anbar: Ethiopia (0.0)\n" +
                "|   admin1 = Khuzestan: Ethiopia (0.0)\n" +
                "|   admin1 = Lagos: Ethiopia (0.0)\n" +
                "|   admin1 = Najran: Ethiopia (0.0)\n" +
                "|   admin1 = Bayelsa: Ethiopia (0.0)\n" +
                "|   admin1 = North West: Ethiopia (0.0)\n" +
                "|   admin1 = Lakes: South Sudan (2.0)\n" +
                "|   admin1 = Tunceli: Ethiopia (0.0)\n" +
                "|   admin1 = Bay: Somalia (3.0)\n" +
                "|   admin1 = Plateau: Ethiopia (0.0)\n" +
                "|   admin1 = Qazvin: Ethiopia (0.0)\n" +
                "|   admin1 = Lacs: Ethiopia (0.0)\n" +
                "|   admin1 = Nouakchott: Ethiopia (0.0)\n" +
                "|   admin1 = Dar'a: Ethiopia (0.0)\n" +
                "|   admin1 = Bushehr: Ethiopia (0.0)\n" +
                "|   admin1 = North: Ethiopia (0.0)\n" +
                "|   admin1 = Kankan: Ethiopia (0.0)\n" +
                "|   admin1 = Uttar Pradesh: Ethiopia (0.0)\n" +
                "|   admin1 = Jizan: Ethiopia (0.0)\n" +
                "|   admin1 = Al Bayda: Ethiopia (0.0)\n" +
                "|   admin1 = Al Balqa: Ethiopia (0.0)\n" +
                "|   admin1 = Karnataka: Ethiopia (0.0)\n" +
                "|   admin1 = Karas: Ethiopia (0.0)\n" +
                "|   admin1 = Zanjan: Ethiopia (0.0)\n" +
                "|   admin1 = Nord-Kivu: Ethiopia (0.0)\n" +
                "|   admin1 = Banaadir: Somalia (12.0)\n" +
                "|   admin1 = Al Mahrah: Ethiopia (0.0)\n" +
                "|   admin1 = Al Hizam Al Akhdar: Ethiopia (0.0)\n" +
                "|   admin1 = KwaZulu-Natal: Ethiopia (0.0)\n" +
                "|   admin1 = Puducherry: Ethiopia (0.0)\n" +
                "|   admin1 = West Java: Ethiopia (0.0)\n" +
                "|   admin1 = Central: Ethiopia (0.0)\n" +
                "|   admin1 = Ouham Pende: Ethiopia (0.0)\n" +
                "|   admin1 = Telangana: Ethiopia (0.0)\n" +
                "|   admin1 = Upper East: Ethiopia (0.0)\n" +
                "|   admin1 = West: Ethiopia (0.0)\n" +
                "|   admin1 = Central Equatoria: South Sudan (1.0)\n" +
                "|   admin1 = Surt: Ethiopia (0.0)\n" +
                "|   admin1 = Edo: Ethiopia (0.0)\n" +
                "|   admin1 = Mardin: Ethiopia (0.0)\n" +
                "|   admin1 = Al Jawf: Ethiopia (0.0)\n" +
                "|   admin1 = North Sinai: Ethiopia (0.0)\n" +
                "|   admin1 = Sadah: Ethiopia (0.0)\n" +
                "|   admin1 = Abyan: Ethiopia (0.0)\n" +
                "|   admin1 = Kunduz: Ethiopia (0.0)\n" +
                "|   admin1 = Nangarhar: Ethiopia (0.0)\n" +
                "|   admin1 = Aleppo: Ethiopia (0.0)\n" +
                "|   admin1 = Bekaa: Ethiopia (0.0)\n" +
                "|   admin1 = Ekiti: Ethiopia (0.0)\n" +
                "|   admin1 = Takhar: Ethiopia (0.0)\n" +
                "|   admin1 = Hatay: Ethiopia (0.0)\n" +
                "|   admin1 = Sool: Somalia (3.0)\n" +
                "|   admin1 = Konya: Ethiopia (0.0)\n" +
                "|   admin1 = Amanat al Asimah: Ethiopia (0.0)\n" +
                "|   admin1 = Unity: South Sudan (2.0)\n" +
                "|   admin1 = Gitega: Burundi (2.0)\n" +
                "|   admin1 = HaDarom: Ethiopia (0.0)\n" +
                "|   admin1 = Antananarivo: Madagascar (3.0)\n" +
                "|   admin1 = Nugaal: Somalia (1.0)\n" +
                "|   admin1 = Enugu: Ethiopia (0.0)\n" +
                "|   admin1 = Baringo: Kenya (2.0)\n" +
                "|   admin1 = Alborz: Ethiopia (0.0)\n" +
                "|   admin1 = Centre: Ethiopia (0.0)\n" +
                "|   admin1 = Basrah: Ethiopia (0.0)\n" +
                "|   admin1 = Suqutra: Ethiopia (0.0)\n" +
                "|   admin1 = Rajasthan: Ethiopia (0.0)\n" +
                "|   admin1 = Hamadan: Ethiopia (0.0)\n" +
                "|   admin1 = Kerman: Ethiopia (0.0)\n" +
                "|   admin1 = Izmir: Ethiopia (0.0)\n" +
                "|   admin1 = Jubbada Hoose: Somalia (1.0)\n" +
                "|   admin1 = Al Hudaydah: Ethiopia (0.0)\n" +
                "|   admin1 = Chandigarh: Ethiopia (0.0)\n" +
                "|   admin1 = Hajjah: Ethiopia (0.0)\n" +
                "|   admin1 = Bauchi: Ethiopia (0.0)\n" +
                "|   admin1 = Taizz: Ethiopia (0.0)\n" +
                "|   admin1 = Dakhlet Nouadhibou: Ethiopia (0.0)\n" +
                "|   admin1 = Gilan: Ethiopia (0.0)\n" +
                "|   admin1 = Shabeellaha Hoose: Somalia (8.0)\n" +
                "|   admin1 = Kohgiluyeh and Buyer Ahmad: Ethiopia (0.0)\n" +
                "|   admin1 = Homs: Ethiopia (0.0)\n" +
                "|   admin1 = Western Cape: Ethiopia (0.0)\n" +
                "|   admin1 = Nana-Gribizi: Ethiopia (0.0)\n" +
                "region = Southern Asia\n" +
                "|   data_id <= 1601396\n" +
                "|   |   data_id <= 1601157: Bangladesh (2.0)\n" +
                "|   |   data_id > 1601157: India (72.0)\n" +
                "|   data_id > 1601396\n" +
                "|   |   data_id <= 1602543\n" +
                "|   |   |   data_id <= 1601678: Nepal (5.0/1.0)\n" +
                "|   |   |   data_id > 1601678: Afghanistan (23.0)\n" +
                "|   |   data_id > 1602543: Pakistan (30.0)\n" +
                "region = South-Eastern Asia: Indonesia (1.0)\n" +
                "region = Western Africa: Nigeria (76.0/22.0)\n" +
                "region = Northern Africa\n" +
                "|   admin1 = Karonga: Libya (0.0)\n" +
                "|   admin1 = Kinshasa: Libya (0.0)\n" +
                "|   admin1 = Ankara: Libya (0.0)\n" +
                "|   admin1 = Adamawa: Libya (0.0)\n" +
                "|   admin1 = Bujumbura Mairie: Libya (0.0)\n" +
                "|   admin1 = Woqooyi Galbeed: Libya (0.0)\n" +
                "|   admin1 = Helmand: Libya (0.0)\n" +
                "|   admin1 = Skikda: Algeria (2.0)\n" +
                "|   admin1 = Ibb: Libya (0.0)\n" +
                "|   admin1 = Isfahan: Libya (0.0)\n" +
                "|   admin1 = Assam: Libya (0.0)\n" +
                "|   admin1 = Kaduna: Libya (0.0)\n" +
                "|   admin1 = Al-Hasakeh: Libya (0.0)\n" +
                "|   admin1 = Babylon: Libya (0.0)\n" +
                "|   admin1 = Oyo: Libya (0.0)\n" +
                "|   admin1 = Lattakia: Libya (0.0)\n" +
                "|   admin1 = South: Libya (0.0)\n" +
                "|   admin1 = Odisha: Libya (0.0)\n" +
                "|   admin1 = Kermanshah: Libya (0.0)\n" +
                "|   admin1 = Gauteng: Libya (0.0)\n" +
                "|   admin1 = Al Mahwit: Libya (0.0)\n" +
                "|   admin1 = Ogun: Libya (0.0)\n" +
                "|   admin1 = Badakhshan: Libya (0.0)\n" +
                "|   admin1 = Tehran: Libya (0.0)\n" +
                "|   admin1 = Al Asimah: Libya (0.0)\n" +
                "|   admin1 = Jammu and Kashmir: Libya (0.0)\n" +
                "|   admin1 = Conakry: Libya (0.0)\n" +
                "|   admin1 = Tel Aviv: Libya (0.0)\n" +
                "|   admin1 = Makamba: Libya (0.0)\n" +
                "|   admin1 = Diyala: Libya (0.0)\n" +
                "|   admin1 = Sistan and Baluchestan: Libya (0.0)\n" +
                "|   admin1 = NCT of Delhi: Libya (0.0)\n" +
                "|   admin1 = Deir-ez-Zor: Libya (0.0)\n" +
                "|   admin1 = Dhaka: Libya (0.0)\n" +
                "|   admin1 = Sirnak: Libya (0.0)\n" +
                "|   admin1 = Federal Capital Territory: Libya (0.0)\n" +
                "|   admin1 = Sanaag: Libya (0.0)\n" +
                "|   admin1 = Hama: Libya (0.0)\n" +
                "|   admin1 = Rumphi: Libya (0.0)\n" +
                "|   admin1 = Zamfara: Libya (0.0)\n" +
                "|   admin1 = Dire Dawa: Libya (0.0)\n" +
                "|   admin1 = Galguduud: Libya (0.0)\n" +
                "|   admin1 = Fars: Libya (0.0)\n" +
                "|   admin1 = Erbil: Libya (0.0)\n" +
                "|   admin1 = Asir: Libya (0.0)\n" +
                "|   admin1 = West Bank: Libya (0.0)\n" +
                "|   admin1 = West Azarbaijan: Libya (0.0)\n" +
                "|   admin1 = Delta: Libya (0.0)\n" +
                "|   admin1 = Mazandaran: Libya (0.0)\n" +
                "|   admin1 = Cibitoke: Libya (0.0)\n" +
                "|   admin1 = Eastern Cape: Libya (0.0)\n" +
                "|   admin1 = Sala al-Din: Libya (0.0)\n" +
                "|   admin1 = Andhra Pradesh: Libya (0.0)\n" +
                "|   admin1 = Upper Nile: Libya (0.0)\n" +
                "|   admin1 = Maharashtra: Libya (0.0)\n" +
                "|   admin1 = Western Bahr el Ghazal: Libya (0.0)\n" +
                "|   admin1 = Jonglei: Libya (0.0)\n" +
                "|   admin1 = Chlef: Algeria (1.0)\n" +
                "|   admin1 = Kordestan: Libya (0.0)\n" +
                "|   admin1 = Damascus: Libya (0.0)\n" +
                "|   admin1 = Katanga: Libya (0.0)\n" +
                "|   admin1 = Chittagong: Libya (0.0)\n" +
                "|   admin1 = Rural Damascus: Libya (0.0)\n" +
                "|   admin1 = Brazzaville: Libya (0.0)\n" +
                "|   admin1 = Warrap: Libya (0.0)\n" +
                "|   admin1 = Capital: Libya (0.0)\n" +
                "|   admin1 = Mambere-Kadei: Libya (0.0)\n" +
                "|   admin1 = Ajdabiya: Libya (1.0)\n" +
                "|   admin1 = Mandera: Libya (0.0)\n" +
                "|   admin1 = Kilifi: Libya (0.0)\n" +
                "|   admin1 = Hormozgan: Libya (0.0)\n" +
                "|   admin1 = Golestan: Libya (0.0)\n" +
                "|   admin1 = Farah: Libya (0.0)\n" +
                "|   admin1 = Orientale: Libya (0.0)\n" +
                "|   admin1 = Hiiraan: Libya (0.0)\n" +
                "|   admin1 = Lahij: Libya (0.0)\n" +
                "|   admin1 = Razavi Khorasan: Libya (0.0)\n" +
                "|   admin1 = Sahel: Libya (0.0)\n" +
                "|   admin1 = Khyber Pakhtunkhwa: Libya (0.0)\n" +
                "|   admin1 = Punjab: Libya (0.0)\n" +
                "|   admin1 = Shabwah: Libya (0.0)\n" +
                "|   admin1 = Constantine: Algeria (2.0)\n" +
                "|   admin1 = Haut-Ogooue: Libya (0.0)\n" +
                "|   admin1 = Kassala: Sudan (1.0)\n" +
                "|   admin1 = Maputo City: Libya (0.0)\n" +
                "|   admin1 = Meru: Libya (0.0)\n" +
                "|   admin1 = Nuristan: Libya (0.0)\n" +
                "|   admin1 = Borno: Libya (0.0)\n" +
                "|   admin1 = Nairobi: Libya (0.0)\n" +
                "|   admin1 = Ghazni: Libya (0.0)\n" +
                "|   admin1 = Anambra: Libya (0.0)\n" +
                "|   admin1 = Hakkari: Libya (0.0)\n" +
                "|   admin1 = Urozgan: Libya (0.0)\n" +
                "|   admin1 = Cairo: Egypt (1.0)\n" +
                "|   admin1 = Gaza Strip: Libya (0.0)\n" +
                "|   admin1 = Thi-Qar: Libya (0.0)\n" +
                "|   admin1 = Brong Ahafo: Libya (0.0)\n" +
                "|   admin1 = Ar Riyad: Libya (0.0)\n" +
                "|   admin1 = Midlands: Libya (0.0)\n" +
                "|   admin1 = Tamil Nadu: Libya (0.0)\n" +
                "|   admin1 = Somali: Libya (0.0)\n" +
                "|   admin1 = Imo: Libya (0.0)\n" +
                "|   admin1 = Northern: Libya (0.0)\n" +
                "|   admin1 = HaZafon: Libya (0.0)\n" +
                "|   admin1 = Bari: Libya (0.0)\n" +
                "|   admin1 = Chhattisgarh: Libya (0.0)\n" +
                "|   admin1 = Istanbul: Libya (0.0)\n" +
                "|   admin1 = Mopti: Libya (0.0)\n" +
                "|   admin1 = Sulaymaniyah: Libya (0.0)\n" +
                "|   admin1 = Taraba: Libya (0.0)\n" +
                "|   admin1 = Kerala: Libya (0.0)\n" +
                "|   admin1 = Sabha: Libya (9.0)\n" +
                "|   admin1 = Baghdad: Libya (0.0)\n" +
                "|   admin1 = Extreme-Nord: Libya (0.0)\n" +
                "|   admin1 = Western: Libya (0.0)\n" +
                "|   admin1 = Haryana: Libya (0.0)\n" +
                "|   admin1 = Kasai-Occidental: Libya (0.0)\n" +
                "|   admin1 = Mulanje: Libya (0.0)\n" +
                "|   admin1 = Rivers: Libya (0.0)\n" +
                "|   admin1 = Mount Lebanon: Libya (0.0)\n" +
                "|   admin1 = Benue: Libya (0.0)\n" +
                "|   admin1 = Nabatiye: Libya (0.0)\n" +
                "|   admin1 = Sanaa: Libya (0.0)\n" +
                "|   admin1 = Bujumbura Rural: Libya (0.0)\n" +
                "|   admin1 = East Azarbaijan: Libya (0.0)\n" +
                "|   admin1 = Oromia: Libya (0.0)\n" +
                "|   admin1 = Central Darfur: Sudan (2.0)\n" +
                "|   admin1 = Beirut: Libya (0.0)\n" +
                "|   admin1 = Bejaia: Algeria (1.0)\n" +
                "|   admin1 = Gao: Libya (0.0)\n" +
                "|   admin1 = Mpumalanga: Libya (0.0)\n" +
                "|   admin1 = Qom: Libya (0.0)\n" +
                "|   admin1 = Nasarawa: Libya (0.0)\n" +
                "|   admin1 = Erzincan: Libya (0.0)\n" +
                "|   admin1 = Sindh: Libya (0.0)\n" +
                "|   admin1 = Khartoum: Sudan (1.0)\n" +
                "|   admin1 = Tunis: Tunisia (2.0)\n" +
                "|   admin1 = Harare: Libya (0.0)\n" +
                "|   admin1 = Balkh: Libya (0.0)\n" +
                "|   admin1 = Marib: Libya (0.0)\n" +
                "|   admin1 = Baalbek-Hermel: Libya (0.0)\n" +
                "|   admin1 = Amhara: Libya (0.0)\n" +
                "|   admin1 = North Darfur: Sudan (2.0)\n" +
                "|   admin1 = Tillabery: Libya (0.0)\n" +
                "|   admin1 = Darnah: Libya (10.0)\n" +
                "|   admin1 = Nord-Ouest: Libya (0.0)\n" +
                "|   admin1 = Idleb: Libya (0.0)\n" +
                "|   admin1 = Afyonkarahisar: Libya (0.0)\n" +
                "|   admin1 = Kirkuk: Libya (0.0)\n" +
                "|   admin1 = Aden: Libya (0.0)\n" +
                "|   admin1 = West Bengal: Libya (0.0)\n" +
                "|   admin1 = Sud-Ouest: Libya (0.0)\n" +
                "|   admin1 = Bangui: Libya (0.0)\n" +
                "|   admin1 = Ninewa: Libya (0.0)\n" +
                "|   admin1 = Sud-Kivu: Libya (0.0)\n" +
                "|   admin1 = Balochistan: Libya (0.0)\n" +
                "|   admin1 = Anbar: Libya (0.0)\n" +
                "|   admin1 = Khuzestan: Libya (0.0)\n" +
                "|   admin1 = Lagos: Libya (0.0)\n" +
                "|   admin1 = Najran: Libya (0.0)\n" +
                "|   admin1 = Bayelsa: Libya (0.0)\n" +
                "|   admin1 = North West: Libya (0.0)\n" +
                "|   admin1 = Lakes: Libya (0.0)\n" +
                "|   admin1 = Tunceli: Libya (0.0)\n" +
                "|   admin1 = Bay: Libya (0.0)\n" +
                "|   admin1 = Plateau: Libya (0.0)\n" +
                "|   admin1 = Qazvin: Libya (0.0)\n" +
                "|   admin1 = Lacs: Libya (0.0)\n" +
                "|   admin1 = Nouakchott: Libya (0.0)\n" +
                "|   admin1 = Dar'a: Libya (0.0)\n" +
                "|   admin1 = Bushehr: Libya (0.0)\n" +
                "|   admin1 = North: Libya (0.0)\n" +
                "|   admin1 = Kankan: Libya (0.0)\n" +
                "|   admin1 = Uttar Pradesh: Libya (0.0)\n" +
                "|   admin1 = Jizan: Libya (0.0)\n" +
                "|   admin1 = Al Bayda: Libya (0.0)\n" +
                "|   admin1 = Al Balqa: Libya (0.0)\n" +
                "|   admin1 = Karnataka: Libya (0.0)\n" +
                "|   admin1 = Karas: Libya (0.0)\n" +
                "|   admin1 = Zanjan: Libya (0.0)\n" +
                "|   admin1 = Nord-Kivu: Libya (0.0)\n" +
                "|   admin1 = Banaadir: Libya (0.0)\n" +
                "|   admin1 = Al Mahrah: Libya (0.0)\n" +
                "|   admin1 = Al Hizam Al Akhdar: Libya (4.0)\n" +
                "|   admin1 = KwaZulu-Natal: Libya (0.0)\n" +
                "|   admin1 = Puducherry: Libya (0.0)\n" +
                "|   admin1 = West Java: Libya (0.0)\n" +
                "|   admin1 = Central: Libya (0.0)\n" +
                "|   admin1 = Ouham Pende: Libya (0.0)\n" +
                "|   admin1 = Telangana: Libya (0.0)\n" +
                "|   admin1 = Upper East: Libya (0.0)\n" +
                "|   admin1 = West: Libya (0.0)\n" +
                "|   admin1 = Central Equatoria: Libya (0.0)\n" +
                "|   admin1 = Surt: Libya (3.0)\n" +
                "|   admin1 = Edo: Libya (0.0)\n" +
                "|   admin1 = Mardin: Libya (0.0)\n" +
                "|   admin1 = Al Jawf: Libya (0.0)\n" +
                "|   admin1 = North Sinai: Egypt (6.0)\n" +
                "|   admin1 = Sadah: Libya (0.0)\n" +
                "|   admin1 = Abyan: Libya (0.0)\n" +
                "|   admin1 = Kunduz: Libya (0.0)\n" +
                "|   admin1 = Nangarhar: Libya (0.0)\n" +
                "|   admin1 = Aleppo: Libya (0.0)\n" +
                "|   admin1 = Bekaa: Libya (0.0)\n" +
                "|   admin1 = Ekiti: Libya (0.0)\n" +
                "|   admin1 = Takhar: Libya (0.0)\n" +
                "|   admin1 = Hatay: Libya (0.0)\n" +
                "|   admin1 = Sool: Libya (0.0)\n" +
                "|   admin1 = Konya: Libya (0.0)\n" +
                "|   admin1 = Amanat al Asimah: Libya (0.0)\n" +
                "|   admin1 = Unity: Libya (0.0)\n" +
                "|   admin1 = Gitega: Libya (0.0)\n" +
                "|   admin1 = HaDarom: Libya (0.0)\n" +
                "|   admin1 = Antananarivo: Libya (0.0)\n" +
                "|   admin1 = Nugaal: Libya (0.0)\n" +
                "|   admin1 = Enugu: Libya (0.0)\n" +
                "|   admin1 = Baringo: Libya (0.0)\n" +
                "|   admin1 = Alborz: Libya (0.0)\n" +
                "|   admin1 = Centre: Libya (0.0)\n" +
                "|   admin1 = Basrah: Libya (0.0)\n" +
                "|   admin1 = Suqutra: Libya (0.0)\n" +
                "|   admin1 = Rajasthan: Libya (0.0)\n" +
                "|   admin1 = Hamadan: Libya (0.0)\n" +
                "|   admin1 = Kerman: Libya (0.0)\n" +
                "|   admin1 = Izmir: Libya (0.0)\n" +
                "|   admin1 = Jubbada Hoose: Libya (0.0)\n" +
                "|   admin1 = Al Hudaydah: Libya (0.0)\n" +
                "|   admin1 = Chandigarh: Libya (0.0)\n" +
                "|   admin1 = Hajjah: Libya (0.0)\n" +
                "|   admin1 = Bauchi: Libya (0.0)\n" +
                "|   admin1 = Taizz: Libya (0.0)\n" +
                "|   admin1 = Dakhlet Nouadhibou: Libya (0.0)\n" +
                "|   admin1 = Gilan: Libya (0.0)\n" +
                "|   admin1 = Shabeellaha Hoose: Libya (0.0)\n" +
                "|   admin1 = Kohgiluyeh and Buyer Ahmad: Libya (0.0)\n" +
                "|   admin1 = Homs: Libya (0.0)\n" +
                "|   admin1 = Western Cape: Libya (0.0)\n" +
                "|   admin1 = Nana-Gribizi: Libya (0.0)";
    }
}
