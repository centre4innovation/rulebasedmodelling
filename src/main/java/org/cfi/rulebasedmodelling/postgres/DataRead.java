package org.cfi.rulebasedmodelling.postgres;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.cfi.rulebasedmodelling.api.Config;
import org.cfi.rulebasedmodelling.postgres.datamanagement.ColumnDefinitions;
import org.cfi.rulebasedmodelling.postgres.datamanagement.DataSet;
import org.cfi.rulebasedmodelling.postgres.datamanagement.TableBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Aaron Swaving
 * created on 21/05/2018
 **/
public class DataRead {


    private final static Logger logger = LoggerFactory.getLogger(DataRead.class);

    private Config config;

    public DataRead(Config config){
        this.config = config;
    }

    public List<DataSet> getDataSetInformation(String name){
        return config.dao.getReadData().datasetInformation(name);
    }

    public List<ColumnDefinitions> getAllColumns(String name){
        return config.dao.getReadData().datasetColumns(name);
    }

    public List<String> getStringLabels(String table, String column){
        return config.dao.getReadData().selectLabels(table,column);
    }


    /** Gets the orignal data table back.
     * @param name is the table name.
     * @param selectedColumns is a string array of columns of interest. If empty then the full table is returned.
     * @return returns a TableBuilder. Use getQuery() to return query string.
     * **/
    public TableBuilder getTable(String name, List<String> selectedColumns){
        if(selectedColumns.size() == 0) {
            TableBuilder qb = new TableBuilder(getAllColumns(name));
            return qb;
        }
        else{
            if(isNotTempTable(name)) {
                List<ColumnDefinitions> selected = config.dao.getReadData().selectColumns(name, selectedColumns);
                List<ColumnDefinitions> reorderSelection = reorder(selected, selectedColumns);
                TableBuilder qb = new TableBuilder(reorderSelection);
                return qb;
            }
            else{
                TableBuilder qb = new TableBuilder().getTempTable(name,selectedColumns);
                return qb;
            }
        }
    }

    private List<ColumnDefinitions> reorder(List<ColumnDefinitions> selected, List<String> selectedColumns){
        Map<String, ColumnDefinitions> columns = Maps.newLinkedHashMap();
        List<ColumnDefinitions> reordered = Lists.newArrayList();
        selected.stream().forEach(cd -> columns.put(cd.getName(),cd));
        for(String col: selectedColumns){
            if(columns.containsKey(col)) {
                reordered.add(columns.get(col));
            }
        }
        return reordered;
    }

    private boolean isNotTempTable(String table){
        List<DataSet> ds = config.dao.getReadData().getdatasets();
        boolean isPermanent = ds.stream().anyMatch(t -> t.getName().equals(table));
        return isPermanent;
    }

    public List<String> getColumnNames(String table){
        List<DataSet> ds = config.dao.getReadData().getdatasets();
        boolean isPermanent = ds.stream().anyMatch(t -> t.getName().equals(table));
        if(isPermanent){
            return getAllColumns(table).stream().map(cd -> cd.getName()).collect(Collectors.toList());
        }
        else{
            return config.dao.getReadData().tableColumNames(table);
        }
    }

    public List<String> getPermanentTables(){
        return config.dao.getReadData().getPermanentTables();
    }

    public void constructAndStorePermanentTables(){
        for(String table : getPermanentTables()) {
            String query = buildPermanentTables(table, Lists.newArrayList()).getQuery();
            config.dao.getLoadData().createTemporaryTable(query, table);
        }
    }

    public void constructAndStorePermanentTable(String table){
            String query = buildPermanentTables(table, Lists.newArrayList()).getQuery();
            config.dao.getLoadData().createTemporaryTable(query, table);
    }

    public TableBuilder buildTable(String tableName, List<String> columnNames) {
        List<DataSet> ds = config.dao.getReadData().getdatasets();
        boolean isPermanent = ds.stream().anyMatch(t -> t.getName().equals(tableName));
        if(config.dao.getReadData().getAvailableTables().contains(tableName) || !isPermanent){
            return new TableBuilder().selectTable(tableName,columnNames);
        }
        else{
            return getTable(tableName, columnNames);
        }
    }

    public TableBuilder buildPermanentTables(String tableName, List<String> columnNames){
        return getTable(tableName, columnNames);
    }



    /** Gets the orignal data table back.
     * @param name is the table name.
     * @return returns a TableBuilder. Use getQuery() to return query string.
     * **/
    public TableBuilder getTable(String name){
        TableBuilder qb = new TableBuilder(getAllColumns(name));
        return qb;
    }

    public List<Map<String, Object>> executeQuery(String query){
        return config.dao.getReadData().executeQuery(query);
    }

    public void storeAsTable(String query, String tableName){
        config.dao.getLoadData().createTemporaryTable(query, tableName);
    }






}
