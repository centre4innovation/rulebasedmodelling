package org.cfi.rulebasedmodelling.postgres.dao;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.cfi.rulebasedmodelling.postgres.datamanagement.*;
import org.cfi.rulebasedmodelling.time.DateTimeUtil;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.mapper.MapMapper;
import org.jdbi.v3.sqlobject.SqlObject;
import org.jdbi.v3.sqlobject.config.RegisterRowMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author Aaron Swaving
 * created on 21/05/2018
 **/
public interface ReadData extends SqlObject{

    Logger logger = LoggerFactory.getLogger(ReadData.class);

    @RegisterRowMapper(DataSetMapper.class)
    @SqlQuery(//language=sql
            "SELECT * FROM dataset WHERE name = :datasetName;")
    List<DataSet> datasetInformation(@Bind("datasetName") String datasetName);

    @RegisterRowMapper(ColumnDefintionsMapper.class)
    @SqlQuery(//language=sql
            "SELECT * FROM columndefinitions WHERE datasetid IN (SELECT id FROM dataset WHERE name = :datasetName);")
    List<ColumnDefinitions> datasetColumns(@Bind("datasetName") String datasetName);

    @RegisterRowMapper(StringValuesMapper.class)
    @SqlQuery(//language=sql
            "SELECT * FROM stringvalues WHERE columnid IN (SELECT columnid FROM columndefinitions WHERE datasetid IN (SELECT id FROM dataset WHERE name = :datasetName));")
    List<StringValues> stringvalues(@Bind("datasetName") String datasetName);

    @SqlQuery(//language=sql
            "SELECT MAX(columnid) FROM columndefinitions;")
    List<Long> getMaxColumnId();

    @SqlQuery(//language=sql
            "SELECT MAX(id) FROM dataset;")
    List<Long> getMaxDatasetId();

    default List<String> tableExists(String dataset){
        Handle handle = getHandle();
        List<String> tableList = handle.createQuery("SELECT relname FROM pg_catalog.pg_class WHERE relkind = 'r' AND relname = " + dataset + ";")
                .mapTo(String.class).list();
        System.out.println(tableList);
        return tableList;
    }

    default List<ColumnDefinitions> selectColumns(String dataset, List<String> columns){
        Handle handle = getHandle();
        return handle.createQuery("SELECT * FROM columndefinitions WHERE " +
                "datasetid IN (SELECT datasetid FROM dataset WHERE name = :datasetName) AND name IN (<listofcols>);")
                .bind("datasetName",dataset)
                .bindList("listofcols",columns).mapTo(ColumnDefinitions.class).list();
    }


    default List<String> selectLabels(String dataset, String column){
        Handle handle = getHandle();
        return handle.createQuery("SELECT DISTINCT value FROM stringvalues WHERE columnid IN (SELECT columnid FROM columndefinitions WHERE " +
                "datasetid IN (SELECT datasetid FROM dataset WHERE name = :datasetName) AND name LIKE :column);")
                .bind("datasetName", dataset)
                .bind("column",column).mapTo(String.class).list();
    }

    default int getRowCount(String table){
        Handle handle = getHandle();
        List<Integer> size = handle.createQuery("SELECT COUNT(*) FROM " + table + ";").mapTo(Integer.class).list();
        return size.get(0);
    }

    default int getMaxRowId(String dataset, String datatype){
        Handle handle = getHandle();
        List<Integer> maxRowId = handle.createQuery("SELECT MAX(rowid) FROM "+ datatype +" WHERE columnid IN (SELECT columnid FROM columndefinitions " +
                "WHERE datasetid IN (SELECT id FROM dataset WHERE name = :datasetName));")
                .bind("datasetName", dataset).mapTo(Integer.class).list();
        int row = maxRowId.get(0) == null ? 1 : maxRowId.get(0);
        return row;
    }

    default List<String> getAvailableModels(){
        Handle handle = getHandle();
        List<String> models = handle.createQuery("SELECT value FROM stringvalues WHERE columnid IN " +
                "(SELECT columnid FROM columndefinitions WHERE name = \'model_name\')").mapTo(String.class).list();
        logger.info("Retrieve all saved models");
        return models;
    }

    default String getColumnOfInterestForModel(String modelName, String columnName){
        Handle handle = getHandle();
        List<String> cols = handle.createQuery("SELECT value FROM stringvalues WHERE columnid IN (SELECT columnid FROM columndefinitions " +
                "WHERE datasetid IN (SELECT id FROM dataset WHERE name = 'created_models') AND name = :columnname) " +
                "AND rowid IN (SELECT rowid FROM stringvalues WHERE value = :modelname);")
                .bind("modelname", modelName)
                .bind("columnname", columnName)
                .mapTo(String.class).list();
        return cols.get(0);
    }

    default String getColumnsUsed(String modelName){
        Handle handle = getHandle();
        List<String> cols = handle.createQuery("SELECT value FROM stringvalues WHERE columnid IN (SELECT columnid FROM columndefinitions " +
                "WHERE datasetid IN (SELECT id FROM dataset WHERE name = 'created_models') AND name = 'column_names') " +
                "AND rowid IN (SELECT rowid FROM stringvalues WHERE value = :modelname);")
                .bind("modelname", modelName).mapTo(String.class).list();
        return cols.get(0);
    }

    default String getTargetIndex(String modelName){
        Handle handle = getHandle();
        List<String> cols = handle.createQuery("SELECT value FROM stringvalues WHERE columnid IN (SELECT columnid FROM columndefinitions " +
                "WHERE datasetid IN (SELECT id FROM dataset WHERE name = 'created_models') AND name = 'target_index') " +
                "AND rowid IN (SELECT rowid FROM stringvalues WHERE value = :modelname);")
                .bind("modelname", modelName).mapTo(String.class).list();
        return cols.get(0);
    }

    default String getColumnTypes(String modelName){
        Handle handle = getHandle();
        List<String> cols = handle.createQuery("SELECT value FROM stringvalues WHERE columnid IN (SELECT columnid FROM columndefinitions " +
                "WHERE datasetid IN (SELECT id FROM dataset WHERE name = 'created_models') AND name = 'column_types') " +
                "AND rowid IN (SELECT rowid FROM stringvalues WHERE value = :modelname);")
                .bind("modelname", modelName).mapTo(String.class).list();
        return cols.get(0);
    }

    default String getTargetValues(String modelName){
        Handle handle = getHandle();
        List<String> cols = handle.createQuery("SELECT value FROM stringvalues WHERE columnid IN (SELECT columnid FROM columndefinitions " +
                "WHERE datasetid IN (SELECT id FROM dataset WHERE name = 'created_models') AND name = 'target_values') " +
                "AND rowid IN (SELECT rowid FROM stringvalues WHERE value = :modelname);")
                .bind("modelname", modelName).mapTo(String.class).list();
        return cols.get(0);
    }

    default String getTargetName(String modelName){
        Handle handle = getHandle();
        List<String> cols = handle.createQuery("SELECT value FROM stringvalues WHERE columnid IN (SELECT columnid FROM columndefinitions " +
                "WHERE datasetid IN (SELECT id FROM dataset WHERE name = 'created_models') AND name = 'target_name') " +
                "AND rowid IN (SELECT rowid FROM stringvalues WHERE value = :modelname);")
                .bind("modelname", modelName).mapTo(String.class).list();
        return cols.get(0);
    }

    default String getConditionalPaths(String modelName){
        Handle handle = getHandle();
        List<String> cols = handle.createQuery("SELECT value FROM stringvalues WHERE columnid IN (SELECT columnid FROM columndefinitions " +
                "WHERE datasetid IN (SELECT id FROM dataset WHERE name = 'created_models') AND name = 'conditional_paths') " +
                "AND rowid IN (SELECT rowid FROM stringvalues WHERE value = :modelname);")
                .bind("modelname", modelName).mapTo(String.class).list();
        return cols.get(0);
    }

    default List<String> getAvailableTables(){
        Handle handle = getHandle();
        List<String> tempTables = handle.createQuery("SELECT * FROM pg_catalog.pg_class WHERE relkind = 'r' AND relowner " +
                "in (SELECT relowner FROM pg_catalog.pg_class WHERE relname = 'dataset');").mapTo(String.class).list();
        Set filterSet = Sets.newHashSet("databasechangelog", "databasechangeloglock", "dataset",
                "doublevalues", "stringvalues", "columndefinitions", "datetimevalues", "jsondoc", "textdoc");
        tempTables = tempTables.stream().filter(tt -> !filterSet.contains(tt)).collect(Collectors.toList());
        List<String> permanentTables = handle.createQuery("SELECT name FROM dataset").mapTo(String.class).list();
        for(String table : permanentTables) {
            if(!tempTables.contains(table)) {
                tempTables.add(table);
            }
        }
        logger.info(tempTables.toString());
        return tempTables;
    }

    default List<String> getPermanentTables(){
        Handle handle = getHandle();
        List<String> permanentTables = handle.createQuery("SELECT name FROM dataset").mapTo(String.class).list();
        return permanentTables;
    }

    default boolean createdModelsExist(){
        Handle handle = getHandle();
        List<String> permanentTables = handle.createQuery("SELECT name FROM dataset WHERE name = \'created_models\'").mapTo(String.class).list();
        if(permanentTables.size() == 1){
            return true;
        }
        else{
            return false;
        }
    }

    default List<Map<String,Object>> executeQuery(String query){
        Handle handle = getHandle();
        logger.info(query);
        return handle.createQuery(query).mapToMap().list();
    }

    default boolean createdModelExist(String modelName){
        Handle handle = getHandle();
        String totalQuery = "SELECT value FROM stringvalues WHERE value = \'" + modelName + "\';";
        List<String> models = handle.createQuery(totalQuery).mapTo(String.class).list();
        if(models.size()>0){
            return true;
        }
        else{
            return false;
        }
    }

    default String getRelOwner(String table){
        Handle handle = getHandle();
        List<String> relowners = handle.createQuery("SELECT relowner FROM pg_catalog.pg_class WHERE relkind = 'r' AND relowner in (SELECT relowner FROM pg_catalog.pg_class WHERE relname = 'dataset') AND relname = '"+ table +"';").mapTo(String.class).list();
        return relowners.get(0);
    }

    @RegisterRowMapper(DataSetMapper.class)
    @SqlQuery(//language=sql
            "SELECT * FROM dataset;")
    List<DataSet> getdatasets();

    @SqlQuery(//language=sql
            "SELECT name FROM columndefinitions WHERE datasetid IN (SELECT id FROM dataset WHERE name = :datasetName);")
    List<String> datasetColumNames(@Bind("datasetName") String datasetName);



    @SqlQuery(//language=sql
            "SELECT column_name FROM information_schema.columns WHERE table_name = :tableName;")
    List<String> tableColumNames(@Bind("tableName") String tableName);


//    @SqlQuery(//language=sql
//            "SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name = :tableName);")
//    List<Boolean> tableExists(@Bind("tableName") String tableName);

    /**
     * Export Arff data file.
     * @see "https://www.cs.waikato.ac.nz/ml/weka/arff.html"
     * @param query something that results in a set of DB rows
     * @param columnNames column headers
     * @param columnTypes one of "numeric", "string", "date"
     * @param file output target
     * @return the number of lines written.
     * @throws IOException
     */
    default int exportArff(String query, List<String> columnNames, List<String> columnTypes, String file) throws IOException {
        if(columnNames.size() != columnTypes.size()){
            throw new IllegalArgumentException("Column names and types should countain the same number of arguments. Names = "+columnNames+", Types = " + columnTypes);
        }

        final ImmutableSet<String> allowedTypes = ImmutableSet.of("numeric", "string", "date");

        try (BufferedWriter out = Files.newBufferedWriter(Paths.get(file))) {
            out.write("% Export from Centre for Innovation - RiskMonitoring\n");
            out.write("@RELATION exportdata\n\n");
            for (int i = 0; i < columnNames.size(); i++) {
                final String name = columnNames.get(i);
                final String type = columnTypes.get(i);

                if(!allowedTypes.contains(type)){
                    throw new IllegalArgumentException("Column '"+name+"' has invalid type '"+type+"'. Choose one of " + allowedTypes);
                }

                out.write("@ATTRIBUTE \"" + name + "\" " + type + "\n");
            }
            out.write("\n@DATA\n");
            return exportCsv(query, out, CSVFormat.DEFAULT);
        }
    }

    /**
     * Export Arff data file.
     * @see "https://www.cs.waikato.ac.nz/ml/weka/arff.html"
     * @param query something that results in a set of DB rows
     * @param file output target
     * @return the number of lines written.
     * @throws IOException
     */
    default int exportArff(String query, String file) throws IOException {

        String relation = "exportdata";
        final Matcher matcher = Pattern.compile("FROM|from\\s+([a-zA-Z0-9.\\-]+)").matcher(query);
        if(matcher.find()) {
            final String group = matcher.group(1);
            if(group != null)
                relation = group;
        }

        final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss");


        AtomicInteger count = new AtomicInteger();
        String finalRelation = relation;

        Map<Integer, List<String>> dataCols = new HashMap<>();
        List<String> dataRows = new ArrayList<>();
        List<String> colNames = new ArrayList<>();
        List<String> colTypes = new ArrayList<>();

        getHandle()
                .createQuery(query)
                .map(new MapMapper())
                .stream()
                .forEach(row -> {
                    try {
                        if (count.getAndIncrement() == 0) {
                            // Write header

                            final String[] columnNames = row.keySet().toArray(new String[0]);
                            final Object[] columnValues = row.values().toArray();


                            for (int i = 0; i < columnNames.length; i++) {
                                final String name = columnNames[i];
                                final Object value = columnValues[i];

                                // Infer ARFF type
                                String atype;
                                if (value instanceof java.util.Date) {
                                    // handles java.util.Date, java.sql.Date
                                    atype = "date";
                                } else if (value instanceof DateTime) {
                                    // jodatime
                                    atype = "date";
                                } else if (value instanceof Number) {
                                    // Integer, Long, Double, ...
                                    atype = "numeric";
                                } else {
                                    atype = "string";
                                }

                                dataCols.put(i, new ArrayList<>());
                                colNames.add(name);
                                colTypes.add(atype);
                            }
                        }
                        final List<Object> vals = new ArrayList<>(row.values());
                        for (int i = 0; i < vals.size(); i++) {
                            final Object val = vals.get(i);
                            String str;
                            final Object type = colTypes.get(i);

                            if (val == null) {
                                // null
                                str = "?";
                            } else if ("date".equals(type)) {
                                str = dateTimeFormatter.print((new DateTime(val)));
                            } else if ("numeric".equals(type)) {
                                double d = (Double) val;
                                str = d == (long) d ? String.format("%d", (long) d) : String.format("%s", d);
                            } else if ("string".equals(type)) {
                                str = val.toString();
                                str = StringUtils.replace(str, "\"", "\\\"");
                                str = StringUtils.replace(str, ",", "\\,");
                                str = "\"" + str + "\"";
                            } else {
                                // unknown type
                                str = "?";
                            }

                            dataCols.get(i).add(str);

                        }
                    } catch (Exception e){
                        System.err.println(e.getMessage());
                        System.err.println(row);
                    }

                });


        try (
                BufferedWriter out = Files.newBufferedWriter(Paths.get(file));
        ) {
            out.write("% Export from Centre for Innovation - RiskMonitoring\n");
            out.write("@RELATION \""+ finalRelation +"\"\n\n");

            // column types
            int nRows = dataCols.get(0).size();
            final int orgColCount = colTypes.size();
            for (int i = 0; i < orgColCount; i++) {
                String colType = colTypes.get(i);
                final String colName = colNames.get(i);
                // infer enum types

                if ("string".equals(colType)) {
                    AtomicInteger notNullCount = new AtomicInteger();

                    final Set<String> enumVals = new HashSet<>();
                    dataCols.get(i).stream().filter(s -> !"?".equals(s)).forEach( s -> {
                                notNullCount.getAndIncrement();
                                enumVals.add(s);
                            }
                    );
                    if(enumVals.size() < notNullCount.get() / 2){
                        // add enum
                        colType = enumVals.stream().collect(Collectors.joining(",", "{","}"));
                        colTypes.set(i, colType);
                        // colType = enumVals.stream().map(this::cleanEnumVal).collect(Collectors.joining(",", "{","}"));
                        //final List<String> enumValsCol = dataCols.get(i).stream().map(s -> "?".equals(s) ? "?" : cleanEnumVal(s)).collect(Collectors.toList());
                        //dataCols.put(colNames.size(), enumValsCol);
                        //colNames.add(colName+"Enum");
                        //out.write("@ATTRIBUTE \"" + colName + "Enum\" " + colType + "\n");
                    }
                }
                out.write("@ATTRIBUTE \"" + colName + "\" " + colType + "\n");

            }

            // write data
            out.write("\n@DATA\n");
            for (int i = 0; i < nRows; i++) {
                List<String> outRow = new ArrayList<>();
                for (List<String> vals : dataCols.values()) {
                    outRow.add(vals.get(i));
                }
                dataRows.add(outRow.stream().collect(Collectors.joining(",", "", "\n")));
            }
            for (String dataRow : dataRows) {
                out.write(dataRow);
            }
            out.flush();
        } catch (Exception e){
            e.printStackTrace();
            return -1;
        }
        return count.get();
    }

    Pattern nonAlphaNum = Pattern.compile("\\W");
    default String cleanEnumVal(String val){
        return nonAlphaNum.matcher(val).replaceAll("");
    }

    /**
     * Export Arff data file.
     * @see "https://www.cs.waikato.ac.nz/ml/weka/arff.html"
     * @param query something that results in a set of DB rows
     * @param file output target
     * @return the number of lines written.
     * @throws IOException
     */
    default int exportArff2(String query, String file) throws IOException {

        String relation = "exportdata";
        final Matcher matcher = Pattern.compile("FROM|from\\s+([a-zA-Z0-9.\\-]+)").matcher(query);
        if(matcher.find()) {
            final String group = matcher.group(1);
            if(group != null)
                relation = group;
        }

        final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss");

        try (
                BufferedWriter out = Files.newBufferedWriter(Paths.get(file));
        ) {
            AtomicInteger count = new AtomicInteger();
            String finalRelation = relation;
            final List<String> colTypes = new ArrayList<>();
            getHandle()
                    .createQuery(query)
                    .map(new MapMapper())
                    .stream()
                    .forEach(row -> {
                        if(row == null){
                            System.out.println("Queueue??");
                        }
                        if(count.getAndIncrement() == 0) {
                            try {
                                // Write header

                                final String[] columnNames = row.keySet().toArray(new String[0]);
                                final Object[] columnValues = row.values().toArray();

                                out.write("% Export from Centre for Innovation - RiskMonitoring\n");
                                out.write("@RELATION \""+ finalRelation +"\"\n\n");
                                for (int i = 0; i < columnNames.length; i++) {
                                    final String name  = columnNames[i];
                                    final Object value = columnValues[i];

                                    // Infer ARFF type
                                    String atype;
                                    if(value instanceof java.util.Date) {
                                        // handles java.util.Date, java.sql.Date
                                        atype = "date";
                                    } else if(value instanceof DateTime) {
                                        // jodatime
                                        atype = "date";
                                    } else if(value instanceof Number) {
                                        // Integer, Long, Double, ...
                                        atype = "numeric";
                                    } else {
                                        atype = "string";
                                    }
                                    colTypes.add(atype);
                                    out.write("@ATTRIBUTE \"" + name + "\" " + atype + "\n");
                                }
                                out.write("\n@DATA\n");
                            } catch (IOException e) {}
                        }
                        try {
                            final List<Object> vals = new ArrayList<>(row.values());
                            for (int i = 0; i < vals.size(); i++) {
                                final Object val = vals.get(i);
                                final Object type = colTypes.get(i);

                                if (val == null ){
                                    // null
                                    vals.set(i, "?");
                                } else if ("date".equals(type)){
                                    vals.set(i, dateTimeFormatter.print((new DateTime(val))));
                                } else if ("numeric".equals(type)){
                                    double d = (Double)val;
                                    vals.set(i, d == (long) d ? String.format("%d",(long)d) : String.format("%s",d));
                                } else if ("string".equals(type)){
                                    String str = val.toString();
                                    str = StringUtils.replace(str, "\"", "\\\"");
                                    str = StringUtils.replace(str, ",", "\\,");
                                    vals.set(i, "\"" + str + "\"");
                                } else {
                                    // unknown type
                                    vals.set(i, "?");
                                }
                            }

                            out.write(vals.stream().map(Object::toString).collect(Collectors.joining(",", "", "\n")));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
            out.flush();
            return count.get();

        } catch (Exception e){
            e.printStackTrace();
            return -1;
        }


    }

    default int exportCsv(String query, String file) throws IOException {
        final String extension = FilenameUtils.getExtension(file);
        CSVFormat format;
        switch (extension){
            case "tsv": format = CSVFormat.DEFAULT; break;
            case "txt": format = CSVFormat.EXCEL; break;
            default: format = CSVFormat.DEFAULT;
        }
        try (BufferedWriter out = Files.newBufferedWriter(Paths.get(file))) {
            return exportCsv(query, out, format);
        }
    }

    default int exportCsv(String query, String file, CSVFormat format) throws IOException {
        try (BufferedWriter out = Files.newBufferedWriter(Paths.get(file))) {
            return exportCsv(query, out, format);
        }
    }

    default int exportCsv(String query, Appendable out, CSVFormat format) throws IOException {
        try (CSVPrinter csvPrinter = new CSVPrinter(out, format)) {
            AtomicInteger count = new AtomicInteger();
            getHandle()
                    .createQuery(query)
                    .map(new MapMapper())
                    .stream()
                    .forEach(row -> {
                        //System.out.println("row = " + row);
                        // do header
                        try {
                            if (count.getAndIncrement() == 0) {
                                csvPrinter.printRecord(row.keySet());
                            }
                            csvPrinter.printRecord(row.values());
                        } catch (IOException ignored) {}
                    });
            csvPrinter.flush();

            return count.get();
        }
    }

    /**
     * Export Arff data file.
     * @see "https://www.cs.waikato.ac.nz/ml/weka/arff.html"
     * @param query something that results in a set of DB rows
     * @param file output target
     * @return the number of lines written.
     * @throws IOException
     */
    default Map<String,String> exportArffWithDateFeatureEngineering(String query, String file,
                                                                    List<String> selectedCols,
                                                                    boolean useDateAsString) {

        String relation = "exportdata";
        final Matcher matcher = Pattern.compile("FROM|from\\s+([a-zA-Z0-9.\\-]+)").matcher(query);
        if(matcher.find()) {
            final String group = matcher.group(1);
            if(group != null)
                relation = group;
        }

        AtomicInteger count = new AtomicInteger();
        Map<Integer, List<String>> dataCols = new HashMap<>();
        List<String> colNames = new ArrayList<>();
        List<String> colTypes = new ArrayList<>();
        AtomicInteger numOfDateCols = new AtomicInteger();

        readQuery(query, count, selectedCols, useDateAsString,
                dataCols, colNames, colTypes, numOfDateCols);

        String finalRelation = relation;
        List<String> columnsNotToConsider = Lists.newArrayList();
        List<String> dataRows = new ArrayList<>();
        Map<String, String> coltypeTracker = Maps.newLinkedHashMap();


        try (
                BufferedWriter out = Files.newBufferedWriter(Paths.get(file));
        ) {
            out.write("% Export from Centre for Innovation - RiskMonitoring\n");
            out.write("@RELATION \""+ finalRelation +"\"\n\n");

           List<Integer> enumCols = Lists.newArrayList();

            // column types
            int nRows = dataCols.get(0).size();
            final int orgColCount = colTypes.size();
            int offset = 0;
            int deleteTrack = 0;

            for (int i = 0; i < orgColCount; i++) {
                String colType = colTypes.get(i);
                final String colName = colNames.get(i);
                // infer enum types

                if ("string".equals(colType)) {
                    AtomicInteger notNullCount = new AtomicInteger();

                    final Set<String> enumVals = new HashSet<>();
                    dataCols.get(i + offset).stream().filter(s -> !"?".equals(s)).forEach( s -> {
                                notNullCount.getAndIncrement();
                                enumVals.add(s.replaceAll(" ", "_").replaceAll(":", "-"));
                            }
                    );
                    if(enumVals.size() < notNullCount.get() / 2){
                        // add enum
                        colType = enumVals.stream().collect(Collectors.joining(",", "{","}"));
                        colTypes.set(i, colType);
                        coltypeTracker.put(colName, "enum");
                        enumCols.add(i + offset - deleteTrack);
                    }
                    else{
                        columnsNotToConsider.add(colName);
                        dataCols.remove(i+offset);
                        deleteTrack++;
                    }
                }
                if ("numeric".equals(colType)) {
                    AtomicInteger notNullCount = new AtomicInteger();

                    final Set<String> enumVals = new HashSet<>();
                    dataCols.get(i + offset).stream().filter(s -> !"?".equals(s)).forEach( s -> {
                                notNullCount.getAndIncrement();
                                enumVals.add(s);
                            }
                    );
                    if(enumVals.size() < 3){
                        // add enum
                        colType = enumVals.stream().collect(Collectors.joining(",", "{","}"));
                        colTypes.set(i, colType);
                        coltypeTracker.put(colName, "enum");
                        enumCols.add(i);
                    }
                    else{
                        coltypeTracker.put(colName, "numeric");
                    }
                }
                if("date".equals(colType)){
                    writeDateProperties(out, colName);
                    offset += 8;
                    coltypeTracker.put(colName, "date");
                }
                if(!"date".equals(colType) && !"string".equals(colType)){
                    out.write("@ATTRIBUTE \"" + colName + "\" " + colType + "\n");
                }
            }

            // write data
            out.write("\n@DATA\n");
            for (int i = 0; i < nRows; i++) {
                List<String> outRow = new ArrayList<>();
                int j = 0;
                for (List<String> vals : dataCols.values()) {
                    if(enumCols.contains(j)){
                        outRow.add(vals.get(i).replaceAll(" ", "_").replaceAll(":", "-"));
                    }
                    else{
                        outRow.add(vals.get(i).replaceAll(":", "-"));
                    }
                    j++;
                }
                dataRows.add(outRow.stream().collect(Collectors.joining(",", "", "\n")));
            }
            for (String dataRow : dataRows) {
                out.write(dataRow);
            }
            out.flush();
        } catch (Exception e){
            e.printStackTrace();
            return coltypeTracker;
        }

        List<String> missing = Lists.newArrayList();
        for(String c : selectedCols){
            if(!coltypeTracker.containsKey(c)){
                missing.add(c);
            }
        }
        return coltypeTracker;
    }


    /**
     * Export Arff data file.
     * @see "https://www.cs.waikato.ac.nz/ml/weka/arff.html"
     * @param query something that results in a set of DB rows
     * @param file output target
     * @return the number of lines written.
     * @throws IOException
     */
    default Map<String,String> exportPredictionArffWithDateFeatureEngineering(String query, String file,
                                                                              List<String> selectedCols,
                                                                              List<String> columnTypes,
                                                                              int targetIndex,
                                                                              String targetName,
                                                                              String targetValues,
                                                                              boolean useDateAsString) throws IOException {

        String relation = "exportdata";
        final Matcher matcher = Pattern.compile("FROM|from\\s+([a-zA-Z0-9.\\-]+)").matcher(query);
        if(matcher.find()) {
            final String group = matcher.group(1);
            if(group != null)
                relation = group;
        }

        AtomicInteger count = new AtomicInteger();
        Map<Integer, List<String>> dataCols = new HashMap<>();
        List<String> colNames = new ArrayList<>();
        List<String> colTypes = new ArrayList<>();
        AtomicInteger numOfDateCols = new AtomicInteger();

        readQuery(query, count, selectedCols, useDateAsString,
                dataCols, colNames, colTypes, numOfDateCols);

        String finalRelation = relation;
        List<String> columnsNotToConsider = Lists.newArrayList();
        List<String> dataRows = new ArrayList<>();
        Map<String, String> coltypeTracker = Maps.newLinkedHashMap();


        try (
                BufferedWriter out = Files.newBufferedWriter(Paths.get(file));
        ){
            out.write("% Export from Centre for Innovation - RiskMonitoring\n");
            out.write("@RELATION \""+ finalRelation +"\"\n\n");

            List<Integer> enumCols = Lists.newArrayList();

            // column types
            int nRows = dataCols.get(0).size();
            final int colCount = selectedCols.contains(targetName) ? colTypes.size() : colTypes.size() + 1;

            int offset = 0;
            int insertCorrector = 0;

            for (int i = 0; i < colCount; i++) {
                insertCorrector = i <= targetIndex || selectedCols.contains(targetName) ? i : i - 1;

                if(i != targetIndex) {
                    String colType = colTypes.get(insertCorrector);
                    final String colName = colNames.get(insertCorrector);
                    final String colPreviousType = columnTypes.get(insertCorrector);
                    // infer enum types

                    if (colPreviousType.equals("enum")) {
                        final Set<String> enumVals = new HashSet<>();
                        dataCols.get(insertCorrector + offset).stream().filter(s -> !"?".equals(s)).forEach(s -> {
                                    enumVals.add(s.replaceAll(" ", "_").replaceAll(":", "-"));
                                }
                        );
                        // add enum
                        colType = enumVals.stream().collect(Collectors.joining(",", "{", "}"));
                        colTypes.set(insertCorrector, colType);
                        coltypeTracker.put(colName, "enum");
                        enumCols.add(insertCorrector + offset);
                    }
                    if (colPreviousType.equals("numeric")) {
                        colType = "numeric";
                        colTypes.set(insertCorrector, "numeric");
                    }
                    if (colPreviousType.equals("date")) {
                        writeDateProperties(out, colName);
                        offset += 8;
                        coltypeTracker.put(colName, "date");
                    }
                    if (!"date".equals(colType) && !"string".equals(colType)) {
                        out.write("@ATTRIBUTE \"" + colName + "\" " + colType + "\n");
                    }
                }
                else{
                    out.write("@ATTRIBUTE \"" + targetName + "\" " + targetValues.replaceAll(" ", "_") + "\n");
                }
            }

            // write data
            out.write("\n@DATA\n");
            for (int i = 0; i < nRows; i++) {
                List<String> outRow = new ArrayList<>();
                int columnNumber = selectedCols.contains(targetName) ? dataCols.keySet().size() : dataCols.keySet().size() + 1;

                for(int j = 0; j < columnNumber; j++){
                    if(!selectedCols.contains(targetName)) {
                        if (j < targetIndex) {
                            if (enumCols.contains(j)) {
                                outRow.add(dataCols.get(j).get(i).replaceAll(" ", "_").replaceAll(":", "-"));
                            } else {
                                outRow.add(dataCols.get(j).get(i).replaceAll(":", "-"));
                            }
                        } else if (j > targetIndex) {
                            if (enumCols.contains(j)) {
                                outRow.add(dataCols.get(j - 1).get(i).replaceAll(" ", "_").replaceAll(":", "-"));
                            } else {
                                outRow.add(dataCols.get(j - 1).get(i).replaceAll(":", "-"));
                            }
                        } else {
                            outRow.add("?");
                        }
                    }
                    else{
                        if (j != targetIndex + offset) {
                            if (enumCols.contains(j)) {
                                outRow.add(dataCols.get(j).get(i).replaceAll(" ", "_").replaceAll(":", "-"));
                            } else {
                                outRow.add(dataCols.get(j).get(i).replaceAll(":", "-"));
                            }
                        }
                        else {
                            outRow.add("?");
                        }
                    }
                }
                dataRows.add(outRow.stream().collect(Collectors.joining(",", "", "\n")));
            }
            for (String dataRow : dataRows) {
                out.write(dataRow);
            }
            out.flush();
        } catch (Exception e){
            e.printStackTrace();
            return coltypeTracker;
        }
        return coltypeTracker;
    }

    default void writeDateProperties(BufferedWriter out, String colName) throws IOException {
        out.write("@ATTRIBUTE \"" + colName + "_year" + "\" " + "numeric" + "\n");
        out.write("@ATTRIBUTE \"" + colName + "_dayOfMonth" + "\" " + "numeric" + "\n");
        out.write("@ATTRIBUTE \"" + colName + "_dayOfWeek" + "\" " + "numeric" + "\n");
        out.write("@ATTRIBUTE \"" + colName + "_dayOfYear" + "\" " + "numeric" + "\n");
        out.write("@ATTRIBUTE \"" + colName + "_hourOfDay" + "\" " + "numeric" + "\n");
        out.write("@ATTRIBUTE \"" + colName + "_minuteOfDay" + "\" " + "numeric" + "\n");
        out.write("@ATTRIBUTE \"" + colName + "_minuteOfHour" + "\" " + "numeric" + "\n");
        out.write("@ATTRIBUTE \"" + colName + "_monthOfYear" + "\" " + "numeric" + "\n");
        out.write("@ATTRIBUTE \"" + colName + "_weekOfYear" + "\" " + "numeric" + "\n");
    }

    default void readQuery(String query,
                           AtomicInteger count,
                           List<String> selectedCols,
                           boolean useDateAsString,
                           Map<Integer, List<String>> dataCols,
                           List<String> colNames,
                           List<String> colTypes,
                           AtomicInteger numOfDateCols) {
        getHandle()
                .createQuery(query)
                .map(new MapMapper())
                .stream()
                .forEach(row -> {
                    try {
                        if (count.getAndIncrement() == 0) {
                            // Write header

                            final String[] columnNames = row.keySet().toArray(new String[0]);
                            final Object[] columnValues = row.values().toArray();


                            for (int i = 0; i < columnNames.length; i++) {
                                final String name = columnNames[i];
                                final Object value = columnValues[i];
                                // Infer ARFF type
                                String atype;
                                if (value instanceof java.util.Date) {
                                    // handles java.util.Date, java.sql.Date
                                    atype = "date";
                                } else if (value instanceof DateTime) {
                                    // jodatime
                                    atype = "date";
                                } else if (value instanceof Number) {
                                    // Integer, Long, Double, ...
                                    atype = "numeric";
                                } else {
                                    atype = "string";
                                }

                                if (atype.equals("date") && !useDateAsString) {
                                    numOfDateCols.incrementAndGet();
                                }

                                atype = useDateAsString && atype.equals("date") ? "string" : atype;
                                colTypes.add(atype);

                                dataCols.put(i, new ArrayList<>());
                                colNames.add(name);

                            }
                            final int numberOfExtraCols = numOfDateCols.get() * 8;
                            for (int j = 0; j < numberOfExtraCols; j++) {
                                dataCols.put(j + columnNames.length, new ArrayList<>());
                            }

                        }
                        final List<Object> vals = new ArrayList<>(row.values());
                        int offset = 0;
                        for (int i = 0; i < vals.size(); i++) {
                            int j = i + offset;
                            final Object val = vals.get(i);
                            String str;
                            final Object type = colTypes.get(i);

                            if (val == null) {
                                // null
                                str = "?";
                            } else if ("date".equals(type)) {
                                str = "";
                                for (Integer timeAttr : DateTimeUtil.getDateTimeAttributes(new DateTime(val)).values()) {
                                    str = timeAttr == (long) timeAttr ? String.format("%d", (long) timeAttr) : String.format("%s", timeAttr);
                                    dataCols.get(j + offset).add(str);
                                    offset++;
                                }
                                offset = offset - 1;
                            } else if ("numeric".equals(type)) {
                                double d = (Double) val;
                                str = d == (long) d ? String.format("%d", (long) d) : String.format("%s", d);
                            } else if ("string".equals(type)) {
                                str = val.toString();
                                str = StringUtils.replace(str, "\"", "\\\"");
                                str = StringUtils.replace(str, ",", "\\,");
                                str = "\"" + str + "\"";
                            } else {
                                // unknown type
                                str = "?";
                            }

                            if (!type.equals("date")) dataCols.get(j).add(str);

                        }
                    } catch (Exception e) {
                        System.err.println(e.getMessage());
                        System.err.println(row);
                    }
                });
    }


}
