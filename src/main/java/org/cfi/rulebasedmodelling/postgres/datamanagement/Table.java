package org.cfi.rulebasedmodelling.postgres.datamanagement;

import com.google.common.collect.Lists;

import java.util.List;

public class Table {

    private String tableName;
    private List<Long> columns;

    Table(String tableName,  List<Long> columns){
        this.tableName = tableName;
        this.columns = columns;
    }

    public List<Long> getColumns(){
        return this.columns;
    }

    public String generateQuery(){
        String initial = initialString();
        String front = frontAddition();
        String end = backAddition();
        StringBuilder query = new StringBuilder();
        query.append(front);
        query.append(initial);
        query.append(end);
        return query.toString();
    }

    private List<Long> extractColumnNumbers(List<Long> colIds){
        List<Long> columnNumbers = Lists.newArrayList();
        for(Long colId  : colIds.subList(2,colIds.size())){
            columnNumbers.add(colId);
        }
        return columnNumbers;
    }

    protected String initialString(){
        if(this.columns.size() > 1) {
            String firstCol = Long.toString(this.columns.get(0));
            String secondCol = Long.toString(this.columns.get(1));
            return "(SELECT c1.rowid, c1.value AS col" + firstCol + ", c2.value AS col" + secondCol + " FROM " + this.tableName + " c1 JOIN " +
                    this.tableName + " c2 ON c1.rowid = c2.rowid WHERE c1.columnid = " + firstCol + " AND c2.columnid = " + secondCol + ")";
        }
        else if(this.columns.size() == 1){
            String firstCol = Long.toString(this.columns.get(0));
            return "SELECT rowid, value AS col" + firstCol + " FROM " + this.tableName + " WHERE columnid = " + firstCol;
        }
        else
            return "SELECT * FROM " + this.tableName;
    }

    protected String frontAddition(){
        if(this.columns.size() > 1) {
            List<Long> colNums = extractColumnNumbers(this.columns);
            String firstCol = "col" + Long.toString(this.columns.get(0));
            String secondCol = "col" + Long.toString(this.columns.get(1));
            StringBuilder selection = new StringBuilder(firstCol + ", " + secondCol);
            StringBuilder sb = new StringBuilder();
            int iter = 3;
            for (long colNum : colNums) {
                StringBuilder temp = new StringBuilder();
                if (iter - 2 < colNums.size()) {
                    temp.append("(");
                }
                temp.append("SELECT c");
                temp.append(iter);
                temp.append(".rowid, ");
                temp.append(selection.toString());
                temp.append(", value AS col");
                temp.append(colNum);
                selection.append(", col");
                selection.append(colNum);
                temp.append(" FROM ");
                sb.insert(0,temp);
                iter++;
            }
            return sb.toString();
        }
        else
            return "";
    }

    protected String backAddition(){
        if(this.columns.size() > 1) {
            List<Long> colNums = extractColumnNumbers(this.columns);
            StringBuilder sb = new StringBuilder();
            int iter = 3;
            for (long colNum : colNums) {
                String ctab = " c" + iter;
                String tab = " t" + (iter - 2);
                sb.append(tab);
                sb.append(" JOIN ");
                sb.append(this.tableName);
                sb.append(ctab);
                sb.append(" ON");
                sb.append(tab);
                sb.append(".rowid =");
                sb.append(ctab);
                sb.append(".rowid where ");
                sb.append(ctab);
                sb.append(".columnid = ");
                sb.append(colNum);
                sb.append(" ORDER BY ");
                sb.append(ctab);
                sb.append(".rowid");
                if (iter - 2 < colNums.size()) {
                    sb.append(")");
                }
                iter++;
            }
            return sb.toString();
        }
        else
            return "";
    }
}
