package org.cfi.rulebasedmodelling.postgres.datamanagement;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;
import org.joda.time.DateTime;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DateTimeValuesMapper implements RowMapper<DateTimeValues> {

    @Override
    public DateTimeValues map(ResultSet rs, StatementContext ctx) throws SQLException {
        return new DateTimeValues()
                .setRow(rs.getLong("rowid"))
                .setColumnid(rs.getLong("columnid"))
                .setValue(new DateTime(rs.getTimestamp("value")));
    }
}
