package org.cfi.rulebasedmodelling.postgres.datamanagement;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ColumnDefintionsMapper implements RowMapper<ColumnDefinitions>{

    @Override
    public ColumnDefinitions map(ResultSet rs, StatementContext ctx) throws SQLException {
        return new ColumnDefinitions()
                .setColumnid(rs.getLong("columnid"))
                .setName(rs.getString("name"))
                .setDescription(rs.getString("description")==null ? "" : rs.getString("description"))
                .setType(rs.getString("type") == null ? "" : rs.getString("type"))
                .setDatasetid(rs.getLong("datasetid"));
    }
}
