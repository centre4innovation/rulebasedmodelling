package org.cfi.rulebasedmodelling.postgres.datamanagement;

import java.util.List;

public class TableType {

    public TableType(){}

    public static class DoubleTable extends Table{
        public DoubleTable(List<Long> columns){
            super("doublevalues", columns);
        }
    }

    public static class StringTable extends Table{
        public StringTable(List<Long> columns){
            super("stringvalues", columns);
        }
    }

    public static class DateTimeTable extends Table{
        public DateTimeTable(List<Long> columns){
            super("datetimevalues", columns);
        }
    }
}
