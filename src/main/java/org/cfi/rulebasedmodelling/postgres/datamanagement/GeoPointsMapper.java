package org.cfi.rulebasedmodelling.postgres.datamanagement;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GeoPointsMapper implements RowMapper<GeoPoints> {

    @Override
    public GeoPoints map(ResultSet rs, StatementContext ctx) throws SQLException {
        return new GeoPoints()
                .setRow(rs.getLong("row"))
                .setColumnid(rs.getLong("columnid"))
                .setLatitude(rs.getDouble("latitude"))
                .setLongitude(rs.getLong("longitude"));
    }
}
