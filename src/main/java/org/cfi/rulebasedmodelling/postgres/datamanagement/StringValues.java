package org.cfi.rulebasedmodelling.postgres.datamanagement;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StringValues {

    @JsonProperty
    public long rowid;                   // 1
    @JsonProperty
    public long columnid;              // 1
    @JsonProperty
    public String value;


    public StringValues(){}

    public long getRowid() {
        return rowid;
    }

    public StringValues setRowid(long row) {
        this.rowid = row;
        return this;
    }

    public long getColumnid() {
        return columnid;
    }

    public StringValues setColumnid(long columnid) {
        this.columnid = columnid;
        return this;
    }

    public String getValue() {
        return value;
    }

    public StringValues setValue(String value) {
        this.value = value;
        return this;
    }
}
