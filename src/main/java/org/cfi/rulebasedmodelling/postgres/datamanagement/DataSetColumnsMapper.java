package org.cfi.rulebasedmodelling.postgres.datamanagement;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DataSetColumnsMapper implements RowMapper<DataSetColumns> {

    @Override
    public DataSetColumns map(ResultSet rs, StatementContext ctx) throws SQLException {
        return new DataSetColumns()
                .setDatasetid(rs.getLong("datasetid"))
                .setColumnid(rs.getLong("columnid"));
    }
}
