package org.cfi.rulebasedmodelling.postgres.datamanagement;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DoubleValues {

    @JsonProperty
    public long rowid;                   // 1
    @JsonProperty
    public long columnid;              // 1
    @JsonProperty
    public double value;


    public DoubleValues(){}

    public long getRowid() {
        return rowid;
    }

    public DoubleValues setRowid(long row) {
        this.rowid = row;
        return this;
    }

    public long getColumnid() {
        return columnid;
    }

    public DoubleValues setColumnid(long columnid) {
        this.columnid = columnid;
        return this;
    }

    public double getValue() {
        return value;
    }

    public DoubleValues setValue(double dvalue) {
        this.value = dvalue;
        return this;
    }

}
