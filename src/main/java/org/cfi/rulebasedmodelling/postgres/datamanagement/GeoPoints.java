package org.cfi.rulebasedmodelling.postgres.datamanagement;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GeoPoints {

    @JsonProperty
    public long rowid;                   // 1
    @JsonProperty
    public long columnid;              // 1
    @JsonProperty
    public double latitude;
    @JsonProperty
    public double longitude;


    public GeoPoints(){}

    public long getRow() {
        return rowid;
    }

    public GeoPoints setRow(long row) {
        this.rowid = row;
        return this;
    }

    public long getColumnid() {
        return columnid;
    }

    public GeoPoints setColumnid(long columnid) {
        this.columnid = columnid;
        return this;
    }

    public double getLatitude() {
        return latitude;
    }

    public GeoPoints setLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }

    public double getLongitude() {
        return longitude;
    }

    public GeoPoints setLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }
}
