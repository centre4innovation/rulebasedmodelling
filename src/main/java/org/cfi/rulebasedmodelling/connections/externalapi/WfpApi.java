package org.cfi.rulebasedmodelling.connections.externalapi;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.cfi.rulebasedmodelling.api.Config;

import java.net.URI;
import java.util.Map;

public class WfpApi {

    Config config;
    HttpClient httpClient;
    ObjectMapper objectMapper;

    public WfpApi(Config config) {
        this.config = config;
        this.httpClient = HttpClientBuilder.create().build();
        this.objectMapper = new ObjectMapper();
    }

    public void retrieveData(String table, Map<String,String> filters, boolean external){
        try
        {
            StringBuilder keyValues = new StringBuilder();
            filters.entrySet().stream().forEach(kv ->{keyValues.append(kv.getKey()+"="+kv.getValue()+"&");});
            keyValues.deleteCharAt(keyValues.length());

            String endpoint = external ? config.wfpApiConfig.externalendpoint : config.wfpApiConfig.datavizendpoint;
//            "commodities"
//            "coping-strategies-indexes"
//            "economic-data"
//            "economic-indicator-properties"
//            "economic-indicators"
//            "food-consumption-scores"
//            "grf-admin-units"
//            "indicator-types"
//            "markets"
//            "methodologies"
//            "monthly-price-items"
//            "ndvi-avg-values"
//            "ndvi-values"
//            "price-types"
//            "rainfall-avg-values"
//            "rainfall-values"
//            "target-groups"
//            "units-of-measure"
//            "pbl-stats-sum"
//            "pbl-stats-sum-4-maps"
//            "stats-sum-l3-no-ven"

            URIBuilder builder = new URIBuilder(endpoint + table + "/" + keyValues.toString());

//            "https://vam-alibaba.external-api.org/v1/grf-admin-units/?iso3=AFG&adm_id=410

            // Request parameters. All of them are optional.
//            builder.setParameter("returnFaceId", "true");
//            builder.setParameter("returnFaceLandmarks", "false");
//            builder.setParameter("returnFaceAttributes", faceAttributes);

            // Prepare the URI for the REST API call.
            URI uri = builder.build();
            HttpGet request = new HttpGet(uri);

            // Request headers.
            request.setHeader("Content-Type", "application/json");
            request.setHeader("x-api-key", config.wfpApiConfig.key);

            // Request body.
//            StringEntity reqEntity = new StringEntity( "{\"url\":\""+imgUrl+"\"}");
//            request.setEntity(reqEntity);

            // Execute the REST API call and get the response entity.
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();

            if (entity != null)
            {
                // Format and display the JSON response.
                System.out.println("REST Response:\n");

                String jsonString = EntityUtils.toString(entity).trim();

                JsonNode node = objectMapper.readTree(jsonString);

            }

        }
        catch (Exception e)
        {
            // Display error message.
            System.out.println(e.getMessage());
        }
    }
}
