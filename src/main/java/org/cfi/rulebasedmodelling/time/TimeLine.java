package org.cfi.rulebasedmodelling.time;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.cfi.rulebasedmodelling.util.StatReduce;
import org.joda.time.DateTime;
import org.joda.time.Period;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.DoubleBinaryOperator;
import java.util.function.Predicate;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;

/**
 * A sequence of events, that can be sampled and analyzed.
 * @version 30-1-17
 * @author Arvid Halma
 */
@SuppressWarnings({"WeakerAccess", "unused"})
@JsonSerialize(using = TimeLineSerializer.class)
public class TimeLine<E extends TimeStamped> implements Iterable<E>, Collection<E>{
        private NavigableMap<DateTime, List<E>> events;

    public TimeLine() {
        this.events = new TreeMap<>();
    }

    public TimeLine(Collection<E> events) {
        this();
        for (E event : events) {
            add(event);
        }
    }

    protected TimeLine(NavigableMap<DateTime, List<E>> dateTimeESortedMap) {
        this.events = dateTimeESortedMap;
    }

    protected TimeLine(Map<DateTime, List<E>> dateTimeESortedMap) {
        this.events = new TreeMap<>(dateTimeESortedMap);
    }

    public List<E> get(DateTime t){
        return events.get(t);
    }

    public List<E> getFloor(DateTime t){
        Map.Entry<DateTime, List<E>> entry = events.floorEntry(t);
        return entry == null ? null : entry.getValue();
    }

    public List<E> getClosest(DateTime t){
        Map.Entry<DateTime, List<E>> floor = events.floorEntry(t);
        Map.Entry<DateTime, List<E>> ceil = events.ceilingEntry(t);
        if(floor == null){
            return ceil == null ? null : ceil.getValue();
        } else if (ceil == null){
            return floor.getValue();
        } else {
            long millis = t.getMillis();
            if(millis - floor.getKey().getMillis() < ceil.getKey().getMillis() - millis){
                return floor.getValue();
            } else {
                return ceil.getValue();
            }
        }
    }

    public double getValue(DateTime t, ToDoubleFunction<E> toDoubleFunction, DoubleBinaryOperator reduceOp, double defaultValue){
        return events.get(t).stream().mapToDouble(toDoubleFunction).reduce(reduceOp).orElse(defaultValue);
    }

    public double sampleValue(DateTime t, ToDoubleFunction<E> toDoubleFunction){
        return sampleValue(t, toDoubleFunction, StatReduce.MEAN);
    }

    public double sampleValue(DateTime t, ToDoubleFunction<E> toDoubleFunction, StatReduce reduce){
        Map.Entry<DateTime, List<E>> e0 = events.floorEntry(t);
        Map.Entry<DateTime, List<E>> e1 = events.ceilingEntry(t);

        if(e0 == null){
            // before first event
            return e1.getValue() == null ? 0 : reduce.apply(e1.getValue().stream().mapToDouble(toDoubleFunction));
        } else if (e1 == null){
            // after last event
            return e0.getValue() == null ? 0 : reduce.apply(e0.getValue().stream().mapToDouble(toDoubleFunction));
        } else {
            double y0 = reduce.apply(e0.getValue().stream().mapToDouble(toDoubleFunction));
            double y1 = reduce.apply(e1.getValue().stream().mapToDouble(toDoubleFunction));
            long tStart = e0.getKey().getMillis();
            double dt = e1.getKey().getMillis() - tStart;
            double w = t.getMillis() - tStart;
            return dt == 0.0 ?  0.5*(y0+y1) : y0 + (w/dt) * (y1-y0);
        }
    }

    public double sampleValue(DateTime t, ToDoubleFunction<E> toDoubleFunction, StatReduce reduce, Period maxDt, double elseValue){
        Map.Entry<DateTime, List<E>> e0 = events.floorEntry(t);
        Map.Entry<DateTime, List<E>> e1 = events.ceilingEntry(t);

        long maxDtMillis = maxDt.toStandardDuration().getMillis();
        if(e0 == null){
            // before first event
            if (e1.getValue() == null) {
                return elseValue;
            } else {
                long dt = e1.getKey().getMillis() - t.getMillis();
                return maxDtMillis < dt ? elseValue : reduce.apply(e1.getValue().stream().mapToDouble(toDoubleFunction));
            }
        } else if (e1 == null){
            // after last event
            if (e0.getValue() == null) {
                return elseValue;
            } else {
                long dt = t.getMillis() - e0.getKey().getMillis();
                return maxDtMillis < dt ? elseValue : reduce.apply(e0.getValue().stream().mapToDouble(toDoubleFunction));
            }
        } else {
            double y0 = reduce.apply(e0.getValue().stream().mapToDouble(toDoubleFunction));
            double y1 = reduce.apply(e1.getValue().stream().mapToDouble(toDoubleFunction));
            long tStart = e0.getKey().getMillis();
            long tEnd = e1.getKey().getMillis();
            double dt = tEnd - tStart;

            long tMillis = t.getMillis();
            double w = tMillis - tStart;
            if(w > maxDtMillis && tEnd - w > maxDtMillis){
                return elseValue;
            }
            return dt == 0.0 ?  0.5*(y0+y1) : y0 + (w/dt) * (y1-y0);
        }
    }

    public List<E> getStart(){
        return this.events.firstEntry().getValue();
    }

    public List<E> getEnd(){
        return this.events.lastEntry().getValue();
    }

    public DateTime getStartDateTime(){
        return this.events.firstKey();
    }

    public DateTime getEndDateTime(){
        return this.events.lastKey();
    }

    public synchronized boolean add(E event){

        DateTime t = event.getTimeStamp();
        if(!events.containsKey(t)){
            events.put(t, new ArrayList<>(1));
        }
        events.get(t).add(event);
        return true;
    }

    public TimeLine<E> select(DateTime t0, DateTime t1){
        return new TimeLine<>(events.subMap(t0, t1));
    }

    public double reduce(StatReduce reduce, ToDoubleFunction<E> toDouble){
        return reduce.apply(asDoubleStream(toDouble));
    }

    public TimeLine<E> filter(Predicate<E> p){
        TimeLine<E> result = new TimeLine<>();
        events.values().parallelStream().flatMap(Collection::stream).filter(p).forEach(result::add);
        return result;
    }

    public int count(){
        return events.values().parallelStream().mapToInt(List::size).sum();
    }

    public int count(Predicate<E> p){
        return (int) events.values().parallelStream().flatMap(Collection::parallelStream).filter(p).count();
    }

    public double sum(ToDoubleFunction<E> toDouble){
        return (int) asDoubleStream(toDouble).sum();
    }

    public TimeLine<TimeValue> interpolate(Period step, ToDoubleFunction<E> toDouble, StatReduce reduce){
        TimeLine<TimeValue> result = new TimeLine<>();
        if(events.isEmpty()){
            return result;
        }
        DateTime tEnd = getEndDateTime();
        for (DateTime t = getStartDateTime(); t.isBefore(tEnd); t = t.plus(step)){
            double value = sampleValue(t, toDouble, reduce);
            result.add(new TimeValue(t, value));
        }
        return result;
    }

    public TimeLine<TimeValue> interpolate(Period step, ToDoubleFunction<E> toDouble, StatReduce reduce, Period maxDt, double elseValue){
        TimeLine<TimeValue> result = new TimeLine<>();
        if(events.isEmpty()){
            return result;
        }
        DateTime tEnd = getEndDateTime();
        for (DateTime t = getStartDateTime(); t.isBefore(tEnd); t = t.plus(step)){
            double value = sampleValue(t, toDouble, reduce, maxDt, elseValue);
            result.add(new TimeValue(t, value));
        }
        return result;
    }

    /**
     * Group values, counting the entries.
     * @param step time delta
     * @return counts per time step
     */
    public TimeLine<TimeValue> resample(Period step){
        return resample(step, e -> 1.0, StatReduce.COUNT);
    }

    /**
     * Group by time step
     * @param step time delta
     * @param toDouble How to select a value from an element
     * @param reduce how the values in the group are combined into a single value
     * @return combined values (e.g. sum, count) per time bracket
     */
    public TimeLine<TimeValue> resample(Period step, ToDoubleFunction<E> toDouble, StatReduce reduce){
        TimeLine<TimeValue> result = new TimeLine<>();
        if(events.isEmpty()){
            return result;
        }
        DateTime tEnd = getEndDateTime();
        for (DateTime t = getStartDateTime(); t.isBefore(tEnd); t = t.plus(step)){
            double value = reduce.apply(toDouble(t, t.plus(step), toDouble));
            result.add(new TimeValue(t, value));
        }
        return result;
    }

    public TimeLine<TimeValue> countStats(Period step){
        return resample(step, e -> 1.0, StatReduce.COUNT);
    }

    public TimeLine<TimeValue> hourStats(){
        return resample(Period.hours(1), d -> 1, StatReduce.COUNT);
    }

    public TimeLine<TimeValue> hourStats(ToDoubleFunction<E> toDouble, StatReduce reduce){
        return resample(Period.hours(1), toDouble, reduce);
    }

    public TimeLine<TimeValue> dayStats(){
        return resample(Period.days(1), d -> 1, StatReduce.COUNT);
    }

    public TimeLine<TimeValue> dayStats(ToDoubleFunction<E> toDouble, StatReduce reduce){
        return resample(Period.days(1), toDouble, reduce);
    }

    public TimeLine<TimeValue> weekStats(){
        return resample(Period.weeks(1), d -> 1, StatReduce.COUNT);
    }

    public TimeLine<TimeValue> weekStats(ToDoubleFunction<E> toDouble, StatReduce reduce){
        return resample(Period.weeks(1), toDouble, reduce);
    }

    public TimeLine<TimeValue> monthStats(){
        return resample(Period.months(1), d -> 1, StatReduce.COUNT);
    }

    public TimeLine<TimeValue> monthStats(ToDoubleFunction<E> toDouble, StatReduce reduce){
        return resample(Period.weeks(1), toDouble, reduce);
    }

    public TimeLine<TimeValue> yearStats(){
        return resample(Period.years(1), d -> 1, StatReduce.COUNT);
    }

    public TimeLine<TimeValue> yearStats(ToDoubleFunction<E> toDouble, StatReduce reduce){
        return resample(Period.years(1), toDouble, reduce);
    }

    public List<Double> weekDayStats(ToDoubleFunction<E> toDouble, StatReduce reduce){
        ArrayList<ArrayList<Double>> groups = new ArrayList<>(7);
        for (int i = 0; i < 7; i++) {
            groups.add(new ArrayList<>());
        }
        Period step = Period.days(1);
        DateTime tEnd = getEndDateTime();
        for (DateTime t = getStartDateTime(); t.isBefore(tEnd); t = t.plus(step)){
            double value = sampleValue(t, toDouble, reduce);
            groups.get(t.getDayOfWeek()).add(value);
        }

        return groups.stream().map(xs-> reduce.apply(xs.stream().mapToDouble(d->d))).collect(Collectors.toList());
    }

    public List<Double> weekDayStats(ToDoubleFunction<E> toDouble, double z, DoubleBinaryOperator reduce){
        ArrayList<Double> groups = new ArrayList<>(7);
        for (int i = 0; i < 7; i++) {
            groups.add(z);
        }
        StatReduce statReduce = StatReduce.asStatReduce(z, reduce);
        Period step = Period.days(1);
        DateTime tEnd = getEndDateTime();
        for (DateTime t = getStartDateTime(); t.isBefore(tEnd); t = t.plus(step)){
            int i = t.getDayOfWeek();
            double value = sampleValue(t, toDouble, statReduce);
            groups.set(i, reduce.applyAsDouble(groups.get(i), value));
        }

        return groups;
    }


    public TimeLine<TimeValue> slidingWindow(Period window, Period step, ToDoubleFunction<E> toDouble, StatReduce reduce){
        TimeLine<TimeValue> result = new TimeLine<>();
        if(events.isEmpty()){
            return result;
        }

        DateTime tEnd = getEndDateTime().minus(window);
        for (DateTime t = getStartDateTime(); t.isBefore(tEnd); t = t.plus(step)){

            DateTime tw = t.plus(window);
            double value = reduce.apply(toDouble(t, tw, toDouble));
            result.add(new TimeValue(tw, value));
        }

        return result;
    }

    public TimeLine<TimeValue> slidingWindow(Period window1, Period window2, Period step, ToDoubleFunction<E> toDouble, StatReduce reduce1, StatReduce reduce2, BinaryOperator<Double> windowValueCombiner){
        TimeLine<TimeValue> result = new TimeLine<>();
        if(events.isEmpty()){
            return result;
        }

        DateTime tEnd = getEndDateTime().minus(window1).minus(window2);
        for (DateTime t = getStartDateTime(); t.isBefore(tEnd); t = t.plus(step)){

            DateTime tw1 = t.plus(window1);
            DateTime tw2 = tw1.plus(window2);
            double value1 = reduce1.apply(toDouble(t, tw1, toDouble));
            double value2 = reduce2.apply(toDouble(tw1, tw2, toDouble));
            result.add(new TimeValue(tw1, windowValueCombiner.apply(value1, value2)));
        }

        return result;
    }

    public TimeLine<TimeValue> derivative(ToDoubleFunction<E> toDouble, Period dt){
        return interpolate(dt, toDouble, StatReduce.SUM).slidingWindow(dt, dt, dt, TimeValue::getValue, StatReduce.SUM, StatReduce.SUM, (ti, tj) -> tj - ti);
    }

    private DoubleStream toDouble(DateTime t0, DateTime t1, ToDoubleFunction<E> toDouble) {
        return events.subMap(t0, t1).values().parallelStream().flatMapToDouble(list -> list.parallelStream().mapToDouble(toDouble));
    }

    @Override
    public Iterator<E> iterator() {
        return events.values().stream().flatMap(Collection::stream).iterator();
    }

    public List<E> asList(){
        return events.values().parallelStream().flatMap(Collection::parallelStream).collect(Collectors.toList());
    }

    @Override
    public int size() {
        return count();
    }

    @Override
    public boolean isEmpty() {
        return events.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return events.values().parallelStream().flatMap(Collection::parallelStream).anyMatch(e -> e.equals(o));
    }

    @Override
    public Object[] toArray() {
        return asList().toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        int i = 0;
        for (E e : this) {
            a[i++] = (T)e;
        }
        return a;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object o : c) {
            if(!contains(o)){
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        for (E e : c) {
            add(e);
        }
        return true;
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException("Can't remove objects from a timeLine");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("Can't remove objects from a timeLine");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("Can't remove objects from a timeLine");
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("Can't remove objects from a timeLine");
    }

    public DoubleStream asDoubleStream(ToDoubleFunction<E> toDouble) {
        return events.values().parallelStream().flatMapToDouble((es) -> es.stream().mapToDouble(toDouble));
    }

    public void print(){
        System.out.println("Timeline");
        for (Map.Entry<DateTime, List<E>> entry : events.entrySet()) {
            System.out.println(" * " + entry.getKey() + " : \t" + entry.getValue());
        }
    }

    @Override
    public String toString() {
        return "TimeLine{" + "events=" + events +
                '}';
    }
}
