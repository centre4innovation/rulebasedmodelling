package org.cfi.rulebasedmodelling.api;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.google.common.collect.Lists;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.forms.MultiPartBundle;
import io.dropwizard.jdbi3.JdbiFactory;
import io.dropwizard.jdbi3.bundles.JdbiExceptionsBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import org.cfi.rulebasedmodelling.api.resourses.*;
import org.cfi.rulebasedmodelling.postgres.DataRead;
import org.cfi.rulebasedmodelling.postgres.dao.*;
import org.cfi.rulebasedmodelling.postgres.datamanagement.*;
import org.jdbi.v3.core.Jdbi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class App extends Application<Config> {
    private final Logger logger = LoggerFactory.getLogger(App.class);

    private Config config;
    private MonitorResource monitorResource;


    public static void main(String[] args) throws Exception {
        new App().run(args);
    }

    @Override
    public String getName() {
        return "riskmonitor";
    }

    public Config getConfig() {
        return config;
    }

    @Override
    public void initialize(Bootstrap<Config> bootstrap) {

        // Serve static content
        bootstrap.addBundle(new AssetsBundle("/assets", "/", "index.html"));

        // Swagger API docs
        bootstrap.addBundle(new SwaggerBundle<Config>() {
            @Override
            protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(Config configuration) {
                return configuration.swaggerBundleConfiguration;
            }
        });

        // Add multipart message support
        bootstrap.addBundle(new MultiPartBundle());

        // Views
        bootstrap.addBundle(new ViewBundle<>());

        // By adding the DBIExceptionsBundle to your application, Dropwizard will automatically
        // unwrap any thrown SQLException or DBIException instances.
        // This is critical for debugging, since otherwise only the common wrapper exception's
        // stack trace is logged.
        bootstrap.addBundle(new JdbiExceptionsBundle());

        // Add db migration support
        bootstrap.addBundle(new MigrationsBundle<Config>() {

            @Override
            public String getMigrationsFileName() {
                return "migrations.yml";
            }

            @Override
            public DataSourceFactory getDataSourceFactory(Config configuration) {
                return configuration.getDataSourceFactory();
            }
        });
    }



    @Override
    public void run(Config config, Environment environment) throws IOException {
        this.config = config;

        // Set JSON formatting
        if(config.prettyPrintJsonResponse) {
            environment.getObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
        }

        // Proper DateTime serialization
        environment.getObjectMapper().registerModule(new JodaModule());
        environment.getObjectMapper().configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        environment.getObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        // DB
        final JdbiFactory factory = new JdbiFactory();
        final Jdbi jdbi = factory.build(environment, config.getDataSourceFactory(), "database");
        jdbi.registerRowMapper(DataSet.class, new DataSetMapper());
        jdbi.registerRowMapper(DataSetColumns.class, new DataSetColumnsMapper());
        jdbi.registerRowMapper(ColumnDefinitions.class, new ColumnDefintionsMapper());
        jdbi.registerRowMapper(DoubleValues.class, new DoubleValuesMapper());
        jdbi.registerRowMapper(StringValues.class, new StringValuesMapper());
        jdbi.registerRowMapper(DateTimeValues.class, new DateTimeValuesMapper());
        jdbi.registerRowMapper(GeoPoints.class, new GeoPointsMapper());
        jdbi.registerRowMapper(TextDoc.class, new TextDocMapper());
        jdbi.registerRowMapper(JsonDoc.class, new JsonDocMapper());

        // Data Access Object interfaces
        config.dao = new Dao(
                jdbi.onDemand(LoadData.class),
                jdbi.onDemand(ReadData.class),
                jdbi.onDemand(TextDocDao.class),
                jdbi.onDemand(JsonDocDao.class)
        );

        // Resources
        monitorResource = new MonitorResource(config, environment.getObjectMapper());
        environment.jersey().register(monitorResource);

        DocumentResource documentResource = new DocumentResource(config);
        environment.jersey().register(documentResource);

        DataCombinationResource dataCombinationResource = new DataCombinationResource(config, environment.getObjectMapper());
        environment.jersey().register(dataCombinationResource);

        VisualiseResource visualiseResource = new VisualiseResource(config, environment.getObjectMapper());
        environment.jersey().register(visualiseResource);

        final FrontEndResource frontEndResource = new FrontEndResource();
        environment.jersey().register(frontEndResource);


        // Health checks
        final Health healthCheck = new Health(config);
        environment.healthChecks().register("dataDir", healthCheck);

        try{

            new DataRead(config).constructAndStorePermanentTables();

        }
        catch(Exception e){
            System.out.println("Connection Failed. Error... " + e);
        }

        logger.warn("Lookout application ready...");
    }
}
