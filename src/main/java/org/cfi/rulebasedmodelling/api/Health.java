package org.cfi.rulebasedmodelling.api;

import com.codahale.metrics.health.HealthCheck;

public class Health extends HealthCheck {
    private Config configuration;

    public Health(Config configuration) {
        this.configuration = configuration;
    }

    @Override
    protected Result check() throws Exception {
        return Result.healthy();
    }
}
