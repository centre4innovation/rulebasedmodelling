package org.cfi.rulebasedmodelling.api.resourses;

import io.swagger.annotations.Api;
import org.cfi.rulebasedmodelling.api.view.PageView;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;


@Path("/ui")
@Api("/ui")
@Consumes({MediaType.APPLICATION_JSON})
@Produces(MediaType.TEXT_HTML)
public class FrontEndResource {

        @GET
        @Path("/")
        public PageView base(){
            throw redirect("/api/v1/ui/dashboard");
        }

        @GET
        @Path("dashboard")
        public PageView dashboard(){
            return new PageView("dashboard.ftl");
        }

        @GET
        @Path("combinationRules")
        public PageView combine(){
            return new PageView("combinationRules.ftl");
        }

        @GET
        @Path("datacombine")
        public PageView datacombine(){
            return new PageView("datacombine.ftl");
        }

        @GET
        @Path("loaddata")
        public PageView loaddata(){
            return new PageView("loaddata.ftl");
        }

        @GET
        @Path("viewData")
        public PageView viewdata(){
            return new PageView("viewData.ftl");
        }

        @GET
        @Path("visualise")
        public PageView visualise(){
        return new PageView("visualise.ftl");
    }

        @GET
        @Path("analyse")
        public PageView analyse(){
            return new PageView("analyse.ftl");
        }

        @GET
        @Path("decisiontree")
        public PageView decisionTree(){
            return new PageView("decisiontree.ftl");
        }

        @GET
        @Path("ModelResults")
        public PageView decisionTreeResults(){
            return new PageView("ModelResults.ftl");
        }

        @GET
        @Path("ModelOutput")
        public PageView decisionTreeOutput(){
            return new PageView("ModelOutput.ftl");
        }

        private static WebApplicationException redirect(String path) {
            URI uri = UriBuilder.fromUri(path).build();
            Response response = Response.seeOther(uri).build();
            return new WebApplicationException(response);
        }
}
