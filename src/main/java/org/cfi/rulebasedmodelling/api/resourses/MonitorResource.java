package org.cfi.rulebasedmodelling.api.resourses;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import org.cfi.rulebasedmodelling.api.Config;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/monitor")
@Api("/monitor")
public class MonitorResource {
    Config config;
    ObjectMapper objectMapper;

    public MonitorResource(Config config, ObjectMapper objectMapper) {
        this.config = config;
        this.objectMapper = objectMapper;
    }

    @GET
    @Path("/dummy")
    public void dummyMethod(){
    }


}
