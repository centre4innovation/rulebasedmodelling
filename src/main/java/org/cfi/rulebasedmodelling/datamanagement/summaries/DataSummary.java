package org.cfi.rulebasedmodelling.datamanagement.summaries;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Maps;
import org.assertj.core.util.Lists;
import org.cfi.rulebasedmodelling.datamanagement.ColumnData;
import org.cfi.rulebasedmodelling.datamanagement.ColumnDateTime;
import org.cfi.rulebasedmodelling.datamanagement.ColumnNumeric;
import org.cfi.rulebasedmodelling.datamanagement.ColumnString;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class DataSummary {

    public String tableName;

    @JsonIgnore
    public AtomicInteger order = new AtomicInteger();

    @JsonProperty
    private List<ColumnData> columns = Lists.newArrayList();

    @JsonIgnore
    private Map<String, Integer> nameOrder = Maps.newHashMap();

    public DataSummary(String tableName) {
        this.tableName = tableName;
    }

    public void initialseColumnData(String columnType){
        int ord = order.getAndIncrement();
        ColumnData columnData;
        if(columnType.equals("numeric")){
            columnData = new ColumnNumeric(this.tableName,ord);
        }
        else if(columnType.equals("datetime")){
            columnData = new ColumnDateTime(this.tableName,ord);
        }
        else{
            columnData = new ColumnString(this.tableName,ord,100);
        }
        columns.add(columnData);
    }

    public List<ColumnData> getColumns(){
        return this.columns;
    }

    public void setColumnName(String columnName, int order){
        int ord = this.columns.get(order).setColumnName(columnName);
        nameOrder.put(columnName,ord);
    }

    public ColumnData getColumnData(String name){
        return this.columns.get(nameOrder.get(name));
    }

}
