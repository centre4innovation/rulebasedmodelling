package org.cfi.rulebasedmodelling.datamanagement;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Maps;
import org.cfi.rulebasedmodelling.time.TimeStamped;
import org.joda.time.DateTime;

import java.util.Map;

public class TableData implements TimeStamped {


    @JsonProperty
    public DateTime dateTime;
    @JsonProperty
    public Map<String,Object> data = Maps.newLinkedHashMap();

    public TableData(Map<String,Object> data){
        for(Map.Entry<String,Object> d : data.entrySet()) {
            if(d.getKey().toLowerCase().contains("date") || d.getKey().toLowerCase().contains("time")) this.dateTime = new DateTime(d.getValue());
            else this.data.put(d.getKey(),d.getValue());
        }
    }

    @Override
    public DateTime getTimeStamp(){
        return dateTime;
    }

    public TableData setDateTime(DateTime dateTime) {
        this.dateTime = dateTime;
        return this;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public TableData setData(Map<String, Object> data) {
        this.data = data;
        return this;

    }
}
