package org.cfi.rulebasedmodelling.datamanagement.Query;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.cfi.rulebasedmodelling.postgres.SQLFunction;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CombinationItem {

    private String table;
    private List<String> columns = Lists.newArrayList();
    private List<String> joins = Lists.newArrayList();
    private List<String> aggregateColumns = Lists.newArrayList();
    private String by;
    private String aggregateFunction;

    public CombinationItem(){}

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public List<String> getColumns() {
        return columns;
    }

    public void setColumns(String column) {
        this.columns.add(column);
    }

    public List<String> getJoins() {
        return this.joins;
    }

    public Map<String, String> getAggregates() {
        Map<String, String> aggsMap = Maps.newLinkedHashMap();
        for(int j = 0; j < aggregateColumns.size(); j++) {
            String[] aggs = aggregateColumns.get(j).split("\\.");
            if (aggs.length > 1) {
                aggsMap.put("aggs_" + aggs[0], "date_trunc(\'" + aggs[1] + "\'," + aggs[0] + ")");
            }
            else{
                aggsMap.put("aggs_" + aggregateColumns.get(j), aggregateColumns.get(j));
            }
        }
        return aggsMap;
    }

    public List<String> getColumnsLabeledWithTable(){
        return this.columns.stream().map(c -> this.table + "_" + c).collect(Collectors.toList());
    }

    public List<String> getColumnsWhenAggregating(){
        List<String> simplifiedAggs = this.aggregateColumns.stream().map(c -> ifDateRewrite(c)).collect(Collectors.toList());
        List<String> toBeAggregated = this.columns.stream().filter(c -> !simplifiedAggs.contains(c)).collect(Collectors.toList());
        List<String> rewritten = toBeAggregated.stream().map(c -> this.getAggregateFunction().toString() + "(agtab." + c + ") AS " + c).collect(Collectors.toList());
        this.aggregateColumns.stream().forEach(c -> rewritten.add(ifDateStrip(c)));
        return rewritten;
    }

    public List<String> getCleanedGroupByColumns(){
        return this.aggregateColumns.stream().map(c -> ifDateRewrite(c)).collect(Collectors.toList());
    }

    private String ifDateStrip(String col){
        String[] aggs = col.split("\\.");
        if (aggs.length > 1) {
            return "date_trunc(\'" + aggs[1] + "\',agtab." + aggs[0] + ") AS " + aggs[0];
        }
        return "agtab."+ col;
    }

    private String ifDateRewrite(String col){
        String[] aggs = col.split("\\.");
        if (aggs.length > 1) {
            return aggs[0];
        }
        return col;
    }

//    public Map<String, String> getBy() {
//        Map<String, String> byMap = Maps.newLinkedHashMap();
//        String[] bys = by.split("\\.");
//        if (bys.length > 1) {
//            byMap.put("join_" + bys[0], "date_trunc(\'" + bys[1] + "\'," + bys[0] + ")");
//        }
//        else {
//            byMap.put("join_" + by, by);
//        }
//        return byMap;
//    }

//    public void setBy(String by) {
//        this.by.add(by);
//    }
//    public void setBy(String by) {
//        this.by = by;
//    }
    public void setJoins(String join) {
        this.joins.add(join);
    }

    public void setAggs(String agg) {
        this.aggregateColumns.add(agg);
    }

    public SQLFunction getAggregateFunction() {
        this.aggregateFunction = this.aggregateFunction == null ? "NONE" : this.aggregateFunction;
        return SQLFunction.valueOf(aggregateFunction);
    }

    public void setAggregateFunction(String aggregateFunction) {
        this.aggregateFunction = aggregateFunction;
    }

    public boolean isAggregate(){
        return aggregateFunction != null;
    }
}
