package org.cfi.rulebasedmodelling.datamanagement.Query;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.collect.Lists;

import java.util.List;


@JsonDeserialize(using = DataClassifierDeserializer.class)
public class DataClassifier implements QueryObject {

    private List<PathFilter> pathlist = Lists.newArrayList();
    private String tableName;
    private String tableToFilter;

    public DataClassifier(List<PathFilter> pathFilters){
        this.pathlist.addAll(pathFilters);
    }

    public DataClassifier(){}

    public List<PathFilter> getOrList() {
        return pathlist;
    }

    public void setPathList(PathFilter pathComponent) {
        this.pathlist.add(pathComponent);
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableToFilter() {
        return tableToFilter;
    }

    public void setTableToFilter(String tableToFilter) {
        this.tableToFilter = tableToFilter;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < this.pathlist.size(); i++){
            sb.append("(");
            sb.append(this.pathlist.get(i).toString());
            sb.append(")");
            if(i != this.pathlist.size() - 1) sb.append(" OR ");
        }
        return sb.toString();
    }
}
