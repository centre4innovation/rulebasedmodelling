package org.cfi.rulebasedmodelling.datamanagement.Query;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.Iterator;

public class CombinationDeserializer  extends JsonDeserializer<Combination> {

    @Override
    public Combination deserialize(JsonParser jp, DeserializationContext cxt) throws IOException, JsonProcessingException {
        ObjectCodec objectCodec = jp.getCodec();
        ObjectNode arrayNode = objectCodec.readTree(jp);

        /**Track all tables to be joined**/
        Iterator<JsonNode> paths = arrayNode.path("tablelist").elements();
        Combination tableCombination = new Combination();
        while(paths.hasNext()) {
            /**Assign a combination item to collect a tables information**/
            CombinationItem table = new CombinationItem();
            JsonNode path = paths.next();

            /**Add name of table**/
            table.setTable(path.get("table").asText());

            /**Add all columns of interest in table**/
            Iterator<JsonNode> cols = path.get("columns").elements();
            while(cols.hasNext()) {
                JsonNode col = cols.next();
                String colVal = col.textValue();
                table.setColumns(colVal);
            }

            /**Will there be aggregation**/
            table.setAggregateFunction(path.get("function") == null ? null : path.get("function").asText());

            /**Add columns to join on**/
            JsonNode by = path.get("by");
            Iterator<JsonNode> join = by.get("join").elements();
            while(join.hasNext()) {
                JsonNode j = join.next();
                String joinVal = j.textValue();
                table.setJoins(joinVal);
            }

            /**Add columns to aggregate over**/
            Iterator<JsonNode> agg = by.get("agg").elements();
            while(agg.hasNext()) {
                JsonNode a = agg.next();
                String aggVal = a.textValue();
                table.setAggs(aggVal);
            }

//            table.setBy(path.get("by").asText());

            /**Add table to combination tracker**/
            tableCombination.setTablesToCombine(table);
        }
        /**Assign common names to columns that are being joined from different tables**/
        Iterator<JsonNode> joins = arrayNode.get("by").elements();
        while(joins.hasNext()) {
            JsonNode js = joins.next();
            String joinVal = js.textValue();
            tableCombination.setBy(joinVal);
        }

//            String by = arrayNode.get("by").asText();
//            tableCombination.setBy(by);
        /**New table name for combined data**/
        String tableName = arrayNode.get("tableName").asText();
        tableCombination.setTableName(tableName);
        return tableCombination;
    }
}
