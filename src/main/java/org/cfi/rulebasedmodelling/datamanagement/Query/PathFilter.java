package org.cfi.rulebasedmodelling.datamanagement.Query;

import com.google.common.collect.Lists;

import java.util.List;

public class PathFilter {

    private List<Filter> pathFilters = Lists.newArrayList();

    public PathFilter(List<Filter> filters){
        this.pathFilters.addAll(filters);
    }

    public PathFilter(){}

    public List<Filter> getFilters() {
        return this.pathFilters;
    }

    public void setPathFilters(Filter pathFilter) {
        this.pathFilters.add(pathFilter);
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < this.pathFilters.size(); i++){
            sb.append(this.pathFilters.get(i).toString());
            if(i != this.pathFilters.size() - 1) sb.append(" AND ");
        }
        return sb.toString();
    }
}
